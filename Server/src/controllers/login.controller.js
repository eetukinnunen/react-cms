// import {mongoDbConnect} from '../utils.js';
import {User} from "../models/index.js";
import jwt from "jsonwebtoken";
import bcryptjs from "bcryptjs";


//Searches user from db, compares hashed pw from db to input pw. If password is correct, cookies are created
const login = async(req, res) => {
    const body = req.body;
    User.findOne({ username: body.username }, function (err, user) {
        if (err){throw err};
        if (user !== null) {
            console.log("User found - checking password")
            //compare result is true if hashed password matches user input
            bcryptjs.compare(body.password, user.password, function(err, result) {
            if (err){throw err};
            if (result){
                console.log("SUCCESS");
                //expiration for jwt-token, last brackets are for the duration in seconds, so 60*60 is an hour;
                //Change also jwtExpTime from utils.js authJWT if you change value of this
                const jwtExpTime = Math.floor(Date.now() / 1000) + (60*60);
                //jwt token payload
                const userForToken = {username: user.username, exp: jwtExpTime};
                //creating the token using secret from .env
                const jwtToken = jwt.sign(userForToken, process.env.JWTKEY);
                res
                    .status(200)
                    //Create cookie needed for auth. Remember secure: true when HTTPS!-----------------------------------
                    .cookie('sitecookie', jwtToken, {httpOnly: true, secure: false,})
                    .send(JSON.stringify("Login"));
            }
            else{
                console.log("Wrong Password");
                return res
                        .status(401)
                        .send(JSON.stringify("Väärä käyttäjätunnus tai salasana"));
            }
            });
    }
        else{
            console.log("User not found");
            res
                .status(401)
                .send(JSON.stringify("Väärä käyttäjätunnus tai salasana"));
        };
    });
};

const logout = (req, res) => {
    console.log("logging out")
    res
        .status(200)
        .cookie('sitecookie', "logout", {httpOnly: true, secure: false,})
        .send(JSON.stringify("Kirjaudutaan ulos"));
}

const check = (req, res) => {
        const token = req.cookies['sitecookie'];
        jwt.verify(token, process.env.JWTKEY, (err) => { 
        if (err) {
            return res.status(401).send("no");
        }
            res.status(200).send("yes");
        });
    };



export{
    login,
    logout,
    check,
}