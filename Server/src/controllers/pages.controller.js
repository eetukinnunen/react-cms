import { pageImgsCrop, myXSS } from "../utils.js";
import mongodb from "mongodb";
import {Pages} from "../models/index.js";

const getPages = async(req, res) => {
	console.log("GETTING PAGES IN PAGES CONTROLLER");
    const pages = await Pages.find({});
    if (!pages) {
        res.status(404).end()
    }
    res.status(200).send(pages);
};

const getPage = async(req, res) => {
    const page = await Pages.findOne({_id: mongodb.ObjectId(req.params.id)});
    if(!page) {
        res.status(404).end();
        return;
    }
    res.status(200).send(page)
};

const updatePage = async(req, res) => {

    console.log(req.body.contents);
    const contents = JSON.parse(req.body.contents);
    console.log("kontentit:");
    console.log(contents);

    // Get the current page by id
    const page = await Pages.findOne({_id: mongodb.ObjectId(req.body._id)});

    let result;

    // empty the array so it can be populated from scratch (so deleted items (somelinks) don't stay in the db)
    page.contents = [];

    // Only update as many fields as necessary (but not too many)
    //let counter = 0;
    //page.contents.length > contents.length ? counter = contents.length : counter = page.contents.length;
    //for (let i = 0; i < counter; i++) {
    for (let i = 0; i < contents.length; i++) {
        page.contents[i] = {
            "name": contents[i].name,
            "content": myXSS.process(contents[i].content)
        };
    }

    // if image infos have been submitted (aka the possibility of images exists on that page)
    if (req.body.imageInfos) {
        // if there are crop infos, there are either new or old images
        console.log(JSON.parse(req.body.imageInfos).length);
        if (JSON.parse(req.body.imageInfos).length > 0) {
            result = await pageImgsCrop(page, req.files, req.body.imageInfos);
        } else {
            // no images at all, but that's fine too
            console.log("no imagiis");
            result = true;
        }
        
        console.log("result after awaiting");
        console.log(result);
    } else {
        // we get here on season change
        result = true;
    }
    
    if (req.body.articlesperpage && req.body.previewLength) {
        page.articlesperpage = req.body.articlesperpage;
        page.previewLength = req.body.previewLength;
    }

    if (req.body.season) {
        page.season = req.body.season;
    };

    if (result) {
        if (typeof result !== "object") {
            const updatePage = await Pages.updateOne({_id: mongodb.ObjectId(req.body._id)}, {$set: page})
            if (updatePage) {
                res.status(200).send(updatePage);
            } else {
                res.status(400).end();
            }
        } else {
            console.log(result);
            res.status(400).send(result.toString());
        }
    } else {
        res.status(400).end();
    }
}

export {
    getPages,
    getPage,
    updatePage
}