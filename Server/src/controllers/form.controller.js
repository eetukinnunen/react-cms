import express from "express";
import {productMail, serviceMail, contactMail} from "../email.js";
import fetch from "node-fetch";
import dotenv from "dotenv";
import xss from "xss";
import {myXSS} from "../utils.js";

const config = dotenv.config().parsed;

const formSubmit = async (req, res) => {

    const secret = config["RECAPTCHA_SECRET"];

    fetch("https://www.google.com/recaptcha/api/siteverify", {
        method: "POST",
        headers: {
            "Content-Type": "application/x-www-form-urlencoded",
        },
        body: `secret=${secret}&response=${req.body.token}`,
    })
    .then(response => response.json())
    .then(async (json) => {

        // if recaptcha passes without issues
        if (json.action === "submit" && json.success === true && json.score > 0.5) {

            // every form has these fields
            const postPackage = {
                customerName: myXSS.process(req.body.name),
                customerEmail: myXSS.process(req.body.email),
                message: myXSS.process(req.body.message),
            };

            //console.log(xss.whiteList);

            let result;

            if (req.body.productName) {
                postPackage.product = req.body.productName;
                postPackage.price = req.body.price;
                postPackage.addons = req.body.addons;
                result = await productMail(postPackage);
            } else if (req.body.serviceName) {
                postPackage.service = req.body.serviceName;
                result = await serviceMail(postPackage);
            } else { // if not product or service: a generic contact form
                result = await contactMail(postPackage);
            }

            console.log("in form controller");
            console.log(result);
            if(result) {
                res.status(200).send(JSON.parse(result));
            } else {
                res.status(400).end();
            }

        } else {
            res.status(403).end();
        }
    })
    .catch(error => {
        console.log(error);
        res.status(400).end();
    });

}

export default formSubmit;
