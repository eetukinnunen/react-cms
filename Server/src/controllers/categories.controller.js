import mongodb from "mongodb";
import { Categories } from "../models/index.js";
import { myXSS, stringToBool } from "../utils.js";

const createCategory = async(req, res) => {

    let summerNum, winterNum;
    // get all season order numbers from db
    const seasonNumbers = await Categories.find({}, "summerNum winterNum");
    // simplify the order numbers into 1d arrays
    const summerNumbers = seasonNumbers.map(season => season.summerNum);
    const winterNumbers = seasonNumbers.map(season => season.winterNum);
    // get the first number not in each array and choose that as our new number
    for (let i = 1; i <= seasonNumbers.length; i++) {
        if (!summerNumbers.some(num => num === i)) {
            summerNum = i;
        }
        if (!winterNumbers.some(num => num === i)) {
            winterNum = i;
        }
    }
    // if there were no free gaps within the array length, give length+1
    if (!summerNum) {
        summerNum = seasonNumbers.length + 1;
    }
    if (!winterNum) {
        winterNum = seasonNumbers.length + 1;
    }

    const newCategory = {
        catName: req.body.catName.trim(),
        catDescription: myXSS.process(req.body.catDescription),
        summerNum: summerNum,
        winterNum: winterNum,
    };
    console.log(newCategory);

    //const category = newCategory;
    const category = await Categories.create(newCategory);
    if(!category){
        res.status(400).end();
    }
    res.status(200).send(category);
};

const updateCategory = async(req, res) => {
    const updateCategory = {
        catName: req.body.catName.trim(),
        catDescription: myXSS.process(req.body.catDescription)
    };

    // if season order numbers are given, put them in
    if (req.body.summerNum) {
        updateCategory.summerNum = req.body.summerNum;
    }
    if (req.body.winterNum) {
        updateCategory.winterNum = req.body.winterNum;
    }

    // if _deleted field exists in request, flip it
    if (req.body._deleted) {
        updateCategory._deleted = !stringToBool(req.body._deleted);
    }

    const category = await Categories.updateOne({_id: mongodb.ObjectId(req.body._id)}, {$set: updateCategory});
    if(!category) {
        res.status(400).end();
        return;
    };
    res.status(200).send(category);
};

const updateCategoryOrder = async(req, res) => {
    const categories = JSON.parse(req.body.order);
    for( const i in categories) {
        const category = categories[i];
        console.log(category);
        await Categories.updateOne({_id: mongodb.ObjectId(category._id)}, {$set: {
            winterNum: category.winterNum,
            summerNum: category.summerNum
        }});
    }

    res.status(200).send({});
}

const deleteCategory = async(req, res) => {
    try {
        const category = await Categories.deleteOne({_id: mongodb.ObjectId(req.params.id)});
        if (category.deletedCount > 0) {
            res.status(200).send(category);
        } else {
            res.status(400).send("Tuotekategoriaa ei löytynyt tällä id:llä!");
        }
    } catch (err) {
        console.log(err);
        res.status(400).send(err.toString());
    }
};

export {
    createCategory,
    updateCategory,
    updateCategoryOrder,
    deleteCategory
}