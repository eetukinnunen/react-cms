import { productImgsCrop, myXSS, slugger, determineUniqueSlug, stringToBool, campaignPriceRemover, errorMessages } from "../utils.js";
import { Products, Categories } from "../models/index.js";
import mongodb from "mongodb";

const getProducts = async(req, res) => {
    const products = req.authed 
        ? await Products.find({})
        : await Products.find({_deleted: {$ne: "true"}})
    const categories = req.authed 
        ? await Categories.find({})
        : await Categories.find({_deleted: {$ne: "true"}})

    const temp = categories.map(category => {
        const categoryProducts = products.filter(product => {
            return product.categoryId.equals(category._id);
        })

        return {
            _id: category._id,
            catName: category.catName,
            summerNum: category.summerNum,
            winterNum: category.winterNum,
            catDescription: category.catDescription,
            catProducts: categoryProducts,
            _deleted: category._deleted
        }
    })

    res.status(200).send(temp);
    
}

const productsById = async(req, res) => {
    const findProduct = await Products.findOne({_id: mongodb.ObjectId(req.params.id)})
    if (!findProduct) {
        res.status(404).end();
        return;
    } 
    res.status(200).send(findProduct);       
}

const createProduct = async(req, res, e) => {
    console.log("START ////////////////////////////////////////////////////////////////////////////////////////");
    let result;

    const productName = req.body.productName.trim();
    const slug = slugger(productName);
    const uniqueSlug = await determineUniqueSlug(slug, req.body._id, "product");

    if (!slug) {
        res.status(500).send("Object type not found");
    }

    let campaignPrice;
    if (req.body.campaignPrice) {
        campaignPrice = req.body.campaignPrice;
        await campaignPriceRemover(campaignPrice);
    }

    let campaignText = "";
    if (req.body.campaignText) {
        campaignText = req.body.campaignText;
    }

    let newProduct = {
        categoryId: mongodb.ObjectId(req.body.categoryId),
        productName: req.body.productName.trim(),
        slug: uniqueSlug,
        productBrief: req.body.productBrief,
        productText: myXSS.process(req.body.productText),
        price: req.body.price,
        campaignPrice: campaignPrice,
        campaignText: campaignText,
        tax: req.body.tax,
        addons: JSON.parse(req.body.addons),
        productImages: []
    }

    if (req.body.campaignPrice) {
        newProduct = await campaignPriceRemover(newProduct, req.body.campaignPrice);
    }

    console.log(req.files.length);
    if (req.files.length > 0) {
        result = await productImgsCrop(newProduct, req.files, req.body.productImageInfo);
    }
    else {
        // no image but fine
        result = true;

        // checks if productimageinfo has actual values, and if it does, it means multer rejected image
        const parsedInfo = JSON.parse(req.body.productImageInfo);
        parsedInfo.forEach(info => {
            const parsedCrop = JSON.parse(info.crop);
            console.log(parsedCrop);
            if (parsedCrop) {
                result = {
                    error: errorMessages.imgRejected
                };
            }
        });
    }

    if (result) {
        if (typeof result !== "object") {
            const product = await Products.create(newProduct);
            if (!product) {
                res.status(400).send("Syötettyä tuotekategoriaa ei löytynyt!");
            } else {
                console.log(product);
                res.status(200).send(product); 
            }
        } else {
            res.status(400).send(result.error);
        }
    } else {
        res.status(400).end();
    }

};

const updateProduct = async(req, res) => {

    const findProduct = await Products.findOne({_id: mongodb.ObjectId(req.body._id)})

    let result;

    if (req.body.productImageInfo) {
        // if there are crop infos, there are either new or old images
        console.log(JSON.parse(req.body.productImageInfo).length);
        if (JSON.parse(req.body.productImageInfo).length > 0) {
            result = await productImgsCrop(findProduct, req.files, req.body.productImageInfo);
        } else {
            // no images at all, but that's fine too
            result = true;
        }

        console.log("result after awaiting");
        console.log(result);
    } else {
        // even moreso no images, as we get here only in soft delete / restore
        result = true;
    }

    const productName = req.body.productName.trim();
    const slug = slugger(productName);
    const uniqueSlug = await determineUniqueSlug(slug, req.body._id, "product");

    if (!slug) {
        res.status(500).send("Object type not found");
    }

    let campaignPrice;
    if (req.body.campaignPrice) {
        campaignPrice = req.body.campaignPrice;
        await campaignPriceRemover(campaignPrice);
    }

    let campaignText = "";
    if (req.body.campaignText) {
        campaignText = req.body.campaignText;
    }

    let update = {
        categoryId: mongodb.ObjectId(req.body.categoryId),
        productName: req.body.productName.trim(),
        slug: uniqueSlug,
        productBrief: req.body.productBrief,
        productText: myXSS.process(req.body.productText),
        price: req.body.price,
        campaignPrice: campaignPrice,
        campaignText: campaignText,
        tax: req.body.tax,
        addons: JSON.parse(req.body.addons),
        productImages: findProduct.productImages,
    };

    // if _deleted field exists in request, flip it
    if (req.body._deleted) {
        update._deleted = !stringToBool(req.body._deleted);
    }

    console.log(update);

    console.log("result right before sending response");
    console.log(typeof result);
    console.log(result);

    if (result) {
        if (typeof result !== "object") {
            console.log("it's true");
            const product = await Products.updateOne({_id: mongodb.ObjectId(req.body._id)}, {$set: update});
            if(product) {
                res.status(200).send(product);
            } else {
                res.status(400).end();
            }
        } else {
            res.status(400).send(result.error);
        }
    } else {
        res.status(400).end();
    }
};

const deleteProduct = async(req, res) => {
    try {
        const product = await Products.deleteOne({_id: mongodb.ObjectId(req.params.id)});
        if (product.deletedCount > 0) {
            res.status(200).send(product);
        } else {
            res.status(400).send("Tuotetta ei löytynyt tällä id:llä!");
        }
    }  catch (err) {
        console.log(err);
        res.status(400).send(err.toString());
    }
}

export {
    getProducts,
    productsById,
    createProduct,
    updateProduct,
    deleteProduct
}