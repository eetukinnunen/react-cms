import {upload, uploadFolder, getFolderSize, convertBytes, imgFileTypes} from "../utils.js";
import fs from "fs";
import path from "path";
import Articles from "../models/articles.model.js";
import Products from "../models/products.model.js";
import Pages from "../models/pages.model.js";
import Services from "../models/services.model.js";

function readImages(folder) {
    return fs.readdirSync(folder).filter(file => imgFileTypes.includes(path.extname(file).toLowerCase()));
}

const getImages = async(req, res) => {
    let allFiles = readImages(uploadFolder);
    const cropped = readImages(`${uploadFolder}/crop`).map(img => `crop/${img}`);
    
    allFiles = allFiles.concat(cropped);
    res.status(200).send(allFiles);
};

const getImagesFolderSize = async(req, res) => {
    const [result, maxSize] = getFolderSize(uploadFolder);
    
    if(result > maxSize) {
        res.status(404).end(`Upload folder is full`)
    } else {
        console.log(`${convertBytes(result)}/${convertBytes(maxSize)}`);
        res.status(200).send([`${result}`, `${maxSize}`]);
    };
};

const imagesLocation = async(req, res) => {
    const places = [];
    console.log(req.body.imgName)
    const articles = await Articles.find({image: req.body.imgName});
    articles.forEach(article => {
        places.push({type: "article", name: article.title, id: article._id, message: ` ${article.title}`});
    });
    
    const products = await Products.find({productImages: {$in: [req.body.imgName]}});
    products.forEach(product => {
        places.push({type: "product", name: product.productName, id: product._id, message: ` ${product.productName}`});
    });
    
    const pages = await Pages.find({$or: [{"images.summer": req.body.imgName}, {"images.winter": req.body.imgName}]});
    pages.forEach(page => {
        places.push({type: "page", name: page.title, id: page._id, message: ` ${page.title}`});
    });
    
    const services = await Services.find({serviceimage: req.body.imgName});
    services.forEach(service => {
        places.push({type: "service", name: service.serviceName, id: service._id, message: ` ${service.serviceName}`});
    });

    console.log(places);
    if (places.length) {
        res.status(200).send(places);
    } else {
        res.status(200).send([])
    }
};

const uploadImage = async(req, res) => {
    // Handles tinymce image upload
        console.log("reqqies:");
        console.log(req.body);
        console.log(req.file);

        //res.send({"location": req.file.path});
        const responseObj = {"location": req.file.filename};
        console.log(JSON.stringify(responseObj))
        res.status(200).send(responseObj);
}

const deleteImage = async(req, res) => {
    fs.unlink(`${uploadFolder}/${req.body.imgName}`, (err) => {
        if (err) {
            console.log(err);
            res.status(400).send(err.toString());
            return;
        } else {
            console.log("image deleted");
            res.status(200).send(true);
        }
    });
};

export {
    getImages,
    getImagesFolderSize,
    imagesLocation,
    uploadImage,
    deleteImage
}