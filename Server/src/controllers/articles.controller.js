import { Articles } from "../models/index.js"; 
import { uploadFolder, myXSS, slugger, determineUniqueSlug, stringToBool, imgCropHandler, errorMessages } from "../utils.js";
import mongodb from "mongodb";
import xss from "xss";

async function getArticle (req,res) {
    //does't return hidden or trashed articles if not authed
	console.log("#############################GETTIN ARTICLES ARTICLE CONTROLLER");
    const articles = req.authed 
        ? await Articles.find({})
        : await Articles.find({public: "true", _deleted: {$ne: "true"}})
    res.status(200).send(articles);
}

async function createArticle (req,res) {
    let result;
    let imgName = ""; // default in case of bug
    const imageInfo = JSON.parse(req.body.articleImageInfo);
    const crop = JSON.parse(imageInfo.crop);

    if (req.file) {
        imgName = req.file.filename;

        // if the user doesn't crop, width and height are set as zero.
        // so if they are, let's not crop here either
        if (!(crop.width === 0 || crop.height === 0)) {
            // because react-image-crop's crop values are based on the scaled-down image preview canvas,
            // we must multiply them by the scaling values to match the original image again
            const scaling = JSON.parse(imageInfo.scaling);
            console.log(crop);
            console.log(scaling);
    
            await imgCropHandler(imgName, crop, scaling, req.body.articleImageInfo.isNewImg)
            .then(function(res) {
                console.log(res);
                result = res;

                if(result && typeof result !== "object") {
                    imgName = result;
                }
            })
            .catch(error => {
                result = error;
            })
            console.log("Result in for-loop");
            console.log(result);
        }
    } else if (crop) {
        // if image info stuff is not null but there is no image, it means multer rejected our image because of disk size (or filetype (todo)) issue
        result = {
            error: errorMessages.imgRejected
        };
    } else  {
        // no article image
        result = true;
    }

    const title = req.body.title.trim();
    const slug = slugger(title);
    const uniqueSlug = await determineUniqueSlug(slug, req.body._id, "article");

    if (!slug) {
        res.status(500).send("Object type not found");
    }

    const newArticle = {
        title: title,
        slug: uniqueSlug,
        date: req.body.date,
        public: stringToBool(req.body.public),
        content: myXSS.process(req.body.content),
        image: imgName
    };

    console.log(newArticle);
    console.log(result);
    console.log(typeof result);
    if (result) {
        if (typeof result !== "object") {
            const article = await Articles.create(newArticle);
            if(article) {
                res.status(200).send(article);
            } else {
                res.status(400).end();
            }
        } else {
            console.log(result);
            res.status(400).send(result.toString());
        }
    } else {
        res.status(400).end();
    }
    

}

async function updateArticle (req, res) {
    const findArticle = await Articles.findOne({_id: mongodb.ObjectId(req.body._id)});
    if(!findArticle) {
        res.status(404).end()
        return;
    };

    let result;

    let imgName;

    if (req.body.articleImageInfo) {
        const imageInfo = JSON.parse(req.body.articleImageInfo);
        const crop = JSON.parse(imageInfo.crop);
        const scaling = JSON.parse(imageInfo.scaling);
    
        // if no image, crop & scaling are null
        if (crop && scaling) {
            console.log(crop);
            console.log(scaling);
            // if the user doesn't crop, width and height are set as zero.
            // so if they are, let's not crop here either
            if (!(crop.width === 0 || crop.height === 0)) {
                // because react-image-crop's crop values are based on the scaled-down image preview,
                // we must multiply them by the scaling values to match the original image again
    
                const isNewImg = JSON.parse(imageInfo.isNewImg);
    
                if (isNewImg) {
                    if (req.file) {
                        imgName = req.file.filename;
                        await imgCropHandler(imgName, crop, scaling, isNewImg)
                        .then(function(res) {
                            console.log(res);
                            result = res;

                            if(result && typeof result !== "object") {
                                imgName = result;
                                console.log(imgName);
                            }
                        })
                        .catch(error => {
                            result = error;
                        })
                        console.log("Result in for-loop");
                        console.log(result);
                    } else {
                        // if no req.file, it means multer rejected img upload
                        result = {
                            error: errorMessages.imgRejected
                        };
                    }
                } else {
                    imgName = findArticle.image;
                    await imgCropHandler(imgName, crop, scaling, isNewImg)
                    .then(function(res) {
                        console.log(res);
                        result = res;

                        if(result && typeof result !== "object") {
                            imgName = result;
                            console.log(imgName);
                        }
                    })
                    .catch(error => {
                        result = error;
                    })
                    console.log("Result in for-loop");
                    console.log(result);
                }
                
                // if(result && typeof result !== "object") {
                //     imgName = result;
                //     console.log(imgName);
                // }
            }
        } else {
            result = true; // no image at all but it's fine
        }
    } else { // we should probably get here only on softdelete/restore
        imgName = req.body.image;
        result = true;
    }
    
    console.log("orig:");
    console.log(req.body.content);
    console.log("default xss:");
    console.log(xss(req.body.content));
    console.log("myxss:");
    console.log(myXSS.process(req.body.content));
    console.log("default xss escapeHTML:");
    console.log(xss.escapeHtml(req.body.content));

    const title = req.body.title.trim();
    
    const slug = slugger(title);
    const uniqueSlug = await determineUniqueSlug(slug, req.body._id, "article");

    if (!slug) {
        res.status(500).send("Object type not found");
    }

    const update = {
        title: title,
        slug: uniqueSlug,
        date: req.body.date,
        public: stringToBool(req.body.public),
        content: myXSS.process(req.body.content),
        image: imgName
    };

    // if _deleted field exists in request, flip it
    if (req.body._deleted) {
        update._deleted = !stringToBool(req.body._deleted);
    }

    console.log(result);
    console.log(typeof result);
    
    //const article = update;
    if (result) {
        if (typeof result !== "object") {
            const article = await Articles.updateOne({_id: mongodb.ObjectId(req.body._id)}, {$set: update});
            if(article) {
                res.status(200).send(article);
            } else {
                res.status(400).end();
            }
        } else {
            console.log(result);
            res.status(400).send(result.toString());
        }
    } else {
        res.status(400).end();
    }

}

async function deleteArticle (req,res) {
    const findArticle = await Articles.findOne({_id: mongodb.ObjectId(req.params.id)});

    if (!findArticle) {
        res.status(400).end();
        return;
    }
    const article = await Articles.deleteOne({_id: mongodb.ObjectId(req.params.id)});

    if (article.deletedCount > 0) {
        res.status(200).send(article);
    } else {
        res.status(400).end();
    }

}

export {
    getArticle, 
    createArticle, 
    updateArticle, 
    deleteArticle,
}


