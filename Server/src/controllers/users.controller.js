import {User} from "../models/index.js";
import bcryptjs from "bcryptjs";
import jwt from 'jsonwebtoken';
import crypto from "crypto";
import nodemailer from "nodemailer";
import dotenv from "dotenv";

const config = dotenv.config().parsed;

const saltRounds = 11;

const changeUserPassword = async(req, res) => {

    //Gets the username of currently logged user from cookie created at login.
    const token = req.cookies['sitecookie'];
    const jwtDecoded = jwt.verify(token, process.env.JWTKEY);
    // console.log("decoded user from jwt = " + jwtDecoded)
    const loggedUser = await User.findOne({ username: jwtDecoded.username});
    //if loggedUser === null, it means jwt verification failed. Means outdated session or something bad.
    if (loggedUser !== null) {
        //if old password correct, hash the new one and replace pw in db, 
        bcryptjs.compare(req.body.password, loggedUser.password, function (err, result) {
            if (err){throw err};
            if (result) {
                // saltrounds changes the complexity, time needed to verify rises fast with more rounds.
                bcryptjs.hash(req.body.newPassword, saltRounds, function (err, hash) {
                    if (err){throw err};
                    loggedUser.password = hash;
                    loggedUser.save();
                    console.log("success")
                    console.log("new pw hashed is now "+ hash);
                    return res
                        .status(200)
                        .send("Changed PW")
                });
            }
            else {
                res
                .status(401)
                .send("Vanha salasana väärin")
            };
        })
    }
    else{
        res
        .status(401)
        .send("Vanhentunut käyttäjäistunto")
    };
};

const changeUserEmail = async(req, res) => {

    const token = req.cookies['sitecookie'];
    const jwtDecoded = jwt.verify(token, process.env.JWTKEY);
    const loggedUser = await User.findOne({ username: jwtDecoded.username});
    if (loggedUser !== null) {
        bcryptjs.compare(req.body.password, loggedUser.password, function (err, result) {
            if (err){throw err};
            if (result) {
                    loggedUser.email = req.body.email;
                    loggedUser.save();
                    console.log("success")
                    console.log("new email is now "+ req.body.email);
                    return res
                        .status(200)
                        .send("Changed email")
            }
            else {
                res
                .status(401)
                .send("Vanha salasana väärin")
            };
        })
    }
    else{
        res
        .status(401)
        .send("Vanhentunut käyttäjäistunto")
    };
};


const resetPasswordEmail = async(req, res) => { 
    const user = await User.findOne({ email: req.body.email});
        if (user !== null) {

            //randomstring and expiration time that lasts an hour from creation.
            const pwResetToken = crypto.randomBytes(20).toString("hex");

            user.resettoken = pwResetToken;
            user.resettokenexpiration = Date.now() + 3600000;
            user.save();
            console.log(user.username+" creating resetpassword email");

            const transporter = nodemailer.createTransport({
                service: "gmail",
                auth: {
                    user: config["emailUserName"],
                    pass: config["emailPassword"]
                }
            });
            //TODO osoite https? Muuttujana envistä?
            const mailOptions = {
                from: "Website Server",
                to: req.body.email,
                subject: "Unohtunut salasana",
                text:   `Vaihda unohtunut salasana täällä: https://localhost:3001/hallintapaneeli/reset/${pwResetToken}`
            };
            console.log("######################################################")
            console.log(mailOptions);
            console.log(config["emailUserName"]);
            console.log(config["emailPassword"]);
            console.log("######################################################")
            // console.log(mailOptions)

            transporter.sendMail(mailOptions, function(error, info){
                if (error) {
                console.log(error);
				console.log("#############################################EMAIL FAILED");

                } else {
                console.log('Email sent: ' + info.response);
				console.log("#############################################EMAIL SENT");
                }
            });
            return res
            .status(200)
            .send("Sähköposti lähetetty")
        }
        else{
            console.log("email not found")
            return res
            .status(404)
            .send("Sähköpostiosoitetta ei löytynyt")
        };
}

const resetPassword = async(req, res) => { 
    
    const user = await User.findOne({ resettoken: req.body.token});
        if (user !== null){
            const timenow = Date.now();
            if(timenow < user.resettokenexpiration){
                bcryptjs.hash(req.body.newPassword, saltRounds, function (err, hash) {
                    if (err){console.log(err)};
                    user.password = hash;
                    user.resettoken = "";
                    user.resettokenexpiration = "";
                    user.save();
                    console.log("Success");
                });     
                return res
                .status(200)
                .send("Salasana vaihdetaan")
            }
            else{
                console.log("Old token");
                return res
                .status(400)
                .send("Salasanalinkki väärin tai vanhentunut");
            };
        }
        else{
            console.log("not valid url")
            return res
            .status(400)
            .send("Salasanalinkki väärin tai vanhentunut");
        };
};

const isThereToken = async(req, res) => { 

    const user = await User.findOne({ resettoken: req.body.token});
        if (user !== null){
            res.status(200).send("found")
        }
        else{
            res.status(404).send("no");
        };

};

export {
    changeUserPassword,
    changeUserEmail,
    resetPasswordEmail,
    resetPassword,
    isThereToken,
}