import mongodb from "mongodb";
import { uploadFolder, stringToBool, imgCropHandler, campaignPriceRemover, errorMessages } from "../utils.js";
import { Services } from "../models/index.js";
import { myXSS } from "../utils.js";

async function getServices (req,res) {
    const services = req.authed 
    ? await Services.find({})
    : await Services.find({public: "true", _deleted: {$ne: "true"}})
    res.status(200).send(services);
}

async function createService (req,res) {
    let summerNum, winterNum;
    // get all season order numbers from db
    const seasonNumbers = await Services.find({}, "summerNum winterNum");
    // simplify the order numbers into 1d arrays
    const summerNumbers = seasonNumbers.map(season => season.summerNum);
    const winterNumbers = seasonNumbers.map(season => season.winterNum);
    // get the first number not in each array and choose that as our new number
    for (let i = 1; i <= seasonNumbers.length; i++) {
        if (!summerNumbers.some(num => num === i)) {
            summerNum = i;
        }
        if (!winterNumbers.some(num => num === i)) {
            winterNum = i;
        }
    }
    // if there were no free gaps within the array length, give length+1
    if (!summerNum) {
        summerNum = seasonNumbers.length + 1;
    }
    if (!winterNum) {
        winterNum = seasonNumbers.length + 1;
    }

    let result;
    let imgName = ""; // default in case of bug
    const imageInfo = JSON.parse(req.body.serviceImageInfo);
    const crop = JSON.parse(imageInfo.crop);

    if (req.file) {
        imgName = req.file.filename;

        // if the user doesn't crop, width and height are set as zero.
        // so if they are, let's not crop here either
        if (!(crop.width === 0 || crop.height === 0)) {
            // because react-image-crop's crop values are based on the scaled-down image preview canvas,
            // we must multiply them by the scaling values to match the original image again
            const scaling = JSON.parse(imageInfo.scaling);
            console.log(crop);
            console.log(scaling);
    
            await imgCropHandler(imgName, crop, scaling, req.body.serviceImageInfo.isNewImg)
            .then(function(res) {
                console.log(res);
                result = res;
                if(result && typeof result !== "object") {
                    imgName = result;
                }
            })
            .catch(error => {
                result = error;
            })
            console.log("Result in for-loop");
            console.log(result);
        }
    } else if (crop) {
        // if image info stuff is not null but there is no image, it means multer rejected our image because of disk size (or filetype (todo)) issue
        result = {
            error: errorMessages.imgRejected
        };
    } else  {
        // no service image
        result = true;
    }

    let campaignPrice;
    if (req.body.campaignPrice) {
        campaignPrice = req.body.campaignPrice;
        await campaignPriceRemover(campaignPrice);
    }

    let newService = {
        serviceName: req.body.serviceName.trim(),
        public: stringToBool(req.body.public),
        servicePrice: req.body.servicePrice,
        campaignPrice: campaignPrice,
        campaignText: req.body.campaignText,
        serviceDescription: myXSS.process(req.body.content),
        serviceimage: imgName,
        summerNum: summerNum,
        winterNum: winterNum
    }
    
    if (result) {
        if (typeof result !== "object") {
            const service = await Services.create(newService);
            res.status(200).send(service);
        } else {
            console.log(result);
            res.status(400).send(result.toString());
        }
    } else {
        res.status(400).end();
    }

}

async function updateService (req, res) {
    const findService = await Services.findOne({_id: mongodb.ObjectId(req.body._id)});
    if(!findService) {
        res.status(404).end()
        return;
    };

    let result;

    console.log(req.body);

    let imgName;

    if (req.body.serviceImageInfo) {
        const imageInfo = JSON.parse(req.body.serviceImageInfo);
        const crop = JSON.parse(imageInfo.crop);
        const scaling = JSON.parse(imageInfo.scaling);
    
        // if no image, crop & scaling are null
        if (crop && scaling) {
            console.log(crop);
            console.log(scaling);
            // if the user doesn't crop, width and height are set as zero.
            // so if they are, let's not crop here either
            if (!(crop.width === 0 || crop.height === 0)) {
                // because react-image-crop's crop values are based on the scaled-down image preview,
                // we must multiply them by the scaling values to match the original image again
    
                const isNewImg = JSON.parse(imageInfo.isNewImg);
    
                if (isNewImg) {
                    if (req.file) {
                        imgName = req.file.filename;
                        await imgCropHandler(imgName, crop, scaling, isNewImg)
                        .then(function(res) {
                            console.log(res);
                            result = res;
                            if(result && typeof result !== "object") {
                                imgName = result;
                            }
                        })
                        .catch(error => {
                            result = error;
                        })
                        console.log("Result in for-loop");
                        console.log(result);
                    } else {
                        // if no req.file, it means multer rejected img upload
                        result = {
                            error: errorMessages.imgRejected
                        };
                    }
                } else {
                    imgName = findService.serviceimage;
                    await imgCropHandler(imgName, crop, scaling, isNewImg)
                    .then(function(res) {
                        console.log(res);
                        result = res;
                        if(result && typeof result !== "object") {
                            imgName = result;
                        }
                    })
                    .catch(error => {
                        result = error;
                    })
                    console.log("Result in for-loop");
                    console.log(result);
                }
            }
        } else {
            result = true; // no image
        }
    } else { // we should probably get here only on softdelete/restore
        imgName = req.body.serviceimage;
        result = true;
    }
    
    let campaignPrice;
    if (req.body.campaignPrice) {
        campaignPrice = req.body.campaignPrice;
        await campaignPriceRemover(campaignPrice);
    }

    console.log(campaignPrice)

    let update = {
        serviceName: req.body.serviceName.trim(),
        public: stringToBool(req.body.public),
        servicePrice: req.body.servicePrice,
        campaignPrice: campaignPrice,
        campaignText: req.body.campaignText,
        serviceDescription: myXSS.process(req.body.content),
        serviceimage: imgName
    };


    // if season order numbers are given, put them in
    if (req.body.summerNum) {
        update.summerNum = req.body.summerNum;
    }
    if (req.body.winterNum) {
        update.winterNum = req.body.winterNum;
    }

    // if _deleted field exists in request, flip it
    if (req.body._deleted) {
        update._deleted = !stringToBool(req.body._deleted);
    }
    
    console.log(result);
    if (result) {
        if (typeof result !== "object") {
            const service = await Services.updateOne({_id: mongodb.ObjectId(req.body._id)}, {$set: update});
            if (service) {
                res.status(200).send(service);
            } else {
                res.status(400).end();
            }
        } else {
            console.log(result);
            res.status(400).send(result.toString());
        }
    } else {
        res.status(400).end();
    }

}

const updateServiceOrder = async(req, res) => {
    const services = JSON.parse(req.body.order);
    for(const i in services) {
        const service = services[i];
        console.log(service)
        await Services.updateOne({_id: mongodb.ObjectId(service._id)}, {$set: {
            winterNum: service.winterNum,
            summerNum: service.summerNum
        }})
    }
    res.status(200).send({});
}

async function deleteService (req,res) {
    try {
        const service = await Services.deleteOne({_id: mongodb.ObjectId(req.params.id)});
        if (service.deletedCount > 0) {
            res.status(200).send(service);
        } else {
            res.status(400).send("Palvelua ei löytynyt tällä id:llä!");
        }
    } catch (err) {
        console.log(err);
        res.status(400).send(err.toString());
    }
}

export {
    getServices, 
    createService, 
    updateService,
    updateServiceOrder,
    deleteService,
}