import { Articles, Products, Services } from "./models/index.js";
import multer from "multer";
import fs from "fs";
import sharp from "sharp";
import jwt from "jsonwebtoken";
import xss from "xss";
import path from "path";

 // for "true" and "false"
const stringToBool = (val) => {
    if (val == "true") {
        return true;
    } else {
        return false;
    }
};

// Image file types
const imgFileTypes= [".jpg", ".jpeg", ".png", ".gif"];

const uploadFolder = "./uploads";
//const upload = multer({ dest: uploadFolder }); // folder for file uploads

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, uploadFolder);
    },
    filename: function (req, file, cb) {

        const extension = path.parse(file.originalname).ext;

        // Get image files from uploads folder
        const uploadFiles = fs.readdirSync(`${uploadFolder}`)
        .filter(file => imgFileTypes.includes(path.extname(file).toLowerCase()));

        // Get image files from uploads/crop folder
        const cropFiles = fs.readdirSync(`${uploadFolder}/crop`)
        .filter(file => imgFileTypes.includes(path.extname(file).toLowerCase()));

        // merge two arrays to one variable
        const allFiles = uploadFiles.concat(cropFiles);

        // Map image files and remove file extensions from each file
        const allFilesMap = allFiles.map(img => {
            return path.parse(img).name;
        });

        // Save image file to new image file if image name already exists
        const imgNameBase = path.parse(file.originalname).name;
        console.log("currentImg", imgNameBase);

        let uniqueImgName = imgNameBase;
        let counter = 2;
        
        while (allFilesMap.find(fileName => fileName === uniqueImgName)) {
            uniqueImgName = `${imgNameBase}-${counter}`;
            counter++;
            console.log("new unique slug: " + uniqueImgName);
        }
        let newImageFile = uniqueImgName + extension;
        console.log("uusi kuva", newImageFile);
        cb(null, newImageFile);
    }
});
const upload = multer({ 
    storage: storage, 
    limits: {
        fieldSize: 5000000,
    },
    fileFilter: function (req, file, cb) {
        // checks that adding this file doesn't go over the maximum directory size
        const [usedSize, maxSize] = getFolderSize(uploadFolder);
        console.log(usedSize);
        console.log(maxSize);
        // since we can't access image's size before it's saved, let's just simply prevent upload
        // if we're [max fieldsize] bytes away from full folder. 
        if (usedSize + 5000000 < maxSize) {
            const filetype = path.parse(file.originalname).ext;
            console.log(filetype);
            if (imgFileTypes.includes(filetype)) {
                console.log("file good");
                cb(null, true);
            } else {
                console.log("invalid file type");
                cb(null, false);
            }
        } else {
            console.log("no room");
            cb(null, false);
        }
    }
});

// Custom options for XSS filtering all incoming tinyMCE data (and more)
const xssOptions = {
    whiteList: xss.whiteList, // the default whitelist of the xss module
    stripIgnoreTagBody: ["script"] // filters out inner contents between script tags
}

const myXSS = new xss.FilterXSS(xssOptions);

function slugger(title) {
    let slug = title.replace(/\s+/g, "-").toLowerCase(); // replaces spaces with hyphens, lowercases
    slug = slug.replace(/[^a-zäöå\-\d\s]+/gi, ""); // Removes all but letters, numbers and whitespace except hyphens.
    console.log("remove chars", slug);
    return slug;
}

async function getArticleSlugs (slug) {
    /* 
    finds all articles where the slug === slug or is format of slug-[any number(s)
    but nothing after number(s)] and returns those articles' slugs
    */
    console.log("getArticleSlugs", slug);
    const slugs = await Articles
        .find({}, "slug")
        .or([{slug: slug}, {slug: {$regex: new RegExp(`${slug}-\\d+$`)}}]);
    //return slugs.map(slug => slug.slug);
    return slugs;
}

async function getProductSlugs (slug) {
    const slugs = await Products
        .find({}, "slug")
        .or([{slug: slug}, {slug: {$regex: new RegExp(`${slug}-\\d+$`)}}]);
    return slugs;
}

async function determineUniqueSlug(slug, id, objtype) {
    // by default, the unique slug is just the article's title sluggified
    let uniqueSlug = slug;

    let slugs;

    // Select correct slugs by objtype
    switch(objtype) {
        case "article":
            slugs = await getArticleSlugs(slug);
            console.log("list of matching slugs:");
            console.log(slugs);
            break;
        case "product":
            slugs = await getProductSlugs(slug);
            console.log("list of matching slugs:");
            console.log(slugs);
            break;
        default:
            console.log("Object type not found");
            return false;
    } 
    
    // if this object's slug hasn't been changed since last time, let's reuse it instead of
    // treating it like we need to step over it and get the next available number
    const thisObjectExistingSlug = slugs.find(slugObj => slugObj.id === id);
    if (thisObjectExistingSlug) {
        return thisObjectExistingSlug.slug;
    }

    // otherwise, if this article has a new type of slug, we check if other articles share this same format of slug,
    // then we loop to get a new number addition to our slug starting from number 2
    let counter = 2;
    while (slugs.some(slugObj => slugObj.slug === uniqueSlug)) {
        uniqueSlug = `${slug}-${counter}`;
        counter++;
        console.log("new unique slug: " + uniqueSlug);
    }

    return uniqueSlug;
}

async function imgCropHandler(imgName, crop, scaling, isNewImg) {
    // we rename the initially uploaded whole image,
    // then save the crop with that original path,
    // then delete the renamed original

    let imgPath = `${uploadFolder}/${imgName}`;

    const extension = path.parse(imgName).ext;

    return new Promise((resolve, reject) => {

        const tempName = Math.floor(Math.random() * Math.floor(9999)); // random-ish integer name
        let tempPath = `${uploadFolder}/tmp/${tempName}${extension}`;
    
        fs.renameSync(imgPath, tempPath, (err) => { // renames file in imgPath to be tempPath
            if (err) {
                console.log("renaming error");
                console.log(err);
                reject(err);
            }
        });

        console.log(crop);
        console.log(scaling);


        // in case bug happens, if value is negative then set y value to 0
        if (crop.y < 0) {
            crop.y = 0;
        }

        // in case bug happens, if value is negative then set x value to 0
        if (crop.x < 0) {
            crop.x = 0;

        }

        // in case bug happens, limit crop dimensions to maximum of image
        if (crop.x + crop.width > scaling.scaledWidth) {
            crop.width = scaling.scaledWidth - crop.x;
            console.log("width change");
            console.log(crop);
        }

        if (crop.y + crop.height > scaling.scaledHeight) {
            crop.height = scaling.scaledHeight - crop.y;
            console.log("height change");
            console.log(crop);
        }

        console.log("computed crop (width, height, x, y)");
        console.log(Math.round(crop.width * scaling.scaleX));
        console.log(Math.round(crop.height * scaling.scaleY));
        console.log(Math.round(crop.x * scaling.scaleX));
        console.log(Math.round(crop.y * scaling.scaleY));
    
        // Check if the image is new or updated and change image path to correct one
        let imgPath2;

        if (isNewImg) {
            imgPath2 = `crop/${imgName}`;
        } else {
            imgPath2 = `${imgName}`;
        }

        sharp(tempPath)
        .extract({ // crops the image
            width: Math.round(crop.width * scaling.scaleX),
            height: Math.round(crop.height * scaling.scaleY),
            left: Math.round(crop.x * scaling.scaleX),
            top: Math.round(crop.y * scaling.scaleY)
        })
        .withMetadata()// preserves metadata
        .toFile(`${uploadFolder}/${imgPath2}`) // saves to file with the original name
        .then(info => {
            console.log("info");
            console.log(info);
            fs.unlinkSync(tempPath, (err) => { // deletes the original file which now has random-ish int name
                if (err) {
                    console.log("unlinking error"); // aka failed to delete file
                    console.log(err);
                    reject(err);
                }
            });
            resolve(`${imgPath2}`); // if everything worked out
        })
        .catch(err => {
            console.log("cropping error");
            console.log(err);
            reject(err);
        });
        
        return `${imgPath2}`;
    });
}

async function pageImgsCrop(page, files, info) {

    const imageInfos = JSON.parse(info);
    console.log(imageInfos);

    let result;

    let newCounter = 0; // keeps track of the index of new image files

    // imageInfos.length == 8, pages.images.length == 4
    // so we need to do things in pairs
    let pairIndex = 0;

    for (let i = 0; i < imageInfos.length; i++) {
        console.log(`round ${i}`);
        console.log(`pair: ${pairIndex}`);     

        const crop = JSON.parse(imageInfos[i].crop);
        console.log(crop);
        const scaling = JSON.parse(imageInfos[i].scaling);
        console.log(scaling);

        let isSummer; // bool whether summer image or not
        // every other img is summer, every other winter
        if (i % 2 === 0) { // even (0, 2, 4...)
            isSummer = true;
            console.log(page.images[pairIndex]);
        } else { // odd (1, 3, 5...)
            isSummer = false;
            console.log(page.images[pairIndex]);
        }
        console.log(isSummer);

        if (crop && scaling) { // if neither is null, cropping has been done aka not an empty slot
            const isNew = imageInfos[i].isNewImg; // true or false
          
            let imgName;

            if (isNew) {
                if (files[newCounter]) {
                    console.log(`new # ${newCounter}`);
                    imgName = files[newCounter].filename;
                    newCounter++; // increment the number of new items so far by one
                } else {
                    // multer rejected file upload
                    result = {
                        error: errorMessages.imgRejected
                    };
                }
            } else {
                // img name info in array stays the same
                isSummer ? imgName = `${page.images[pairIndex].summer}`
                : imgName = `${page.images[pairIndex].winter}`
            }
                
            console.log(imgName);

            if (crop.width !== 0 && crop.height !== 0) {
                await imgCropHandler(imgName, crop, scaling)
                .then(function(res) {
                    console.log(res);
                    result = res;
                })
                .catch(error => {
                    result = error;
                })
                console.log("Result in for-loop");
                console.log(result);
            } else {
                // If image is not cropped then assign imgName to result variable
                result = imgName;
            }

            isSummer ? page.images[pairIndex].summer = result
            : page.images[pairIndex].winter = result

        } else {
            console.log("no image");
        }

        !isSummer && pairIndex++; // if at winter, increment pair index for next round
        console.log("result at end of loop", result);
    }

    console.log("result at end of pageImgsCrop");
    console.log(result);
    return result;
}

async function productImgsCrop(product, files, info) {
    // change: instead of pushing into productimages here, return a copy of productimages
    // and do the operation in parent function? mongodb compatibility etc?

    const imageInfos = JSON.parse(info);
    console.log(imageInfos);

    let result;

    // copy the img array and empty it.
    // then at the end the array gets refilled with all the handled data
    const productImagesCopy = product.productImages;
    product.productImages = [];

    let newCounter = 0; // keeps track of the index of new image files

    for (let i = 0; i < imageInfos.length; i++) {
        console.log(`round ${i}`);

        const crop = JSON.parse(imageInfos[i].crop);
        console.log(crop);
        const scaling = JSON.parse(imageInfos[i].scaling);
        console.log(scaling);

        if (crop && scaling) { // if neither is null
            const isNew = imageInfos[i].isNewImg; // true or false
            console.log(`new: ${isNew}`);
            if (crop.width !== 0 && crop.height !== 0) {

                let imgName;

                if (isNew) {
                    if (files[newCounter]) {
                        console.log(`new # ${newCounter}`);
                        imgName = files[newCounter].filename;
                        newCounter++; // increment the number of new items so far by one
                        await imgCropHandler(imgName, crop, scaling)
                        .then(function(res) {
                            console.log(res);
                            result = res;

                            if(result && typeof result !== "object") {
                                product.productImages.push(result);
                                console.log("Result in for-loop");
                                console.log(result);
                            }
                        })
                        .catch(error => {
                            result = error;
                        })
                        console.log("isNew result");
                        console.log(result);
                    } else {
                        // multer rejected file upload
                        result = {
                            error: errorMessages.imgRejected
                        };
                    }
                } else {
                    //imgPath = `${uploadFolder}/${productImagesCopy[i]}`;
                    imgName = `${imageInfos[i].filename}`;
                    await imgCropHandler(imgName, crop, scaling)
                    .then(function(res) {
                        console.log(res);
                        result = res;

                        if(result && typeof result !== "object") {
                            product.productImages.push(result);
                            console.log("Result in for-loop");
                            console.log(result);
                        }
                    })
                    .catch(error => {
                        result = error;
                    })
                    console.log("Else result other than crop.width !== 0 && crop.height !== 0");
                    console.log(result);
                }
                
                console.log(imgName);

            }
        }
    }

    console.log("result at end of productImgsCrop");
    console.log(result);
    return result;
}
//Verifies the request. Finds a cookie that contains JWT created in login function.
//if jwt.verify returns an error, it makes a 401 response, which goes to
//client AuthenticatedRequest, which moves user to loginscreen.
//uses jsonwebtoken & cookie-parser
const authJWT = (req, res, next) => {
    // console.log("--------------------------------------------------------------------------------------------------------------------------------")
    //Get the jwt created in login


    const token = req.cookies['sitecookie'];

	
    // console.log("values from cookie " + token);
    //verify the jwt from cookie with .env secret
    jwt.verify(token, process.env.JWTKEY, (err) => { 
    if (err) {
		
        return res
			// next();
            .status(401)
            // // //some things break if they don't get to eat an array response 
            .send([]);
    }
        //Add time to JWT expiration every time user does a thing that needs AUTH
        const jwtDecoded = jwt.verify(token, process.env.JWTKEY);
        //Consider changing this timevalue in login controller too. Last brackets are seconds.
        jwtDecoded.exp = Math.floor(Date.now() / 1000) + (60*60);
        const jwtToken = jwt.sign(jwtDecoded, process.env.JWTKEY);
        // remember secure: true when HTTPS!-----------------------------------
        res.cookie("sitecookie", jwtToken, {httpOnly: true, secure: false,})
        next();
    });

};

const authJWTget = (req, res, next) => {
	

    const token = req.cookies['sitecookie'];
    jwt.verify(token, process.env.JWTKEY, (err) => { 
        if (err) {
            req.authed=false;
        }
        else {
            req.authed=true;
        }
        next();
    });

};

async function campaignPriceRemover() {
    // removes campaign price from all other products and services
    // for each product, if campaign price is not null, set it to undefined
    console.log("number of campaign price changes:");

    const productCamps = await Products.updateMany({ "campaignPrice":{$ne:null}}, { "campaignPrice": undefined });
    console.log(productCamps.n); // number of matches
    console.log(productCamps.nModified); // number of modified

    const serviceCamps = await Services.updateMany({ "campaignPrice":{$ne:null}}, { "campaignPrice": undefined });
    console.log(serviceCamps.n);
    console.log(serviceCamps.nModified);

    return;
}

function getFolderSize(directory) {
    const getAllFiles = (dirPath, arrayOfFiles) => {
        const files = fs.readdirSync(dirPath)
        arrayOfFiles = arrayOfFiles || []

        files.forEach(file => {
            if (fs.statSync(dirPath + "/" + file).isDirectory()) {
                arrayOfFiles = getAllFiles(dirPath + "/" + file, arrayOfFiles)
              } else {
                  arrayOfFiles.push(path.join(dirPath, file))
                }
        })
        return arrayOfFiles
    }

    const getTotalSize = directoryPath => {
        const arrayOfFiles = getAllFiles(directoryPath)
        let totalSize = 0;
        
        arrayOfFiles.forEach(filePath => totalSize += fs.statSync(filePath).size)
        return totalSize;
    }
    
    const maxSize = 1120000000; // determine elsewhere?
    const result = getTotalSize(directory);
    //const result = 1119999980;
    
    return [result, maxSize];
}

const convertBytes = bytes => {
    const sizes = ["Bytes", "KB", "MB", "GB", "TB"];
    if(bytes === 0) {
        return "n/a";
    }
    
    const i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    if(1 === 0) {
        return bytes + " " + sizes[i];
    }
    return (bytes / Math.pow(1024, i)).toFixed(1) + " " + sizes[i];
}

const errorMessages = {
    imgRejected: "Kuvan tallennus epäonnistui! Todennäköinen syy: Väärä tiedostopääte tai palvelimen tila täynnä!",
}


export { storage, upload, uploadFolder, stringToBool, myXSS, slugger, determineUniqueSlug, imgCropHandler, pageImgsCrop, productImgsCrop, campaignPriceRemover, getFolderSize, convertBytes, authJWT, authJWTget, errorMessages, imgFileTypes };
