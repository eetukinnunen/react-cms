import express from "express";
import bodyParser from "body-parser";
import cors from "cors";
import cookieParser from "cookie-parser";
import apiRouter from "./api/api.router.js";
import {connectDb} from "./models/index.js";
import dotenv from "dotenv";
import morgan from "morgan";
const config = dotenv.config().parsed;

const app = express();

app.use(morgan('combined'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// app.use(function(req, res, next) {  
      // res.header('Access-Control-Allow-Origin', req.headers.origin);
      // res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
      // next();
// });

//app.use(cors({credentials: true, origin: config["CORS_ORIGIN"]})); // this can be used in production on a server, when you have one origin for everything

// on localhost, let's do this instead:
const whitelist = ["http://localhost:3000", "http://localhost:3001"];
const corsOptions = {
    credentials: true,
    origin: function (origin, callback) {
        console.log("origin", origin);
        if (whitelist.indexOf(origin) !== -1) {
            callback(null, true);
        } else {
            callback(new Error("Not allowed by CORS"));
        }
    }
};
app.use(cors(corsOptions));

app.use(cookieParser());

const port = config["PORT"];

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
app.get("/", (req, res) => {
    res.status(200).send("testi!");
});

app.use("/api", apiRouter);

// Return 404 status code
/*app.use(function(req, res, next){
    res.status(404);
  
    // respond with html page
    if (req.accepts("html")) {
      res.send("404");
      return;
    }
  
    // respond with json
    if (req.accepts("json")) {
      res.send({ error: "Not found" });
      return;
    }
  
    // default to plain-text. send()
    res.type("txt").send("Not found");
  });*/

connectDb().then(async () => {
    app.listen(port, 'localhost', () => console.log(`Backend API listening on port ${port}!`));
});
