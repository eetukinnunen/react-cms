import nodemailer from "nodemailer";
import dotenv from "dotenv";
import User from "./models/users.model.js";
const config = dotenv.config().parsed;
console.log(config);

async function productMail(postData) {
	const admin = await User.findOne({username: "Test"});
    const mailOptions = {
        from: "Website Server",
        to: admin.email,
        subject: `Tiedustelu: ${postData.product}`,
        text:   `${postData.product} - ${postData.price} € \n` +
                `Asiakas: ${postData.customerName} - ${postData.customerEmail} \n` +
                `Lisäosat: ${postData.addons.map(addon => " "+addon.addonName+" - "+addon.addonPrice+" €")} \n` +
                `Viesti: ${postData.message}`
    };

    console.log(mailOptions);
    const result = await sendEmail(mailOptions);
    return result;
}

async function serviceMail(postData) {
	const admin = await User.findOne({username: "Test"});
    const mailOptions = {
        from: "Website Server",
        to: admin.email,
        subject: `Tiedustelu: ${postData.service}`,
        text:   `Asiakas: ${postData.customerName} - ${postData.customerEmail} \n` +
                `Viesti: ${postData.message}`
    };

    console.log(mailOptions);
    const result = await sendEmail(mailOptions);
    return result;
}

async function contactMail(postData) {
	const admin = await User.findOne({username: "Test"});
    const mailOptions = {
        from: "Website Server",
        to: admin.email,
        subject: `Yhteydenotto`,
        text:   `Asiakas: ${postData.customerName} - ${postData.customerEmail} \n` +
                `Viesti: ${postData.message}`
    };

    console.log(mailOptions);
    const result = await sendEmail(mailOptions);
    return result;
}

async function sendEmail(options) {

    return new Promise((resolve, reject) => {

        const transporter = nodemailer.createTransport({
            service: "gmail",
            auth: {
                user: config["emailUserName"],
                pass: config["emailPassword"]
            }
        });

        transporter.sendMail(options, function(error, info){
            if (error) {
                console.log(error);
                resolve(false);
            } else {
                console.log('Email sent: ' + info.response);
                resolve(true);
            }
        });

    });

}


export {productMail, serviceMail, contactMail};
