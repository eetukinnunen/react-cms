import express from "express";
import { getImages, getImagesFolderSize, imagesLocation, uploadImage, deleteImage} from "../controllers/images.controller.js";
import {upload, authJWT} from "../utils.js";

const router = express.Router();

router.get("/get", authJWT, getImages);
router.get("/foldersize", authJWT, getImagesFolderSize);

router.post("/location", imagesLocation);
router.post("/imageupload", upload.single("imagefile"), uploadImage);

router.delete("/delete", deleteImage);

export default router;