import express from "express";
import {login, logout, check} from "../controllers/login.controller.js";
const router = express.Router();

router.get("/logout", logout);
router.get("/check", check);
router.post("/", login);

export default router;