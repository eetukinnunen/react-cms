import express from "express";
import {getArticle, createArticle, updateArticle, deleteArticle} from '../controllers/articles.controller.js';
import {upload, authJWT, authJWTget} from "../utils.js";
const router = express.Router();



router.get("/get",authJWTget, getArticle);
router.get("/get/admin",authJWTget, authJWT, getArticle);

router.post("/create", authJWT, upload.single("articleimage"), createArticle );
router.patch("/update", authJWT, upload.single("articleimage"), updateArticle );
router.delete("/delete/:id", authJWT, deleteArticle );

export default router;