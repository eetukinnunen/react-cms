import express from "express";
import { createCategory, updateCategory, deleteCategory, updateCategoryOrder } from "../controllers/categories.controller.js";
import {upload, authJWT} from "../utils.js";

const router = express.Router();

router.post("/create", authJWT, upload.none(), createCategory);
router.patch("/update", authJWT, upload.none(), updateCategory);
router.patch("/reorder", authJWT, upload.none(), updateCategoryOrder);
router.delete("/delete/:id", authJWT, deleteCategory);

export default router;