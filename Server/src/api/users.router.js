import express from "express";
import {changeUserPassword, changeUserEmail, resetPasswordEmail, resetPassword, isThereToken} from "../controllers/users.controller.js";
import {authJWT} from "../utils.js";
const router = express.Router();

router.patch("/changepassword", authJWT, changeUserPassword);
router.patch("/changeemail", authJWT, changeUserEmail);
router.patch("/resetpasswordEmail", resetPasswordEmail);
router.patch("/resetpassword", resetPassword);
router.patch("/checkreseturl", isThereToken)

export default router;