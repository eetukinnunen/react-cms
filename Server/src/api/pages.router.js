import express from "express";
import { getPages, getPage, updatePage } from "../controllers/pages.controller.js";
import {upload, authJWT} from "../utils.js";

const router = express.Router();

router.get("/get", getPages);

router.get("/get/:id", getPage);

router.patch("/update", authJWT,  upload.array("image"), updatePage);

export default router;