import express from "express";
import { getServices, createService, updateService, updateServiceOrder, deleteService } from "../controllers/services.controller.js";
import {upload, authJWT, authJWTget} from "../utils.js";

const router = express.Router();

router.get("/get", authJWTget, getServices);
router.get("/get/admin", authJWTget, authJWT, getServices);

router.post("/create", authJWT, upload.single("serviceimage"), createService );
router.patch("/update", authJWT, upload.single("serviceimage"), updateService );
router.patch("/reorder", authJWT, upload.none(), updateServiceOrder);
router.delete("/delete/:id", authJWT, deleteService );

export default router;