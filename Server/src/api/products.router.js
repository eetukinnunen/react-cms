import express from "express";
import {getProducts, productsById, createProduct, updateProduct, deleteProduct} from "../controllers/products.controller.js";
import {upload, authJWT, authJWTget} from "../utils.js";

const router = express.Router();

router.get("/get", authJWTget, getProducts);
router.get("/get/admin", authJWTget, authJWT, getProducts);

router.get("/get/:id", authJWT, productsById);
router.post("/create", authJWT, upload.array("productimage"), createProduct);
router.patch("/update", authJWT, upload.array("productimage"), updateProduct);
router.delete("/delete/:id", authJWT, deleteProduct);

export default router;