import express from "express";
import articleRouter from './articles.router.js';
import productRouter from "./products.router.js";
import categoriesRouter from "./categories.router.js";
import pagesRouter from "./pages.router.js";
import servicesRouter from "./services.router.js";
import userRouter from "./users.router.js";
import formRouter from "./form.router.js";
import imagesRouter from "./images.router.js";
import loginRouter from "./login.router.js";
import {upload, uploadFolder, getFolderSize, convertBytes, authJWT, authJWTget} from "../utils.js";

const app = express.Router();

app.use("/login", loginRouter);

app.use("/pages", pagesRouter);

app.use("/articles", articleRouter);

app.use("/products", productRouter);

app.use("/categories", categoriesRouter);

app.use("/services", servicesRouter);

app.use("/user", userRouter);

app.use("/images", imagesRouter);

app.use("/form", formRouter);

app.use(express.static(uploadFolder));

export default app;