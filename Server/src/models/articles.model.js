import mongoose from "mongoose";

const ArticleSchema = new mongoose.Schema({
    title: String,
    slug: String,
    date: String,
    public: Boolean,
    content: String,
    image: String,      // image name
    tags: [String],     // not in use
    _deleted: Boolean   // whether the article is in trash bin
});

const Articles = mongoose.model("articles", ArticleSchema);

export default Articles;