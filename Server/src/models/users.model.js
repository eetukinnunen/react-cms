import mongoose from "mongoose";

const UserSchema = new mongoose.Schema ({
    username: String,
    email: String,
    password: String,
    resettoken: String,
    resettokenexpiration: String
});

const User = mongoose.model("users", UserSchema);
export default User;