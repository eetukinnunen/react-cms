import mongoose from "mongoose";
import Articles from "./articles.model.js";
import Products from "./products.model.js";
import Categories from "./categories.model.js";
import Pages from "./pages.model.js";
import Services from "./services.model.js";
import User from "./users.model.js";
import dotenv from "dotenv";
const config = dotenv.config().parsed;

const DB_USER = config["DB_USER"];
const DB_PASS = config["DB_PASS"];
const DB_HOST = config["DB_HOST"];
const DB_PREFIX = config["DB_PREFIX"];

const connectDb = () => {
    return mongoose.connect(`${DB_PREFIX}://${DB_USER}:${DB_PASS}@${DB_HOST}`, { useNewUrlParser: true } );
};


export {connectDb, Articles, Products, Categories, Pages, Services, User};