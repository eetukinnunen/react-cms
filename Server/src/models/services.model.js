import mongoose from "mongoose";

const ServicesSchema = new mongoose.Schema({
    serviceName: String,
    public: Boolean,
    servicePrice: String,
    campaignPrice: String,
    campaignText: String,
    serviceDescription: String,
    serviceimage: String,
    summerNum: Number,
    winterNum: Number,
    _deleted: Boolean
});

const Services = mongoose.model("services", ServicesSchema);

export default Services;