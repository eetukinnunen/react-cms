import mongoose from "mongoose";

const PagesSchema = new mongoose.Schema({
    title: String,
    nav: Boolean,                   // whether the page title is in the navigation bar (practically always true)
    contents: [
        {
            name: String,
            content: String
        }
    ],
    images: [
        {
            name: String,
            summer: String,         // the image in summer
            winter: String,         // the image in winter
            defaultImg: { type: String, default: "default.jpg" }   // default in case other image fields empty
        }
    ],
    articlesperpage: String,        // only applies to news page
    previewLength: String,          // only applies to news page, length of text in article preview
    season : String,                // only applies to contact page, determines site's season
    orderNumber: String             // the page's order in navigation bar
});

const Pages = mongoose.model("pages", PagesSchema);

export default Pages;