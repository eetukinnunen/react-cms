import mongoose from "mongoose";


const ProductSchema = new mongoose.Schema({
    categoryId: mongoose.ObjectId,
    productName: String,
    slug: String,
    productBrief: String,
    productText: String,
    price: String,
    campaignPrice: String,
    campaignText: String,
    tax: String,
    addons: [
        {
            addonName: String,
            addonPrice: String
        }
    ],
    productImages: [String],
    _deleted: Boolean
});

const Products = mongoose.model("products", ProductSchema);

export default Products;