import mongoose from "mongoose";

const CategoriesSchema = new mongoose.Schema({
    catName: String,
    summerNum: Number,      // this category's order number in summer
    winterNum: Number,      // this category's order number in summer
    catDescription: String,
    _deleted: Boolean
});

const Categories = mongoose.model("categories", CategoriesSchema);

export default Categories;
