import React from "react";
import ProductCard from './ProductCard';
import { Row } from 'react-bootstrap';
import DOMPurify from "dompurify";

const ProductCategory = (props) => {
    const {category} = props;
    const sanitized = category.catDescription;
    const description = <div  dangerouslySetInnerHTML={{__html: sanitized}} />;

    return (
        <div className="product-category" id={category.catName.toLowerCase()}>
            <h2 className="category-name">{category.catName}</h2>
            {description}
            <Row>
                {category ? category.catProducts.map(
                    product => <ProductCard key={product._id} name={product.productName} 
                    price={product.price} campaignPrice={product.campaignPrice} tax={product.tax} 
                    text={product.productText} brief={product.productBrief} addons={product.addons} 
                    images={product.productImages} productId={product._id} slug={product.slug} />
                    ) : "No products"}
            </Row>

        </div>
    );
};

export default ProductCategory;
