import { useEffect } from "react";
import { useLocation } from "react-router-dom";

export default function BurgerMenuCloser() {
    const { pathname } = useLocation();
  
    useEffect(() => {
        const toggleButton = document.querySelector(".navbar-toggler");
        if (!toggleButton.classList.contains("collapsed")) {
            toggleButton.click();
        }
    }, [pathname]);
  
    return null;
}