import React from 'react';
import conf from "./config";
import ThemeContext from "../context/theme.context.js";

// Bootstrap
import {
    Jumbotron,
    Container
} from 'react-bootstrap';

const HeroHeader = (props) => {
    const { content, image } = props;
    const theme = React.useContext(ThemeContext);
    
    const imgPath = !image[theme] ? image["defaultImg"]
                    :image[theme] ? image[theme]
                    :image["winter"] || image["summer"];
    
    const imgUrl = image[theme] ? `${conf.server_base_url}/${imgPath}` : `/${imgPath}`;

    return (
        <section className="header-jumbotron">
            <Jumbotron fluid className="hero-header text-center d-flex align-items-end" 
            id="hero-header" style={{backgroundImage: `url(${imgUrl})`}} 
            >
                    <div className="gradient"></div>
                    <Container>
                        <h1 className="hero-text">{content}</h1>
                    </Container>
            </Jumbotron>
        </section>
    );
};

export default HeroHeader;