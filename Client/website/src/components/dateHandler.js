// This function converts date format from YYYY-MM-DD to DD.MM.YYYY

const dateHandler = (date) => {
    const day = date.slice(8,10);
    const month = date.slice(5,7);
    const year = date.slice(0,4);

    const formattedDate = `${day.replace(/^0+/, '')}.${month.replace(/^0+/, '')}.${year}`;
    return formattedDate;
};

export default dateHandler;
