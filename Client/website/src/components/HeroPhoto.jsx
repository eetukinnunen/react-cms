import React from 'react';
import conf from "./config";
import ThemeContext from "../context/theme.context.js";

// Bootstrap
import {
    Jumbotron,
} from 'react-bootstrap';

const HeroPhoto = (props) => {
    const { order, image } = props;

    const theme = React.useContext(ThemeContext);
    const getClassName = () => {
        switch (order) {
            case "1":
                return "hero1";
            case "2":
                return "hero2";
            default:
                break;
        }
    };
    
    const imgPath = !image[theme] ? image["defaultImg"]
                    :image[theme] ? image[theme] 
                    :image["summer"] || image["winter"];
                    
    const imgUrl = image[theme] ? `${conf.server_base_url}/${imgPath}` : `/${imgPath}`;
    

    return (
        <section className="hero-image">
            <Jumbotron fluid className={getClassName(order)} 
            style={{backgroundImage: `url(${imgUrl})`}} 
            >

            </Jumbotron>
        </section>
    );
};

export default HeroPhoto;