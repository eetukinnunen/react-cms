import React from "react";
import conf from "./config";
import { Carousel } from "react-bootstrap";
import { v4 as uuidv4 } from 'uuid';

const ProductImageSlider = (props) => {
    const {images} = props;
    
    return (
        <Carousel>
            {images[0] ? images.map(
                image => <Carousel.Item key={uuidv4()}><img className="d-block w-100" 
                src={`${conf.server_base_url}/${image}`} 
                />
                </Carousel.Item>
            ) : "jeejee" }

        </Carousel>
        
    );
};

export default ProductImageSlider;