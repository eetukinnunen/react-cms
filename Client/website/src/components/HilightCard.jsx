import React from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
//import { library } from '@fortawesome/fontawesome-svg-core';
//import { faSleigh, faBone, faCampground } from '@fortawesome/free-solid-svg-icons';
import * as icons from '@fortawesome/free-solid-svg-icons';
import DOMPurify from "dompurify";
// Bootstrap
import { 

} from 'react-bootstrap';

const HilightCard = (props) => {
    let { heading, content, icon } = props;
    let finalText = content;
    if (typeof window !== "undefined" ) {
        finalText = DOMPurify.sanitize(finalText);
    } else {
        // SSR
    }
    const finalContent = <div dangerouslySetInnerHTML={{__html: finalText}} />;

    const cls = icons[icon];

    return (
        <div className="text-center">
            <div className="icon-container">
                <FontAwesomeIcon icon={cls} />
            </div>
            <h3>
                {heading}
            </h3>
            {finalContent}
        </div>
    );
};

export default HilightCard;
