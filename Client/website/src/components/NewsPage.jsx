import React, {useState, useEffect, useContext} from "react";
import conf from "./config";
import SiteHelmet from './SiteHelmet';
import ArticlesContext from "../context/articles.context.js";
import { Image } from 'react-bootstrap';
import DOMPurify from "dompurify";
import LoadingSpinner from "./LoadingSpinner";
import NotFoundPage from "./NotFoundPage";
import dateHandler from "./dateHandler";
import ReactGA from "react-ga";

// Google Analytics
ReactGA.initialize('UA-176959782-1');

const NewsPage = (props) => {
    const { thisArticleSlug, thisArticle, thisArticleContent } = props;
    const articles = useContext(ArticlesContext) || [];
    //const [articles, setArticles] = useState(useContext(ArticlesContext));

    //const [article, setArticle] = useState({});
    const [article, setArticle] = useState(thisArticle);

    //const [content, setContent] = useState(thisArticleContent);
    const [content, setContent] = useState(thisArticleContent || "");

    // if (thisArticle) {
    //     //const sanitized = DOMPurify.sanitize(article.content);
    //     console.log("yes article prop");
    //     //sanitized = thisArticle.content;
    //     //setContent(<div dangerouslySetInnerHTML={{__html: sanitized}} />);
    // }

    useEffect(() => {
        if (!thisArticle && articles.length > 0) { // if not given thisArticle prop (only done on ssr) and articles array isn't empty
            const foundArticle = articles.find(article => article.slug === thisArticleSlug);
            if (foundArticle) {
                setArticle(foundArticle);
                let finalText = foundArticle.content; //todo: rename
                if (typeof window !== "undefined" ) {
                    finalText = DOMPurify.sanitize(finalText);
                } else {
                    // SSR
                }
                setContent(<div dangerouslySetInnerHTML={{__html: finalText}} />);
            } else {
                //setArticle(false);
            }
        }
    }, [articles]);

    /*useEffect(() => {
        console.log(article);
        if(article) {
            //const sanitized = DOMPurify.sanitize(article.content);
            const sanitized = article.content;
            setContent(<div dangerouslySetInnerHTML={{__html: sanitized}} />);
        }
        console.log("useeffect2");
    }, [article]);*/
    
    if (typeof window !== "undefined" ) {
        ReactGA.pageview(window.location.pathname);
    }
    
    return (
        <>
            {article ?
            <>
                <SiteHelmet title={article.title} 
                description={article.content.replace(/<\/?[^>]+(>|$)/g, "").slice(0,100)} 
                image={`http:${conf.server_base_url}/${article.image}`} 
                url={`http:${conf.client_base_url}/uutiset/${article.slug}`} 
                />
                <article className="container news-article py-3 py-lg-5 px-lg-5">
                    {article.image && <Image src={`${conf.server_base_url}/${article.image}`} 
                    fluid className="article-image"
                    />}
                        <h1>{article.title}</h1>
                        <p>{dateHandler(article.date)}</p>
                        {content}
                </article>
            </>
            :
                <LoadingSpinner className="centered" />
            }
        </>
    );
};

export default NewsPage;