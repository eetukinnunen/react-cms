import React, {useState, useEffect} from "react";
import conf from "./config";
import ServiceContext from "../context/services.context.js";
import ProductContext from "../context/products.context.js";

// Bootstrap
import { Container, Row, Col, Image, Button } from 'react-bootstrap';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    //useHistory,
    useLocation,
    useParams
  } from "react-router-dom";

const Campaign = (props) => {
    const products = React.useContext(ProductContext);
    const services = React.useContext(ServiceContext);
    
    let campaignProduct;

    products.forEach(category => {
        const tempValue = category.catProducts.find(
            product => product.campaignPrice && !product._deleted
        );
        if(tempValue) {
            campaignProduct = tempValue;
        }
    });

    function getCampaignService() {
        return services.find(service => service.campaignPrice && !service._deleted);
    }
    
    let campaignService = getCampaignService();

    const [productCampaign, setProductCampaign] = useState(campaignProduct || {});
    const [serviceCampaign, setServiceCampaign] = useState(campaignService || {});

    useEffect(() => {
        if (campaignService) {
            setServiceCampaign(campaignService);
        } else if (campaignProduct) {
            setProductCampaign(campaignProduct);   
        }
        
    }, [services, products]);


    const readMore = () => {
        if (campaignService) {
            return `/palvelut#${serviceCampaign.serviceName}`;
        } else if (campaignProduct) {
            return `/tuotteet/${productCampaign.slug}`;
        }
    };

    const campaignImageUrl = () => {
        let imgPath;
        if (serviceCampaign.serviceimage) {
            imgPath = conf.server_base_url + "/" + serviceCampaign.serviceimage;
        } else if (productCampaign.productImages) {
			if ( productCampaign.productImages.length > 0) {
				imgPath = conf.server_base_url + "/" + productCampaign.productImages[0];
			} else {
				imgPath = "./no-image.jpg";
			}
        } else {
			imgPath = "./no-service.jpg";
		};

        return imgPath;
    };

    if (campaignProduct || campaignService) {
        
        return (
            <section className="campaign frontpage-campaign">
                <Container>
                    <Row>
                        <Col xs={12} lg={6}>
                            <Image src={campaignImageUrl()} fluid />
                        </Col>
                        <Col xs={12} lg={6}>
                            <h1>Tarjous!</h1>
                            <h2 className="campaign-upper-h2">{productCampaign.productName}
                            {serviceCampaign.serviceName}&nbsp;
                            <s>{productCampaign.price}{serviceCampaign.servicePrice}€</s>
                            </h2>
                            <h2 className="campaign-lower-h2">Nyt vain&nbsp;
                                <span className="campaign-price">
                                    {productCampaign.campaignPrice}{serviceCampaign.campaignPrice}€!
                                </span>
                            </h2>
                            <p>{productCampaign.campaignText}{serviceCampaign.campaignText}</p>
                            <Link to={readMore().toLowerCase()} 
                            className="btn btn-lg theme-button btn-primary campaign-button"
                            >
                                Lue lisää!
                            </Link>
                        </Col>
                    </Row>
                </Container>
            </section>
        );

    }  else {
        return (
            <>
            </>
        );
    }
};

export default Campaign;
