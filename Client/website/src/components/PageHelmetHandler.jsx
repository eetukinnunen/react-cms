import React from "react";
import conf from "./config";
import ThemeContext from "../context/theme.context.js";
import SiteHelmet from "./SiteHelmet";
import useCurrentPage from "../context/currentPage.context.js";


const PageHelmetHandler = (props) => {
    let {image, description, title, url} = props;
    const page = useCurrentPage("Etusivu");
    const theme = React.useContext(ThemeContext);

    const imgPath = !image[theme] ? image["defaultImg"]
                    :image[theme] ? image[theme] 
                    :image["summer"] || image["winter"];

    const imgUrl = image[theme] ? `http:${conf.server_base_url}/${imgPath}` : `/${imgPath}`;

    const strippedDescription = description.replace(/<\/?[^>]+(>|$)/g, "");

    return (
        <SiteHelmet title={title} image={imgUrl} 
        description={strippedDescription} url={"http:" + url}
        />
    );
};

export default PageHelmetHandler;