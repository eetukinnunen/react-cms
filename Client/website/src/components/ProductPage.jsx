import React, {useState, useEffect, useContext} from "react";
import conf from "./config";
import SiteHelmet from './SiteHelmet';
import ProductImageSlider from "./ProductImageSlider";
import { v4 as uuidv4 } from 'uuid';
import { Col, Row, Image, Button } from 'react-bootstrap';
import FormedForm from "./FormedForm";
import DOMPurify from "dompurify";
import ProductsContext from "../context/products.context";
import LoadingSpinner from "./LoadingSpinner";
import ReactGA from "react-ga";

// Google Analytics
ReactGA.initialize('UA-176959782-1');

const Checkbox = (props) => {
    const {onToggle, addonName, toggled} = props;
    const [isChecked, setIsChecked] = useState(toggled);
    
    const handleChange = (e) => {
        setIsChecked(!isChecked);
        onToggle(addonName, !isChecked);
    };
    
    return (
        <input type="checkbox" name="addon" onChange={handleChange} checked={isChecked} />
    );
};

function Addons(props) {
    const {addons, onToggle} = props;
    
    return (
        <ul>
           {addons.map(addon => 
            <li key={uuidv4()}>
                <label className="mr-3">
                    <Checkbox 
                        onToggle={onToggle} 
                        toggled={addon.toggled} 
                        addonName={addon.addonName} 
                    />
                </label>
                    {addon.addonName}, hinta {addon.addonPrice}€
            </li>)}
        </ul>
    );
}

const ProductPage = (props) => {
    //const {name, price, campaignPrice, tax, brief, text, images, slug} = props;
    const {thisProductSlug, thisProduct, thisProductText, thisProductPrice, thisProductCampaignPrice, thisProductFirstImage} = props;

    function getSingleProduct() {
        for (const category of products) {
            const tempValue = category.catProducts.find(product => product.slug === thisProductSlug);
            if (tempValue) {
                return tempValue;
            }
        }
        return false;
    }

    const products = useContext(ProductsContext) || [];
    const [product, setProduct] = useState(thisProduct);
    const [addons, setAddons] = useState();
    const [totalPrice, setTotalPrice] = useState(thisProductCampaignPrice || thisProductPrice);
    const [defaultImage, setDefaultImage] = useState(`${conf.server_base_url}/${thisProductFirstImage}` || "/no-image.jpg");
    const [productText, setProductText] = useState(thisProductText);
    
    /*useEffect(() => {
        if (campaignPrice) {
            setTotalPrice(campaignPrice);
        }
    }, []);*/

    useEffect(() => {
        if (products.length > 0) {
            const productget = getSingleProduct();
            if (productget) {
                setProduct(productget);
            }
        }
    }, [products]);

    useEffect(() => {
        if (product) {
            if (product.productImages.length > 0) {
                setDefaultImage(`${conf.server_base_url}/${product.productImages[0]}`);
            } else {
                setDefaultImage("/no-image.jpg");
            }
            
            //const sanitized = DOMPurify.sanitize(product.productText);
            let finalText = product.productText;
            if (typeof window !== "undefined" ) {
                finalText = DOMPurify.sanitize(finalText);
            } else {
                // SSR
            }
            setProductText(
                <div className="product-modal-text" dangerouslySetInnerHTML={{__html: finalText}} />
            );

            if (product.campaignPrice) {
                setTotalPrice(product.campaignPrice);
            } else {
                setTotalPrice(product.price);
            }
            setAddons(product.addons);
        }
    }, [product]);

    // Count total price to product when addon is toggled
    const toggleAddon = (addonName, toggled) => {
        let total;
        product.campaignPrice 
            ? total = parseFloat(product.campaignPrice) 
                : total = parseFloat(product.price);
        setAddons(addons.map(addon => {
            if(addon.addonName === addonName) {
                addon.toggled = toggled;
            }
            if (addon.toggled) {
                total += parseFloat(addon.addonPrice);
            }
            return addon;
        }));
        setTotalPrice(Math.round(total * 100) / 100);
    };
	if (typeof window !== "undefined" ) {
		ReactGA.pageview(window.location.pathname);
	}
    return (
        <>
        {product ?
            <>
                <SiteHelmet title={product.productName} 
                image={`http:${conf.server_base_url}/${product.productImages[0]}`} 
                url={`http:${conf.client_base_url}/tuotteet/${product.slug}`} 
                description={product.productBrief} 
                />
                <div className={`${product.campaignPrice 
                        ? "campaign" : ""} container py-3 py-lg-5 px-lg-5 mt-4 product-modal`}>
                    <FormedForm productName={product.productName} price={totalPrice} addons={addons}>
                        <Row>
                            <Col xs={12} lg={6}>
                                {/* If only one image or less just show it, otherwise 
                                show image carousel */}
                                {/* if no actual image, show placeholder default */}
                                {product.productImages.length <= 1 ? 
                                    <Image src={defaultImage} /> 
                                    : <ProductImageSlider images={product.productImages}/> }
                            </Col>
                            <Col xs={12} lg={6}>
                                <h1>{product.productName}</h1>
                                <h4 className="price">
                                {/* if product has campaignprice, set this to simple price, 
                                otherwise totalprice. if no price at all, only the text */}
                                    {product.price ?
                                        `${product.campaignPrice ? product.price : totalPrice}€`
                                    : "Pyydä tarjous"}
                                </h4>
                                {(product.price && product.tax) 
                                    ? <span className="tax">sis. alv {product.tax}%</span> 
                                        : ""}
                                {product.campaignPrice 
                                    ? <h4 className="campaign-price">{`${totalPrice}€`}</h4> 
                                        : ""}
                                {productText}

                                {/* Check if there are any addons to show */}
                                {product.addons.length ?(
                                    <>
                                        <h3>Lisäosat</h3>
                                        <Addons addons={product.addons} onToggle={toggleAddon} />
                                    </>) : <></>
                                }

                            </Col>
                        </Row>

                        <Row>
                            <Col xs={12}>
                                <h4 className="form-header">Lähetä tiedustelu tuotteesta</h4>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={12} lg={6}>
                                <div className="form-group">
                                    <label htmlFor="email">Sähköpostiosoite</label>
                                    <input type="email" className="form-control" id="email" 
                                    name="email" placeholder="Sähköpostiosoite" required
                                    />
                                    <label htmlFor="name">Nimi</label>
                                    <input type="text" className="form-control" id="name" 
                                    name="senderName" placeholder="Nimi" required 
                                    />
                                
                                </div>
                            </Col>
                            <Col xs={12} lg={6}>
                                <div className="form-group">
                                    <label htmlFor="message">Viesti</label>
                                    <textarea className="form-control" id="message" 
                                    name="message" rows="4" placeholder="Kirjoita viestisi tähän">
                                    </textarea>
                                </div>
                            </Col>
                        </Row>

                        <button type="" className="btn btn-primary send-button theme-button">
                            Lähetä tiedustelu
                        </button>
                    </FormedForm>
                </div>
            </>
        : <LoadingSpinner className="centered" /> }
        </>
    );
};

export default ProductPage;