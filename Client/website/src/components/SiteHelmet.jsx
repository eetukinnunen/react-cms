import React from "react";
import Helmet from "react-helmet";


function SiteHelmet (props) {
    const {title, description, url, image} = props;

    const titleText = "Sivusto - " + title;

    return (
        <Helmet>
            <title>{titleText}</title>
            <meta property="og:title" content={titleText} />
            <meta property="og:type" content="website" />
            <meta property="og:description" content={description}/>
            <meta property="og:url" content={url} />
            <meta property="og:image" content={image} />
            <meta property="fb:app-id" content="645253199695227" />
        </Helmet>
    );
};

export default SiteHelmet;