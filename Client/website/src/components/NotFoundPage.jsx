import React from 'react';
import {Link} from 'react-router-dom';
import { 
    Container,
} from 'react-bootstrap';
import "../style.scss";

const NotFound = () => {
    return (
    <Container>
        <div className="notfoundpage">
            <h1 className="display-1">404</h1>
            <h1>Sivua ei löydy!</h1>
            <Link to ="/">
               <button className="btn btn-primary">Palaa takaisin etusivulle</button> 
            </Link>
        </div>
    </Container>
    );
};

export default NotFound;