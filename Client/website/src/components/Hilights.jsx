import React from "react";
import HilightCard from "./HilightCard";

// Bootstrap
import { 
    Container,
    Row,
    Col
} from 'react-bootstrap';

const Hilights = (props) => {
    const { heading1, content1, heading2, content2, heading3, content3 } = props;

    return (
        <section className="hilights">
            <Container>
                <Row>
                    <Col xs={12} lg={4}>
                        <HilightCard heading={heading1} content={content1} icon="faSleigh"/>
                    </Col>
                    <Col xs={12} lg={4}>
                        <HilightCard heading={heading2} content={content2}  icon="faBone"/>
                    </Col>
                    <Col xs={12} lg={4}>
                        <HilightCard heading={heading3} content={content3}  icon="faCampground"/>
                    </Col>
                </Row>
            </Container>
        </section>
    );
};

export default Hilights;