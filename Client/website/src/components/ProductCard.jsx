import React, {useState, useEffect} from "react";
import conf from "./config";
import Modal from "react-modal";
import ProductImageSlider from "./ProductImageSlider";
import { v4 as uuidv4 } from 'uuid';
import { Col, Row, Image, Button } from 'react-bootstrap';
import FormedForm from "./FormedForm";
import DOMPurify from "dompurify";

import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    //useHistory,
    useLocation,
    useParams
  } from "react-router-dom";

Modal.setAppElement("#root");

const Checkbox = (props) => {
    const {onToggle, addonName, toggled} = props;
    const [isChecked, setIsChecked] = useState(toggled);
    
    const handleChange = (e) => {
        setIsChecked(!isChecked);
        onToggle(addonName, !isChecked);
    };
    
    return (
        <input type="checkbox" name="addon" onChange={handleChange} checked={isChecked} />
    );
};

function Addons(props) {
    const {addons, onToggle} = props;
    
    return (
        <ul>
           {addons.map(addon => <li key={uuidv4()}>
            <label className="mr-3">
                <Checkbox onToggle={onToggle} toggled={addon.toggled} addonName={addon.addonName} />
            </label>{addon.addonName}, hinta {addon.addonPrice}€</li>)}
        </ul>
    );
}

function ProductCard(props) {
    const [modalIsOpen, setIsOpen] = useState(false);
    const {name, price, campaignPrice, tax, brief, text, images, slug} = props;
    const [addons, setAddons] = useState(props.addons);
    const [totalPrice, setTotalPrice] = useState(price);
    //const history = useHistory();
    
    useEffect(() => {
        if (campaignPrice) {
            setTotalPrice(campaignPrice);
        }
    }, []);
    
    /* Open correct product modal automatically if id is found and id matches slug. */ 
    let { productSlug } = useParams();

    useEffect(() => {
        if (productSlug && productSlug === slug) {
            setIsOpen(true);
            // Prevent body scrolling 
            document.body.style.top = `-${window.scrollY}px`;
            document.body.style.overflowY = "scroll";
            document.body.style.position = "fixed";
        }
    }, [productSlug]);

    // Count total price to product when addon is toggled
    const toggleAddon = (addonName, toggled) => {
        let total;
        campaignPrice ? total = parseFloat(campaignPrice) : total = parseFloat(price);
        setAddons(addons.map(addon => {
            if(addon.addonName === addonName) {
                addon.toggled = toggled;
            }
            if (addon.toggled) {
                total += parseFloat(addon.addonPrice);
            }
            return addon;
        }));
        setTotalPrice(Math.round(total * 100) / 100);
    };

    let defaultImage;
    if (images.length > 0) {
        defaultImage = conf.server_base_url + "/" + images[0];
    } else {
        defaultImage = "/no-image.jpg";
    }

    const sanitized = text;
    const productText = <div className="product-modal-text" dangerouslySetInnerHTML={{__html: sanitized}} />;

    function openModal() {
        setIsOpen(true);
        // Prevent body scrolling 
        //document.body.style.overflow = 'hidden';
        document.body.style.top = `-${window.scrollY}px`;
        document.body.style.overflowY = "scroll";
        document.body.style.position = "fixed";
    }
    function closeModal() {
        // Re-enable body scrolling when component unmounts
        //document.body.style.overflow = 'visible';
        const scrollY = document.body.style.top;

        document.body.style.position = '';
        document.body.style.top = '';

        window.scrollTo(0, parseInt(scrollY) * -1);
        // document.body.scrollTo(0, parseInt(scrollY) * -1);
        setIsOpen(false);
        //history.push("/tuotteet");
    }

    return (
        <>
            <Col xs={12} lg={6} className={`modalDiv product-card p-3 
            ${campaignPrice ? "campaign" : ""}`} /*onClick={openModal}*/
            >
                <Link to={`/tuotteet/${slug}`}>
                    <div className="padding-hack p-4">
                        <div className="img-parent">
                            <Image src={defaultImage} fluid/>
                        </div>
                        <h2>{name}</h2>
                        <h4 className="price">{price ? `${price}€` : "Pyydä tarjous"}</h4>
                        {campaignPrice ? 
                        <h4 className="campaign-price">{`${campaignPrice}€`}
                        </h4> : ""}
                        <p>{brief}</p>
                        {/* <Button size="lg" className="theme-button">Lue lisää</Button> */}
                        <Link to={`/tuotteet/${slug}`} 
                        className="theme-button btn btn-primary btn-lg"
                        >
                            Lue lisää
                        </Link>
                    </div>
                </Link>
            </Col>
            <Modal
                isOpen={modalIsOpen}
                // onAfterOpen={afterOpenModal}
                onRequestClose={closeModal}
                contentLabel="Product"
            >
                <div className={`product-modal ${campaignPrice ? "campaign" : ""}`}>
                    <div className="modalInfo">

                    </div>

                    <FormedForm productName={name} price={totalPrice} addons={addons}>
                        <Row>
                            <Col xs={12} lg={6}>
                                {/* If only one image or less just show it, 
                                otherwise show image carousel */}
                                {/* if no actual image, show placeholder default */}
                                {images.length <= 1 ? 
                                    <Image src={defaultImage} /> 
                                    : <ProductImageSlider images={images}/> }
                            </Col>
                            <Col xs={12} lg={6}>
                                <h1>{name}</h1>
                                <h4 className="price">
                                {/* if product has campaignprice, set this to simple price, 
                                otherwise totalprice. if no price at all, only the text */}
                                    {price ?
                                        `${campaignPrice ? price : totalPrice}€`
                                    : "Pyydä tarjous"}
                                </h4>
                                {(price && tax) ? <span className="tax">sis. alv {tax}%</span> : ""}
                                {campaignPrice ? <h4 className="campaign-price">{`${totalPrice}€`}
                                </h4> : ""}
                                {productText}

                                {/* Check if there are any addons to show */}
                                {addons.length ?(
                                    <>
                                        <h3>Lisäosat</h3>
                                        <Addons addons={addons} onToggle={toggleAddon} />
                                    </>) : <></>
                                }

                            </Col>
                        </Row>

                        <Row>
                            <Col xs={12}>
                                <h4 className="form-header">Lähetä tiedustelu tuotteesta</h4>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={12} lg={6}>
                                <div className="form-group">
                                    <label htmlFor="email">Sähköpostiosoite</label>
                                    <input type="email" className="form-control" id="email" 
                                    name="email" placeholder="Sähköpostiosoite" required 
                                    />
                                    <label htmlFor="name">Nimi</label>
                                    <input type="text" className="form-control" id="name" 
                                    name="senderName" placeholder="Nimi" required 
                                    />
                                
                                </div>
                            </Col>
                            <Col xs={12} lg={6}>
                                <div className="form-group">
                                    <label htmlFor="message">Viesti</label>
                                    <textarea className="form-control" id="message" name="message" 
                                    rows="4" placeholder="Kirjoita viestisi tähän">
                                    </textarea>
                                </div>
                            </Col>
                        </Row>

                        <button type="" className="btn btn-primary send-button theme-button">
                            Lähetä tiedustelu
                        </button>
                        <button className="btn btn-primary close-button theme-button" 
                        id="closeProduct" type="button" onClick={closeModal}>
                            Sulje ikkuna
                        </button>
                    </FormedForm>
                </div>
            </Modal>
        </>
    );
};

export default ProductCard;
