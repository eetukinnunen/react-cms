import React from "react";
import conf from "./config";
import Swal from "sweetalert2";

const FormedForm = (props) => {
    const {productName, price, addons, serviceName} = props;

    // The function that sends all the data to backend with recaptcha
    function submitForm(e, obj) {      

        let form = {
            name: e.target.senderName.value,
            email: e.target.email.value,
            message: e.target.message.value,
        };

        // if an object is supplied, adds it to the form object
        // for example a product name and addons object
        if (obj) {
            form = Object.assign(form, obj);
        }

        window.grecaptcha.ready(function() {
            window.grecaptcha.execute(conf.recaptcha_site_key, {action: "submit"})
            .then(function(token) {
                // if this part of recaptcha process succeeds, gives a token to be used in our backend
                form.token = token;

                fetch(`${conf.server_base_url}/form/submit`, {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                    },
                    body: JSON.stringify(form),
                })
                .then(response => {
                    if (!response.ok) {
                        return response.text().then(text => {
                            throw new Error(text);
                        });
                    } else {
                        return response.json();
                    }
                })
                .then(json => {
                    Swal.fire(
                        "Viesti lähetetty!",
                        "Kiitos viestistäsi!",
                        "success"
                    );
                })
                .catch(error => {
                    console.log(error);
                    Swal.fire(
                        "Hups!",
                        "Toiminto ei onnistunut.",
                        "error"
                    );
                });
            })
            .catch(error => console.log(error));
        });
    }

    function submitHandler(e) {
        e.preventDefault();

        if (productName) {
            const selectedAddons = [];
            if (e.target.addon) {
                if (e.target.addon.length > 1) {
                    // adds to the array the addon objects that have been selected with checkboxes
                    for (let i = 0; i < e.target.addon.length; i++) {
                        if (e.target.addon[i].checked) {
                            selectedAddons.push(addons[i]);
                        }
                    }
                } else if (e.target.addon.checked) { // this is needed because if not an array but singular, [i] syntax doesn't work
                    selectedAddons.push(addons[0]);
                }
            }
    
            const productObj = {
                productName: productName,
                price: price,
                addons: selectedAddons
            };
            submitForm(e, productObj);
        } else if (serviceName) {
            const serviceObj = {
                serviceName: serviceName
            };
            submitForm(e, serviceObj);
        } else { // if not product or service: a generic contact form
            submitForm(e);
        }
    }

    return (
        <form onSubmit={submitHandler}>
            {props.children} {/* all the form html content from 
                                the component FormedForm is called in */}
            <p className="recaptcha-disclaimer">
                This site is protected by reCAPTCHA and the Google&nbsp;
                <a href="https://policies.google.com/privacy">Privacy Policy</a> 
                and&nbsp;
                <a href="https://policies.google.com/terms">Terms of Service</a> apply.
            </p>
        </form>
    );
};

export default FormedForm;