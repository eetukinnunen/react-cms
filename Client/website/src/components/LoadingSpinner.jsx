import React from "react";


const LoadingSpinner = (props) => {

    return (
        <div className={`lds-spinner ${props.className}`}>
            <div></div><div></div><div></div><div></div><div></div><div></div>
            <div></div><div></div><div></div><div></div><div></div><div></div>
        </div>
    );
};

export default LoadingSpinner;