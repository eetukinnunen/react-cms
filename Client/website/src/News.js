import React, {useState, useEffect } from "react";
import conf from "./components/config";
import { NavLink } from "react-router-dom"; 
import HeroHeader from './components/HeroHeader';
import SiteHelmet from './components/SiteHelmet';
import useCurrentPage from "./context/currentPage.context.js";
import ArticlesContext from "./context/articles.context.js";
import { v4 as uuidv4 } from 'uuid';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleLeft, faAngleRight } from '@fortawesome/free-solid-svg-icons';
import DOMPurify from "dompurify";
import ThemeContext from "./context/theme.context.js";
import PageHelmetHandler from "./components/PageHelmetHandler";
import LoadingSpinner from "./components/LoadingSpinner";
import dateHandler from "./components/dateHandler";
import ReactGA from "react-ga";

// Bootstrap
import {
        Container,
        Image,
        Button,
        ButtonGroup,
} from 'react-bootstrap';

// Google Analytics
ReactGA.initialize('UA-176959782-1');

const Article = (props) => {
    const {article, index, charAmount} = props;

    let content;
    // removes all img elements from preview first
    article.content = article.content.replace(/<img[^>]*>/g,"");
    let finalText = article.content;
    if (typeof window !== "undefined" ) {
        finalText = DOMPurify.sanitize(finalText);
    } else {
        // SSR
    }

    // if article text is longer than maximum, cut it
    if (article.content.length > charAmount) {
        content = <div dangerouslySetInnerHTML={{__html: cutText(finalText, charAmount)+"..."}} />;
    } else {
        content = <div dangerouslySetInnerHTML={{__html: finalText}} />;
    }

    // cuts the text widhout breaking words, and closes open html tags, if any
    function cutText(text, n) {
        let short = text.substr(0, n);
        if (/^\S/.test(text.substr(n))) {
            return short.replace(/\s+\S*$/, "");
        }
        return short;
    }

    const ind = "news-" + index;

    return (
        <article className="container news-article p-5" id={ind}>
                {article.image && <Image src={`${conf.server_base_url}/${article.image}`} 
                fluid className="article-image"/>
                }
                <h2 className="article-title">{article.title}</h2>
                <h5>{dateHandler(article.date)}</h5>
                {content}
                {/* if article has no slug, no button. but maybe a different approach */}
                {article.slug && <Button href={`/uutiset/${article.slug}`} size="lg" 
                className="theme-button">Lue lisää</Button>}
        </article>
    );
};

const PageLink = (props) => {
    const {num, text, disabled} = props;
    const classList = `btn btn-lg btn-primary theme-button ${disabled ? `disabled` : ``}`;

    const [anchorTarget, setAnchorTarget] = useState(null);

    /* Set anchor target to the first new element news-0" 
       This is for smooth scrolling from pagination links to the top of the page */
    useEffect(() => {
        setAnchorTarget(document.getElementById("news-0"));
      }, []);

    const handleClick = () => {
        anchorTarget.scrollIntoView({ behavior: "smooth", block: "end" });
    };

    return (
        <NavLink 
        key={uuidv4()} 
        className={classList} 
        activeClassName="currentPage" 
        to={`/uutiset/sivu/${num}`} 
        onClick={() => handleClick()} >{text}
        </NavLink>
    );
};

const News = (props) => {
    const {pageNum} = props;
    const articles = React.useContext(ArticlesContext);
    const page = useCurrentPage("Uutiset");

    const pageNumInt = parseInt(pageNum, 10);
    
    const [pageLinks, setPageLinks] = useState([]);

    useEffect(() => {
        // the amount of pages we need is rounded up
        const pageAmount = Math.ceil(articles.filter(
            article => article.public === true).length / page.articlesperpage
        );

        // default amount
        let maxLinkAmount = 10;

        // if mobile res, show less links
        const mobileChecker = document.getElementById("mobile-checker");
        if (mobileChecker) {
            if (window.getComputedStyle(mobileChecker).display === "block") {
                maxLinkAmount = 5;
            }
        }

        // resets links to nothing in order to avoid bug
        setPageLinks([]);

        // default to all pages to links
        for (let i = 1; i <= pageAmount; i++) {
            setPageLinks(pageLinks => [...pageLinks, <PageLink num={i} text={i} key={uuidv4()} />]);
        }

        // If there are more links than maximum, slice the array
        // By default the current page is around the middle
        if (pageAmount > maxLinkAmount) {

            let sliceHelper;

            // if we're approaching the end of the array
            if ((pageNumInt - (maxLinkAmount / 2) + maxLinkAmount) > pageAmount) {
                sliceHelper = pageNumInt - (pageAmount - maxLinkAmount);
            }
            // else if we're at least above halfway
            else if (pageNumInt > maxLinkAmount / 2) {
                sliceHelper = maxLinkAmount / 2;
            }
            // else we're at or near the beginning of the array
            else {
                sliceHelper = pageNumInt;
            }

            // round things down just in case
            let sliceStart = Math.floor(pageNumInt - sliceHelper);
            let sliceEnd = Math.floor((pageNumInt - sliceHelper) + maxLinkAmount);

            setPageLinks(pageLinks => [...pageLinks.slice(sliceStart, sliceEnd)]);
        }

        // the prev & next arrow links
        setPageLinks(
            pageLinks => [<PageLink num={pageNumInt -1} 
            text={<FontAwesomeIcon icon={faAngleLeft} />} 
            disabled={pageNumInt > 1 ? false : true} key={uuidv4()} />, ...pageLinks]
        );
        setPageLinks(
            pageLinks => [...pageLinks, <PageLink num={pageNumInt + 1} 
            text={<FontAwesomeIcon icon={faAngleRight} />} 
            disabled={pageNumInt < pageAmount ? false : true} key={uuidv4()} />]
        );

    }, [articles, pageNum, page]);

    ReactGA.pageview("/uutiset");

    return (
        <>
            
            {page.contents && page.title === "Uutiset" ? (
                <>
                    <PageHelmetHandler image={page.images[0]} description="Uutiset" 
                    title={page.contents[0].content} url={conf.client_base_url + "/uutiset"}/>
                    <HeroHeader content={page.contents[0].content} image={page.images[0]} />
                    {articles ?
                    <>
                        <section className="news">
                            {/* filters only public articles, then slices only the ones that should be on this page number, then maps the articles into components */}
                            {articles.filter(article => article.public === true)
                                .slice((page.articlesperpage * (pageNum -1)), 
                                (page.articlesperpage * pageNum))
                                .map((article, index) => <Article article={article} 
                                key={article._id} index={index} charAmount={page.previewLength} 
                            />)}
                            
                            {articles.length > page.articlesperpage &&
                            
                            <Container className="pagination">
                                <ButtonGroup className="theme-pagination">
                                    {pageLinks}
                                </ButtonGroup>
                            </Container>
                            }
                        </section>


                    </>
                    : ""
                    }
                </>
            ) : <LoadingSpinner className="centered" />}
        </>
    );
};

export default News;