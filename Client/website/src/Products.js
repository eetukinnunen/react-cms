import React, { useEffect } from "react";
import { useLocation } from 'react-router-dom';
import HeroHeader from './components/HeroHeader';
import ProductCategory from './components/ProductCategory';
import SiteHelmet from './components/SiteHelmet';
import useCurrentPage from "./context/currentPage.context.js";
import ProductsContext from "./context/products.context.js";
import { v4 as uuidv4 } from 'uuid';
import { Container } from 'react-bootstrap';
import PageHelmetHandler from "./components/PageHelmetHandler";
import conf from "./components/config";
import LoadingSpinner from "./components/LoadingSpinner";
import ReactGA from "react-ga";

// Google Analytics
ReactGA.initialize('UA-176959782-1');

const Products = (props) => {
    const products = React.useContext(ProductsContext);
    const page = useCurrentPage("Tuotteet");
    const { hash } = useLocation();

    useEffect(() => {
        // if not a hash link scroll to top
        if(hash===""){
            window.scrollTo(0, 0);
        }
        // else scroll to id
        else{

            // also close the burger menu if open
            const toggleButton = document.querySelector(".navbar-toggler");
            if (!toggleButton.classList.contains("collapsed")) {
                toggleButton.click();
            }

            setTimeout(
                () => {
                    const id = hash.replace("#", "");
                    const element = document.getElementById(decodeURI(id));
                    if (element) {
                        element.scrollIntoView();
                    }
                },
                0
            );
        }
    }); // do this on route change


    let categories = [];

    for (let i = 0; i  < products.length; i++) {
        categories.push(
            <ProductCategory category={products[i]} key={uuidv4()} />
        );
    };

    ReactGA.pageview('/tuotteet');

    return (
        <div>
            <SiteHelmet title={page.title} />
            {page.contents && page.title === "Tuotteet" ? (
                <>
                    <PageHelmetHandler 
                    image={page.images[0]} 
                    description="Tuotteet" 
                    title={page.contents[0].content} 
                    url={conf.client_base_url + "/tuotteet"}/>
                    
                    <HeroHeader content={page.contents[0].content} image={page.images[0]} />
                    
                    <Container>

                        {categories}

                    </Container>
                </>
            ) : <LoadingSpinner className="centered" />}
        </div>
    );
};

export default Products;