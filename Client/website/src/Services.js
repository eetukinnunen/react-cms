import React, { useState, useEffect } from "react";
import { useLocation } from "react-router-dom";
import conf from "./components/config";
import Collapse from 'react-bootstrap/Collapse';
import FormedForm from "./components/FormedForm";
import HeroHeader from './components/HeroHeader';
import SiteHelmet from './components/SiteHelmet';
import DOMPurify from "dompurify";
import useCurrentPage from "./context/currentPage.context.js";
import ServicesContext from "./context/services.context.js";
import PageHelmetHandler from "./components/PageHelmetHandler";
import LoadingSpinner from "./components/LoadingSpinner";
import ReactGA from "react-ga";

// Bootstrap
import {
        Container,
        Button,
        Row,
        Col,
        Image
} from 'react-bootstrap';

// Google Analytics
ReactGA.initialize('UA-176959782-1');

const ServiceForm = (props) => {
    const {serviceName} = props;
    return (
        <div>
            <Row>
                <Col xs={12}>
                    <h3 className="form-header">Lähetä tiedustelu aiheesta "{serviceName}"</h3>
                </Col>
            </Row>
            <Row>
                <Col xs={12} lg={6}>
                    <div className="form-group">
                        <label htmlFor="email">Sähköpostiosoite</label>
                        <input type="email" className="form-control" name="email" 
                        placeholder="Sähköpostiosoite" required 
                        />
                        <label htmlFor="name">Nimi</label>
                        <input type="text" className="form-control" name="senderName" 
                        placeholder="Nimi" required 
                        />
                    
                    </div>
                </Col>
                <Col xs={12} lg={6}>
                    <div className="form-group">
                        <label htmlFor="message">Viesti</label>
                        <textarea className="form-control" name="message" rows="4" 
                        placeholder="Kirjoita viestisi tähän"></textarea>
                    </div>
                </Col>
            </Row>
            <Row>
                <Col xs={12}>
                    <div className="form-group">
                        <button type="" className="btn btn-primary send-button theme-button">
                            Lähetä
                        </button>
                    </div>
                </Col>
            </Row>
        </div>
    );
};

const Service = (props) => {
    const {service} = props;
    let finalText = service.serviceDescription;
    if (typeof window !== "undefined" ) {
        finalText = DOMPurify.sanitize(finalText);
    } else {
        // SSR
    }
    const content = <div dangerouslySetInnerHTML={{__html: finalText}} />;
    
    let imgPath;
    if (service.serviceimage) {
        imgPath = conf.server_base_url + "/" + service.serviceimage;
    } else {
        imgPath = "no-service.jpg";
    }

    const [open, setOpen] = useState(false);

    return (
        <Container id={service.serviceName.toLowerCase()} 
        className={`service-item p-4 p-md-5 ${service.campaignPrice ? "campaign" : ""}`}
        >
            <Image src={imgPath} fluid/>
            <h2 className="service-name">{service.serviceName}</h2>
            <h4 className="price">
                {service.servicePrice ? `${service.servicePrice}€` : "Pyydä tarjous"}
            </h4>
            {service.campaignPrice ? <h4 className="campaign-price">
                {`${service.campaignPrice}€`}
            </h4> : ""}
            <p>{content}</p>
            <Button onClick={() => setOpen(!open)}
                aria-controls="example-collapse-text"
                aria-expanded={open}
                className="theme-button btn-lg"
            >
                Lähetä tiedustelu
            </Button>
            <Collapse in={open}>
                <div id="example-collapse-text">
                    <FormedForm serviceName={service.serviceName}>
                        <ServiceForm serviceName={service.serviceName} />
                    </FormedForm>
                </div>
            </Collapse>
        </Container>

    );
};
const Services = (props) => {
    const page = useCurrentPage("Palvelut");
    const services = React.useContext(ServicesContext);

    const { hash } = useLocation();

    useEffect(() => {
        // if not a hash link scroll to top
        if(hash===""){
            window.scrollTo(0, 0);
        }
        // else scroll to id
        else {
            setTimeout(
                () => {
                    const id = hash.replace("#", "");
                    const element = document.getElementById(decodeURI(id));
                    if (element) {
                        element.scrollIntoView();
                    }
                },
                0
            );
        }
    }); // do this on route change

    ReactGA.pageview('/palvelut');

    return (
        <>
            {page.contents && page.title === "Palvelut" ? (
                <>
                    <PageHelmetHandler 
                    image={page.images[0]} 
                    description="Palvelut" 
                    title={page.contents[0].content} 
                    url={conf.client_base_url + "/palvelut"}/>
                    
                    <HeroHeader content={page.contents[0].content} image={page.images[0]} />
                        <section className="services">
                                {
                                    services ? 
                                        services
                                            .filter(service => service.public === true)
                                            .map(service => <Service key={service._id} 
                                            service={service} 
                                            />) : 
                                        "Nothing" }
                        </section>
                
                </>
            ) : <LoadingSpinner className="centered" />}
        </>
    );
};

export default Services;
