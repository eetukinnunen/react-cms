import React from "react";
import FormedForm from "./components/FormedForm";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMapMarkedAlt, faPhone, faFileAlt, faGlobe } from '@fortawesome/free-solid-svg-icons';
import {
    faFacebookSquare, 
    faInstagram, 
    faTwitter, 
    faLinkedin, 
    faTiktok, 
    faYoutube, 
    faSnapchat 
} from "@fortawesome/free-brands-svg-icons";
//import { ReactComponent as Logo } from './img/logo.jpg';
import { v4 as uuidv4 } from 'uuid';
import LoadingSpinner from "./components/LoadingSpinner";

// Bootstrap
import { 
    Container,
    Row,
    Col,
} from 'react-bootstrap';

const Footer = (props) => {
    const {contactInfo} = props;

    const someOptions = [
        {
            name: "Facebook",
            icon: faFacebookSquare
        },
        {
            name: "Instagram",
            icon: faInstagram
        },
        {
            name: "Twitter",
            icon: faTwitter
        },
        {
            name: "LinkedIn",
            icon: faLinkedin
        },
        {
            name: "Tiktok",
            icon: faTiktok
        },
        {
            name: "YouTube",
            icon: faYoutube
        },
        {
            name: "Snapchat",
            icon: faSnapchat
        },
    ];
    
    const someArray = [];
    if (contactInfo) {
        if (contactInfo.contents) {
            //for testing
            //contactInfo.push({name: "Testi", content: "#"});
            
            for (let i = 11; i < contactInfo.contents.length; i++) {
                let iconObj = someOptions.find(
                    someOption => someOption.name === contactInfo.contents[i].name
                );
                
                const someLink = {
                    name: iconObj ? contactInfo.contents[i].name : contactInfo.contents[i].content,
                    content: contactInfo.contents[i].content,
                    icon: iconObj ? iconObj.icon : faGlobe
                };
                someArray.push(someLink);
            }
        }
    }

    return (
    <>
        {contactInfo ?
            contactInfo.contents ? (

            <footer className="footer">
                <Container>
                    <Row>
                        <Col xs={12} className="text-center footer-logo-container">
                            {/* <Logo className="footer-logo"/> */}
                        </Col>
                    </Row>
                    <Row className="footer-row">
                        <Col xs={12} sm={6} lg={3}>
                            <h3>Seuraa somessa</h3>

                            {someArray.map(someLink => 
                                <a key={uuidv4()} href={someLink.content} target="_blank">
                                    <div className="d-flex social-container">
                                        <div className="info-icon-container">
                                            {}
                                            <FontAwesomeIcon icon={someLink.icon} size="2x"/>
                                        </div>
                                        <div className="info-text-container">
                                            <p>{someLink.name}</p>
                                        </div>
                                    </div>
                                </a>
                            )}
                        </Col>

                        <Col  xs={12} sm={6} lg={3}>
                            <h3>{contactInfo.contents[0].content}</h3>
                            <div className="d-flex info-container">
                                <div className="info-icon-container">
                                    <FontAwesomeIcon icon={faMapMarkedAlt} size="2x"/>
                                </div>
                                <div className="info-text-container">
                                    <p className="footer-text">
                                        {contactInfo.contents[1].content}<br/>
                                        {contactInfo.contents[2].content}<br/>
                                        {contactInfo.contents[3].content}<br/>
                                        {contactInfo.contents[4].content}
                                    </p>
                                </div>
                            </div>

                            <div className="d-flex info-container">
                                <div className="info-icon-container">
                                <FontAwesomeIcon icon={faPhone} size="2x"/>
                                </div>
                                <div className="info-text-container">
                                    <p className="footer-text">
                                        {contactInfo.contents[5].content}<br/>
                                        {contactInfo.contents[6].content}<br/>
                                        {contactInfo.contents[7].content}
                                    </p>
                                </div>
                            </div>

                            <div className="d-flex info-container">
                                <div className="info-icon-container">
                                <FontAwesomeIcon icon={faFileAlt} size="2x"/>
                                </div>
                                <div className="info-text-container">
                                    <p className="footer-text">
                                        {contactInfo.contents[8].content}<br/>
                                        {contactInfo.contents[9].content}<br/>
                                        {contactInfo.contents[10].content}
                                    </p>
                                </div>
                            </div>
                        </Col>
                            <FormedForm>
                                <Col xs={12} className="p-sm-0">
                                    <h3>Lähetä meille viesti</h3>
                                    <div className="form-group">
                                            <input 
                                            type="email" 
                                            className="form-control" 
                                            id="email" name="email" 
                                            placeholder="Sähköpostiosoite" />
                                    </div>
                                    <div className="form-group">    
                                            <input 
                                            type="text" 
                                            className="form-control" 
                                            id="name" name="senderName" 
                                            placeholder="Nimi" />    
                                    </div>
                                </Col>
                                <Col xs={12} className="p-sm-0">
                                    <div className="form-group">
                                        <textarea 
                                        className="form-control" 
                                        id="message" 
                                        name="message" 
                                        rows="4" 
                                        placeholder="Kirjoita viestisi tähän">
                                        </textarea>
                                    </div>
                                    <div className="form-group">
                                        <button 
                                        type="" 
                                        className="btn btn-primary send-button footer-button">
                                            Lähetä viesti
                                        </button>
                                    </div>
                                </Col>
                            </FormedForm>
                    </Row>
                    <Row>
                        <Col xs={12}>
                            <p className="text-center">
                                &copy; 2020 Tekijä
                            </p>
                        </Col>
                    </Row>
                </Container>
            </footer>
        ) : <LoadingSpinner className="centered" />
        : <LoadingSpinner className="centered" />}
    </>
    );
};

export default Footer;