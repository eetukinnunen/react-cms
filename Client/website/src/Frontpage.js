import React from "react";
import conf from "./components/config";
import Campaign from "./components/Campaign";
import HeroHeader from "./components/HeroHeader";
import HeroPhoto from "./components/HeroPhoto";
import Hilights from "./components/Hilights";
import Helmet from "react-helmet";
import useCurrentPage from "./context/currentPage.context.js";
import DOMPurify from "dompurify";
import PageHelmetHandler from "./components/PageHelmetHandler";
import LoadingSpinner from "./components/LoadingSpinner";
import ThemeContext from "./context/theme.context.js";
import ServiceContext from "./context/services.context.js";
import ProductContext from "./context/products.context.js";
import ReactGA from "react-ga";

// Bootstrap
import { 
    Container,
    Row,
    Col,
    Image,
} from 'react-bootstrap';

// Google Analytics
ReactGA.initialize('UA-176959782-1');

// Parallax for hero header
if (typeof window !== "undefined" ) {
    window.addEventListener('scroll', function (e) {
        let scrolled = window.pageYOffset;
        const background = document.querySelector('.hero-header');
        if (background) {
            background.style.backgroundPosition = '50% ' +  (scrolled * 0.2) + 'px';
        }
    });
}

const CompanyInfo = (props) => {
    let {content, image} = props;
    const theme = React.useContext(ThemeContext);
    let finalText = content;
    if (typeof window !== "undefined" ) {
        finalText = DOMPurify.sanitize(finalText);
    } else {
        // SSR
    }
    const finalContent = <div dangerouslySetInnerHTML={{__html: finalText}} />;

    const imgPath = !image[theme] ? image["defaultImg"]
                    :image[theme] ? image[theme] 
                    :image["summer"] || image["winter"];

    const imgUrl = image[theme] ? `${conf.server_base_url}/${imgPath}` : `/${imgPath}`;

    ReactGA.pageview("/");

    return (
        <section className="company-info">
            <Container>
                <Row>
                    <Col xs={12} lg={6}>
                        {finalContent}
                    </Col>
                    <Col xs={12} lg={6}>
                        <Image src={imgUrl} fluid/>
                    </Col>
                </Row>
            </Container>
        </section>
    );
};

const Frontpage = (props) => {
    const page = useCurrentPage("Etusivu");

    return (
        <>
        
        {page.contents ? ( // if no page found or if currently loaded page isn't this (because of async), display loading spinner
                <>
                    <PageHelmetHandler 
                    image={page.images[0]} 
                    description={page.contents[4].content} 
                    title={page.contents[0].content} 
                    url={conf.client_base_url} />
                    <HeroHeader content={page.contents[0].content} image={page.images[0]} />
                    <Campaign />
                    <Hilights 
                        content1={page.contents[1].content}
                        content2={page.contents[2].content}
                        content3={page.contents[3].content}
                        />
                    <HeroPhoto order="1" image={page.images[1]} />
                    <CompanyInfo 
                        content={page.contents[4].content}
                        image={page.images[2]}
                    />
                    <HeroPhoto order="2" image={page.images[3]} />
                {/*page.contents.map(cont => <div>{cont.content}</div>)*/}

                </>
            ) : <LoadingSpinner className="centered" />}
        </>
    );
};

export default Frontpage;
