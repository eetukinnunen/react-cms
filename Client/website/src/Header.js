import React, { useState } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
//import { ReactComponent as Logo } from './img/logo.jpg';
import PagesContext from "./context/pages.context.js";
import ProductsContext from "./context/products.context.js";
import { v4 as uuidv4 } from 'uuid';

// Bootstrap
import { 
    Container,
    Navbar,
    Nav,
} from 'react-bootstrap';

const Header = (props) => {
    const {pages} = props;
    const products = React.useContext(ProductsContext);

    return (
        <header>
            <Navigation pages={pages} products={products} />
        </header>
    );
};

const Navigation = (props) => {
    const {products} = props;
    const [menuOpen, setMenuOpen] = useState(false);

    function mobileMenuClicker() {
        // if mobile res (based on an element in footer that uses the same breakpoint as the navigation burgerification),
        // then open or close the submenu
        const mobileChecker = document.getElementById("mobile-checker");
        if (mobileChecker) {
            if (window.getComputedStyle(mobileChecker).display === "block") {
                setMenuOpen(!menuOpen);
            }
        }
    }

    const productCategories = products.map((item) =>
        //<Dropdown.Item key={uuidv4()} href={"/tuotteet#" + item.catName.toLowerCase()} value={item.catName.toLowerCase()}>{item.catName}</Dropdown.Item>
        <li key={uuidv4()}>
            <Link 
            key={uuidv4()} 
            to={"/tuotteet#" + item.catName.toLowerCase()} 
            value={item.catName.toLowerCase()} 
            className="nav-link">{item.catName}
            </Link>
        </li>
    );

    // Map function gets navigation link titles from database
    const pages = React.useContext(PagesContext);
    const pagesHTML = (pages || []).sort((a, b) => a.orderNumber - b.orderNumber).map(page => {

        if (page.title === "Tuotteet") {
            return (
                /*<Dropdown key={uuidv4()} className="nav-link">
                <Link to="/tuotteet">{page.title}</Link>
                <Dropdown.Toggle split id="dropdown-split-basic" />
                <Dropdown.Menu className="ml-4">
                    {productCategories}
                </Dropdown.Menu>
                </Dropdown>*/
                <li key={uuidv4()} className={`has-submenu ${menuOpen ? "open" : ""}`} 
                    onClick={() => mobileMenuClicker()}
                >
                    <Link to="/tuotteet" className="nav-link">{page.title}</Link>
                    <ul className="submenu" /*style={{marginTop: `-${productCategories.length * 42}px`}}*/
                    >
                        {productCategories}
                    </ul>
                </li>
            );
        } else if (page.title === "Etusivu") {
            return (
                <li key={uuidv4()}>
                    <Link key={uuidv4()} to="/" className="nav-link">{page.title}</Link>
                </li>
            );
        }

        const url = "/" + page.title.toLowerCase();

        return (
            <li key={uuidv4()}>
                <Link key={uuidv4()} to={url} className="nav-link" >{page.title}</Link>
            </li>
        );
    });

    return (
        <Navbar expand="lg" fixed="top">
            <Container>
                <Navbar.Brand href="/">
                    {/* <Logo /> */}
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <a className="sr-only sr-only-focusable" href="#maincontent">
                        Skippaa sisältöön
                    </a>
                    <Nav className="ml-auto">
                        <ul className="topmenu d-lg-flex pl-sm-0">
                            {pagesHTML}
                        </ul>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
};

export default Header;