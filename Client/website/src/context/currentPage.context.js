import React, {useEffect, useState} from "react";
import { useLocation } from "react-router-dom";
import PagesContext from "./pages.context.js";

const useCurrentPage = (pageTitle) => {
    const pages = React.useContext(PagesContext);
    const [currentPage, setCurrentPage] = useState((pages || []).find(
        page => page.title === pageTitle) || {}
    );
    const location = useLocation();
    
    
    const findPage = (pages, url) => {
        pages = pages && pages.length > 0 ? pages : [];
        let title = pageTitle;

        let page = (pages || []).find(page => page.title === title);

        if(!page && pages.length > 0) {
            return {};
        } else if (!page) {
            return {};
        }
        return page;
    };
    
    useEffect(() => {
        const page = findPage(pages, location.pathname);
        setCurrentPage(page);
    }, [pages, location]);
    
    return currentPage;
};

export default useCurrentPage;