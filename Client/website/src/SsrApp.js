import React, {useState, useEffect, useContext} from 'react';
import { BrowserRouter, StaticRouter, Switch, Route } from "react-router-dom";
import conf from "./components/config";
import Header from "./Header";
import Frontpage from "./Frontpage";
import News from "./News";
import Products from "./Products";
import Services from "./Services";
import Contact from "./Contact";
import Footer from "./Footer";
import NewsPage from "./components/NewsPage";
import NotFoundPage from "./components/NotFoundPage";
import BurgerMenuCloser from "./components/BurgerMenuCloser";
import ThemeContext from "./context/theme.context.js";
import PagesContext from "./context/pages.context.js";
import ProductsContext from "./context/products.context.js";
import ArticlesContext from "./context/articles.context.js";
import ServicesContext from "./context/services.context.js";
import ProductPage from "./components/ProductPage";
import ScrollToTop from "./components/ScrollToTop";
import DOMPurify from "dompurify";

const sortByDate = function (a, b) {
    if ( a.date < b.date ) {
        return 1;
    }
    if ( a.date > b.date ) {
        return -1;
    }
    return 0;
};

const SsrApp = ({ssrLocation, ssrArticles, ssrProducts, ssrPages, ssrServices}) => {
    const [services, setServices] = useState(ssrServices);
    const [pages, setPages] = useState(ssrPages);
    const [contactInfo, setContactInfo] = useState(pages.find(page => page.title === "Yhteystiedot"));
    const [articles, setArticles] = useState(ssrArticles);
    const [products, setProducts] = useState(ssrProducts);
    const [theme, setTheme] = useState(pages.find(page => page.title === "Yhteystiedot").season);

    const sortByTheme = (a, b) => {
        if (theme === "winter") {
            if (a.winterNum > b.winterNum) {
                return 1;
            }
            if (a.winterNum < b.winterNum) {
                return -1;
            }
            return 0;
        } 
        else if (theme === "summer") {
            if (a.summerNum > b.summerNum) {
                return 1;
            }
            if (a.summerNum < b.summerNum) {
                return -1;
            }
            return 0;
        }
    };

    function getSingleProduct(slug) {
        for (const category of products) {
            const tempValue = (category.catProducts || []).find(product => product.slug === slug);
            if (tempValue) {
                return tempValue;
            }
        }
        return false;
    }

    const findArticle = (articleSlug) => {
        let found = null;
            const foundArticle = (articles || []).find(article => article.slug === articleSlug);
            if (foundArticle) {
                found = foundArticle;
            }
        return found;
    };


    const routes = 
    (<>
    <ScrollToTop />
    <BurgerMenuCloser />
    <Header />
    <main id="maincontent">
        <Switch>
            <Route
                exact path="/"
                render={() => (
                    <Frontpage />
                )}
            />
            <Route
                exact path="/uutiset"
                render={() => (
                    <News pageNum={1} />
                )}
            />
            <Route
                exact path="/uutiset/sivu/:pagenum"
                render={(props) => (
                    <News pageNum={props.match.params.pagenum} />
                )}
            />
            <Route
                exact path="/uutiset/:slug"
                render={(props) => {
                const foundArticle = findArticle(props.match.params.slug);
                if (articles.length === 0 || (articles.length > 0 && !foundArticle)) {
                    return (<NotFoundPage />);
                } else {
                    return (
                        //todo: get article and give as prop (content)
                        <NewsPage 
                            thisArticleSlug={props.match.params.slug}
                            thisArticle={
                                articles
                                .find(article => article.slug === props.match.params.slug)
                            }
                            thisArticleContent={
                                <div dangerouslySetInnerHTML={{ //todo: dompurify
                                    __html: articles
                                    .find(article => article.slug === props.match.params.slug)
                                    .content //todo: clean?
                                }} />
                            }
                        />
                    );
                }}}
            />
            <Route
                path="/tuotteet/:slug"
                render={(props) => {
                    const product = getSingleProduct(props.match.params.slug);
                    
                    if (products.length === 0 || (products.length > 0 && !product)) {
                        return (<NotFoundPage />);
                    } else {
                        return (
                            <div>
                                <ProductPage 
                                    thisProductSlug={props.match.params.slug}
                                    thisProduct={product}
                                    thisProductText={
                                        <div dangerouslySetInnerHTML={{ //todo: dompurify
                                            __html: product.productText //todo: clean?
                                        }} />
                                    }
                                    thisProductPrice={product.price}
                                    thisProductCampaignPrice={product.campaignPrice}
                                    thisProductFirstImage={product.productImages[0]}
                                />
                            </div>
                        );
                    }}}
            />
            <Route
                path="/tuotteet"
                render={() => (
                    <div>
                        <Products />
                    </div>
                )}
            />
            <Route
                path="/palvelut"
                render={() => (
                    <div>
                        <Services />
                    </div>
                )}
            />
            <Route
                path="/yhteystiedot"
                render={() => (
                    <Contact />
                )}
            />
            <Route component={NotFoundPage} />
        </Switch>
    </main>
    <Footer contactInfo={contactInfo}/>
    </>);

  return (
    <ThemeContext.Provider value={theme}>
        <PagesContext.Provider value={pages}>
            <ProductsContext.Provider value={products}>
                <ArticlesContext.Provider value={articles}>
                    <ServicesContext.Provider value={services}>
                        {ssrLocation ? <StaticRouter location={ssrLocation}>{routes}</StaticRouter> : <BrowserRouter>{routes}</BrowserRouter>}
                    </ServicesContext.Provider>
                </ArticlesContext.Provider>
            </ProductsContext.Provider>
        </PagesContext.Provider>
    </ThemeContext.Provider>
  );
};

export default SsrApp;