import React, { useState, useEffect } from 'react';
import { BrowserRouter, StaticRouter, Switch, Route } from "react-router-dom";
import conf from "./components/config";
import Header from "./Header";
import Frontpage from "./Frontpage";
import News from "./News";
import Products from "./Products";
import Services from "./Services";
import Contact from "./Contact";
import Footer from "./Footer";
import NewsPage from "./components/NewsPage";
import NotFoundPage from "./components/NotFoundPage";
import BurgerMenuCloser from "./components/BurgerMenuCloser";
import ThemeContext from "./context/theme.context.js";
import PagesContext from "./context/pages.context.js";
import ProductsContext from "./context/products.context.js";
import ArticlesContext from "./context/articles.context.js";
import ServicesContext from "./context/services.context.js";
import ProductPage from "./components/ProductPage";
import ScrollToTop from "./components/ScrollToTop";
import CryptoJS from "crypto-js";

const sortByDate = function (a, b) {
    if (a.date < b.date) {
        return 1;
    }
    if (a.date > b.date) {
        return -1;
    }
    return 0;
};

const App = () => {

    const decodeBase64 = (argStr) => { 
        try { 
            const parsedWord = CryptoJS.enc.Base64.parse(argStr);
            const parsedStr = parsedWord.toString(CryptoJS.enc.Utf8);
            return parsedStr;
        } catch (err) { 
            throw Error("Couldn't parse base64 string."); 
        } 
    };

    const windowdata = window.data;
    //const jotaki = atob(windowdata || "");
    const jotaki = decodeBase64(windowdata || "");
    const data = JSON.parse(jotaki || JSON.stringify({
        "pages": [], "articles": [], "products": [], "services": [] 
    }));
    const [services, setServices] = useState(data.services);
    const [pages, setPages] = useState(data.pages);
    const [contactInfo, setContactInfo] = useState(pages.find(
        page => page.title === "Yhteystiedot"
    ));
    const [articles, setArticles] = useState(data.articles);
    const [products, setProducts] = useState(data.products);
    const [theme, setTheme] = useState("winter");

    const sortByTheme = (a, b) => {
        if (theme === "winter") {
            if (a.winterNum > b.winterNum) {
                return 1;
            }
            if (a.winterNum < b.winterNum) {
                return -1;
            }
            return 0;
        }
        else if (theme === "summer") {
            if (a.summerNum > b.summerNum) {
                return 1;
            }
            if (a.summerNum < b.summerNum) {
                return -1;
            }
            return 0;
        }
    };

    function getPages() {
        fetch(`${conf.server_base_url}/pages/get`)
            .then(response => response.json())
            .then((json) => {
                setPages(json);
            })
            .catch(error => {
                console.log(error);
            });
    }

    // first of all, get all pages
    useEffect(() => {
        getPages();
    }, []);

    function getArticles() {
        fetch(`${conf.server_base_url}/articles/get`)
            .then(response => response.json())
            .then((json) => {
                // Sort articles by date
                let orderedJson = [...json];
                orderedJson.sort(sortByDate);
                setArticles(orderedJson);
            })
            .catch(error => {
                console.log(error);
            });
    }

    const findArticle = (articleSlug) => {
        let found = null;
            const foundArticle = (articles || []).find(article => article.slug === articleSlug);
            if (foundArticle) {
                found = foundArticle;
            }
        return found;
    };

    // launches when page loaded
    useEffect(() => {
        getArticles();
    }, []);

    // fetch products from backend
    function getProducts() {
        fetch(`${conf.server_base_url}/products/get`)
            .then(response => response.json())
            .then((json) => {
                json.sort(sortByTheme);
                //return json;
                setProducts(json);
            })
            .catch(error => {
                console.log(error);
            });
    }

    // launches when page loaded
    useEffect(() => {
        getProducts();
    }, [theme]);

    
    const findProduct = (productsSlug) => {
        let found = null;
        products.forEach(category => {
            const foundProduct = (category.catProducts || []).find(
                product => product.slug === productsSlug
            );
            if (foundProduct) {
                found = foundProduct;
            }
        });
        return found;
    };


    function getContactInfo() {
        if (pages && pages.length > 0) {
            const contactPage = pages.find(page => page.title === "Yhteystiedot");
            setContactInfo(contactPage);
            setTheme(contactPage.season);
            document.body.classList.add(contactPage.season);
        }
    }

    function getServices() {
        fetch(`${conf.server_base_url}/services/get`)
            .then(response => response.json())
            .then((json) => {
                json.sort(sortByTheme);
                //return json;
                setServices(json);
            });
    }

    // launches when page loaded
    useEffect(() => {
        getServices();
    }, [theme]);


    // after all pages are got, get contact info page based on name
    useEffect(() => {
        getContactInfo();
    }, [pages]);

    const routes =
        (<>
            <ScrollToTop />
            <BurgerMenuCloser />
            <Header />
            <main id="maincontent">
                <Switch>
                    <Route
                        exact path="/"
                        render={() => (
                            <Frontpage />
                        )}
                    />
                    <Route
                        exact path="/uutiset"
                        render={() => (
                            <News pageNum={1} />
                        )}
                    />
                    <Route
                        exact path="/uutiset/sivu/:pagenum"
                        render={(props) => (
                            articles ? <News pageNum={props.match.params.pagenum} /> : ""
                        )}
                    />
                    <Route
                        exact path="/uutiset/:slug"
                        render={(props) => {
                            const foundArticle = findArticle(props.match.params.slug);
                            if (articles.length === 0 || articles.length > 0 && !foundArticle) {
                                return (<NotFoundPage />);
                            }
                            return (
                            <NewsPage thisArticleSlug={props.match.params.slug}  />
                        );}}
                    />
                    <Route
                        path="/tuotteet/:slug"
                        render={(props) => {
                            const foundProduct = findProduct(props.match.params.slug);
                            if (products.length === 0) {
                                return (<NotFoundPage />);
                            } else if (products.length > 0 && !foundProduct) {
                                return (<NotFoundPage />);
                            }
                            return (
                            <div>
                                <ProductPage 
                                    thisProductSlug={props.match.params.slug} 
                                    thisProduct={foundProduct} />
                            </div>
                        );}}
                    />
                    <Route
                        path="/tuotteet"
                        render={() => (
                            <div>
                                <Products />
                            </div>
                        )}
                    />
                    <Route
                        path="/palvelut"
                        render={() => (
                            <div>
                                <Services />
                            </div>
                        )}
                    />
                    <Route
                        path="/yhteystiedot"
                        render={() => (
                            <Contact />
                        )}
                    />
                    <Route component={NotFoundPage} />
                </Switch>
            </main>
            <Footer contactInfo={contactInfo} />
        </>);

    return (
        <ThemeContext.Provider value={theme}>
            <PagesContext.Provider value={pages}>
                <ProductsContext.Provider value={products}>
                    <ArticlesContext.Provider value={articles}>
                        <ServicesContext.Provider value={services}>
                            <BrowserRouter>{routes}</BrowserRouter>
                        </ServicesContext.Provider>
                    </ArticlesContext.Provider>
                </ProductsContext.Provider>
            </PagesContext.Provider>
        </ThemeContext.Provider>
    );
};

export default App;