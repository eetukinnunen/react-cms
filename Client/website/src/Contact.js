import React from "react";
import HeroHeader from './components/HeroHeader';
import SiteHelmet from './components/SiteHelmet';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMapMarkedAlt, faPhone, faFileAlt } from '@fortawesome/free-solid-svg-icons';
import useCurrentPage from "./context/currentPage.context.js";
import FormedForm from "./components/FormedForm";
import conf from "./components/config";
import PageHelmetHandler from "./components/PageHelmetHandler";
import LoadingSpinner from "./components/LoadingSpinner";
import ReactGA from "react-ga";

// Bootstrap
import {
        Container,
        Row,
        Col,
        Image
} from 'react-bootstrap';

// Google Analytics
ReactGA.initialize('UA-176959782-1');

const ContactInfo = (props) => {
    const {page} = props;

    return (
        <>  
            <Row className="contact-info">
                <Col xs={12} md={6}>
                <h2>{page.contents[1].content}</h2>
                    <div className="d-flex align-items-center contact-info-container">
                        <div className="text-center">
                            <div className="info-icon-container">
                                <FontAwesomeIcon icon={faMapMarkedAlt} />
                            </div>
                        </div>
                        <div className="contact-text-container">
                            <p className="contact-text">
                                {page.contents[2].content}<br/>
                                {page.contents[3].content}<br/>
                                {page.contents[4].content}
                            </p>
                        </div>
                    </div>
                    <div className="d-flex align-items-center contact-info-container">
                    <div className="text-center">
                            <div className="info-icon-container">
                                <FontAwesomeIcon icon={faPhone} />
                            </div>  
                        </div>
                        <div className="contact-text-container">
                            <p className="contact-text">
                                {page.contents[5].content}<br/>
                                {page.contents[6].content}<br/>
                                {page.contents[7].content}
                            </p>
                        </div>
                    </div>
                    <div className="d-flex align-items-center contact-info-container">
                    <div className="text-center">
                            <div className="info-icon-container">
                                <FontAwesomeIcon icon={faFileAlt} />
                            </div>  
                        </div>
                        <div className="contact-text-container">
                            <p className="contact-text">
                                {page.contents[8].content}<br/>
                                {page.contents[9].content}<br/>
                                {page.contents[10].content}
                            </p>
                        </div>
                    </div>
                </Col>
                <Col xs={12} md={6}>
                    <Image src={`${conf.server_base_url}/${page.images[1].summer}`} fluid />
                </Col>
            </Row>

            
        </>
    );
};

const ContactForm = (props) => {
    return (
        <Row className="contact-form p-md-5">
            <Col xs={12}>
                <FormedForm>
                    <h3>Lähetä meille viesti</h3>
                    <Row>
                        <Col xs={12} md={6}>
                            <div className="form-group">
                                <label htmlFor="email">Sähköpostiosoite</label>
                                <input 
                                type="email" 
                                className="form-control" 
                                name="email" 
                                placeholder="Sähköpostiosoite" required />
                            </div>
                        </Col>
                        <Col xs={12} md={6}>
                            <div className="form-group">
                                <label htmlFor="name">Nimi</label>    
                                <input 
                                type="text" 
                                className="form-control" 
                                name="senderName" 
                                placeholder="Nimi" required />    
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12}>
                            <div className="form-group">
                                <label htmlFor="message">Viesti</label>
                                <textarea 
                                className="form-control" 
                                name="message" 
                                rows="4" 
                                placeholder="Kirjoita viestisi tähän">
                                </textarea>
                            </div>
                            <div className="form-group">
                                <button 
                                type="" 
                                className="btn btn-primary btn-lg send-button theme-button">
                                    Lähetä viesti
                                </button>
                            </div>
                        </Col>
                    </Row>
                </FormedForm>
            </Col>
        </Row>
        
    );
};

const GoogleMap = (props) => {
    return (
        <Row className="contact-map">
            {/* <iframe 
            width="100%" 
            height="500" 
            src="" 
            frameBorder="0" scrolling="no" marginHeight="0" marginWidth="0"
            >
                <a href="http://www.gps.ie/">Google Maps coordinates</a>
            </iframe> */}
        </Row>

    );
};

const Contact = (props) => {
    const page = useCurrentPage("Yhteystiedot");

    ReactGA.pageview("/yhteystiedot");
    
    return (
        <>
            {page.contents && page.title === "Yhteystiedot" ? (
                <>
                    <PageHelmetHandler image={page.images[1]} 
                    description="Yhteystiedot" 
                    title={page.contents[0].content} 
                    url={conf.client_base_url + "/yhteystiedot"}/>
                    
                    <HeroHeader content={page.contents[0].content} image={page.images[0]} />
                        <section className="contact">
                                <Container>
                                    <ContactInfo page={page} />
                                    <ContactForm />
                                    <GoogleMap />
                                </Container>
                        </section>
                </>
            ) : <LoadingSpinner className="centered" />}
        </>
    );
};

export default Contact;