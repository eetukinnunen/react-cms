import React, {useState, useEffect} from "react";
import conf from "../components/config";
import { Row, Col } from "react-bootstrap";
import Wysiwyg from "../components/Wysiwyg";
import ImgCropTool from "../components/ImgCropTool";
import useCurrentPage from "../context/currentPage.context.js";
import { v4 as uuidv4 } from "uuid";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBullhorn, faListUl, faRulerHorizontal, faFileImage } from '@fortawesome/free-solid-svg-icons';
import AdminHelmet from "../components/AdminHelmet";

const NewsPage = (props) => {
    const {saveContents} = props;
    const page = useCurrentPage("Uutiset")

    const contents = page.contents; // possible error at some point if contents undefined?

    const [images, setImages] = useState([]);

    useEffect(() => {
        setImages([]);
        if(!page.images) {
            return;
        }
        setImages(images => [...images, 
            <Col lg="6" key={uuidv4()}>
                <label htmlFor="image" className="form-input-header mt-4"><FontAwesomeIcon icon={faFileImage} size="1x" className="mr-3"/>Kuva 1 (kesä)</label>
                {/* if image name exists in json, put it into imgcroptool */}
                <ImgCropTool inputName="image" imgSrc={page.images[0] ? `${conf.server_base_url}/${page.images[0].summer}` : ""} />
            </Col>,
            <Col lg="6" key={uuidv4()}>
                <label htmlFor="image" className="form-input-header mt-4"><FontAwesomeIcon icon={faFileImage} size="1x" className="mr-3"/>Kuva 1 (talvi)</label>
                <ImgCropTool inputName="image" imgSrc={page.images[0] ? `${conf.server_base_url}/${page.images[0].winter}` : ""} />
            </Col>
        ]);
    }, [page]);
    
    if(!contents) {
        return (
            <></>
        )
    }

    return (
        <>
            <AdminHelmet title={page.title} />
            <div className="">
                <form onSubmit={saveContents} encType="multipart/form-data">
                    <input type="hidden" id="formType" name="formType" value="newsPage" />
                    <input type="hidden" id="pageId" name="id" value={page._id} />
                    <h3>{page.title}</h3>
                    
                    <label htmlFor="newsSubtitle" className="form-input-header mt-4"><FontAwesomeIcon icon={faBullhorn} size="1x" className="mr-3"/>Otsikko</label>
                    <input className="form-control" type="text" id="newsSubtitle" name="newsSubtitle" defaultValue={contents[0].content} />

                    <label htmlFor="articlesperpage" className="form-input-header mt-4"><FontAwesomeIcon icon={faListUl} size="1x" className="mr-3"/>Artikkelien määrä yhdellä sivulla</label>
                    <input className="form-control" type="number" min="1" step="1" id="articlesperpage" name="articlesperpage" defaultValue={page.articlesperpage} />

                    <label htmlFor="previewLength" className="form-input-header mt-4"><FontAwesomeIcon icon={faRulerHorizontal} size="1x" className="mr-3"/>Artikkelitekstin lyhennyksen pituus merkkeinä</label>
                    <input className="form-control" type="number" min="1" step="1" id="previewLength" name="previewLength" defaultValue={page.previewLength} />

                    <Row>
                    {images}
                    </Row>

                    <button type="submit" className="btn btn-lg btn-success">Tallenna</button>
                </form>
            </div>
        </>
    );
};

export default NewsPage;

