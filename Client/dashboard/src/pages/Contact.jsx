import React, {useState, useEffect} from "react";
import conf from "../components/config";
import { Row, Col } from "react-bootstrap";
import Wysiwyg from "../components/Wysiwyg";
import ImgCropTool from "../components/ImgCropTool";
import useCurrentPage from "../context/currentPage.context.js";
import { v4 as uuidv4 } from "uuid";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBullhorn, faMapMarkedAlt, faPhone, faFileAlt, faTag, faShare, faInfo, faGlobe } from '@fortawesome/free-solid-svg-icons';
import { faFacebookSquare, faInstagram, faTwitter, faLinkedin, faTiktok, faYoutube, faSnapchat } from '@fortawesome/free-brands-svg-icons';
import AdminHelmet from "../components/AdminHelmet";

const SomeLink = (props) => {

    const someOptions = [
        {
            name: "Facebook",
            icon: faFacebookSquare
        },
        {
            name: "Instagram",
            icon: faInstagram
        },
        {
            name: "Twitter",
            icon: faTwitter
        },
        {
            name: "LinkedIn",
            icon: faLinkedin
        },
        {
            name: "Tiktok",
            icon: faTiktok
        },
        {
            name: "YouTube",
            icon: faYoutube
        },
        {
            name: "Snapchat",
            icon: faSnapchat
        },
        {
            name: "Muu",
            icon: faGlobe
        }
    ];

    const [someType, setSomeType] = useState(someOptions[0]);
    const [someUrl, setSomeUrl] = useState("");

    useEffect(() => {
        console.log("prop effect");
        if (props.type && props.url) {
            console.log(props);
            setSomeType(someOptions.find(someOption => someOption.name === props.type));
            setSomeUrl(props.url);
        }

    }, [props]);

    return (
        <>
            <Col xs="10" md="3">
                <select className="form-control" name="someName" value={someType.name} onChange={(e) => setSomeType(someOptions.find(someOption => someOption.name === e.target.value))} required>
                    {someOptions.map(someOption => <option value={someOption.name}>{someOption.name}</option>)}
                </select>
            </Col>
            <Col xs="1">
                {someType ?
                    <div className="form-input-header">
                        <FontAwesomeIcon icon={someType.icon} />
                    </div>
                : ""}
            </Col>
            {someType ?
                <Col xs="10" md="6">
                    <input className="form-control" type="text" name="someLink" defaultValue={someUrl} placeholder="Osoite" />
                </Col>
            : ""}
        </>
    );
};

const Contact = (props) => {
    const {saveContents} = props;
    const page = useCurrentPage("Yhteystiedot");

    const contents = page.contents; // possible error at some point if contents undefined?

    const [images, setImages] = useState([]);
    const [someLinks, setSomeLinks] = useState([]);

    function deleteSomeLink(id) {
        console.log("delete");
        console.log(id);

        console.log(someLinks);
        someLinks.forEach(some => {
            console.log(some);
        });
        setSomeLinks([...someLinks.filter(some => some.id !== id)]);
    }

    // adds a new somelink object to the state array
    function addSomeLink(type, url, id=uuidv4()) {
        setSomeLinks(someLinks => [...someLinks, 
            {
                url,
                type,
                id
            }
        ]);
    }

    useEffect(() => {
        setImages([]);
        if(!page.images) {
            return;
        }
        setImages(images => [...images, 
            <Col lg="6" key={uuidv4()}>
                <label htmlFor="image" className="form-input-header mt-4" className="form-input-header mt-4">Kuva 1 (kesä)</label>
                {/* if image name exists in json, put it into imgcroptool */}
                <ImgCropTool inputName="image" imgSrc={page.images[0] ? `${conf.server_base_url}/${page.images[0].summer}` : ""} />
            </Col>,
            <Col lg="6" key={uuidv4()}>
                <label htmlFor="image" className="form-input-header mt-4">Kuva 1 (talvi)</label>
                <ImgCropTool inputName="image" imgSrc={page.images[0] ? `${conf.server_base_url}/${page.images[0].winter}` : ""} />
            </Col>
        ]);

        setImages(images => [...images, 
            <Col lg="6" key={uuidv4()}>
                <label htmlFor="image" className="form-input-header mt-4">Kuva 2 (kesä)</label>
                <ImgCropTool inputName="image" imgSrc={page.images[1] ? `${conf.server_base_url}/${page.images[1].summer}` : ""} />
            </Col>,
            <Col lg="6" key={uuidv4()}>
                <label htmlFor="image" className="form-input-header mt-4">Kuva 2 (talvi)</label>
                <ImgCropTool inputName="image" imgSrc={page.images[1] ? `${conf.server_base_url}/${page.images[1].winter}` : ""} />
            </Col>
        ]);

        // initialize each somelink
        for (let i = 11; i < page.contents.length; i++) {
            console.log(page.contents[i]);
            addSomeLink(page.contents[i].name, page.contents[i].content);
        }
    }, [page]);
    
    if(!contents) {
        return (
            <></>
        );
    };

    return (
        <>
            <AdminHelmet title={page.title} />
            <div className="">
                <form onSubmit={saveContents} encType="multipart/form-data">
                    <input type="hidden" id="formType" name="formType" value="contactPage" />
                    <input type="hidden" id="pageId" name="id" value={page._id} />
                    <h3>{page.title}</h3>

                    <Row>

                        <Col xs={12} md={6}>
                            <div className="d-flex flex-row">
                                <div className="flex-fill">
                                    <div>
                                        <div className="contact-icon-container mt-4 mr-4">
                                            <FontAwesomeIcon icon={faInfo} />
                                        </div>
                                    </div>
                                    <label htmlFor="subtitle"  className="form-input-header mt-4"><FontAwesomeIcon icon={faBullhorn} className="mr-3"/>{contents[0].name}</label>
                                    <input className="form-control" type="text" id="subtitle" name="subtitle" defaultValue={contents[0].content} />

                                    <label htmlFor="companyName"  className="form-input-header mt-4"><FontAwesomeIcon icon={faTag} className="mr-3"/>{contents[1].name}</label>
                                    <input className="form-control" type="text" id="companyName" name="companyName" defaultValue={contents[1].content} />
                                </div>
                            </div>
                        </Col>

                        <Col xs={12} md={6}>
                            <div className="d-flex flex-row">
                                <div className="flex-fill">
                                    <div>
                                        <div className="contact-icon-container mt-4 mr-4">
                                            <FontAwesomeIcon icon={faMapMarkedAlt} />
                                        </div>
                                    </div>
                                    <label htmlFor="adress1" className="form-input-header mt-4">{contents[2].name}</label>
                                    <input className="form-control" type="text" id="adress1" name="adress1" defaultValue={contents[2].content} />

                                    <label htmlFor="adress2" className="form-input-header mt-4">{contents[3].name}</label>
                                    <input className="form-control" type="text" id="adress2" name="adress2" defaultValue={contents[3].content} />

                                    <label htmlFor="adress3" className="form-input-header mt-4">{contents[4].name}</label>
                                    <input className="form-control" type="text" id="adress3" name="adress3" defaultValue={contents[4].content} />
                                </div>
                            </div>
                        </Col>

                        <Col xs={12} md={6}>
                            <div className="d-flex flex-row">
                                <div className="flex-fill">
                                    <div>
                                        <div className="contact-icon-container mt-4 mr-4">
                                            <FontAwesomeIcon icon={faPhone} />
                                        </div>
                                    </div>
                                    <label htmlFor="contact1" className="form-input-header mt-4">{contents[5].name}</label>
                                    <input className="form-control" type="text" id="contact1" name="contact1" defaultValue={contents[5].content} />

                                    <label htmlFor="contact2" className="form-input-header mt-4">{contents[6].name}</label>
                                    <input className="form-control" type="text" id="contact2" name="contact2" defaultValue={contents[6].content} />

                                    <label htmlFor="contact3" className="form-input-header mt-4">{contents[7].name}</label>
                                    <input className="form-control" type="text" id="contact3" name="contact3" defaultValue={contents[7].content} />
                                </div>
                            </div>
                        </Col>

                        <Col xs={12} md={6}>
                            <div className="d-flex flex-row">
                                <div className="flex-fill">
                                    <div className="d-flex">
                                        <div className="contact-icon-container mt-4 mr-4">
                                            <FontAwesomeIcon icon={faFileAlt} />
                                        </div>
                                    </div>
                                    <label htmlFor="company1" className="form-input-header mt-4">{contents[8].name}</label>
                                    <input className="form-control" type="text" id="company1" name="company1" defaultValue={contents[8].content} />

                                    <label htmlFor="company2" className="form-input-header mt-4">{contents[9].name}</label>
                                    <input className="form-control" type="text" id="company2" name="company2" defaultValue={contents[9].content} />

                                    <label htmlFor="company3" className="form-input-header mt-4">{contents[10].name}</label>
                                    <input className="form-control" type="text" id="company3" name="company3" defaultValue={contents[10].content} />
                                </div>
                            </div>
                        </Col>

                        {/*<Col xs={12}>
                            <div className="d-flex flex-row">
                                <div className="flex-fill">
                                    <div className="d-flex">
                                        <div className="contact-icon-container mt-4 mr-4">
                                            <FontAwesomeIcon icon={faShare} />
                                        </div>
                                    </div>
                                    <label htmlFor="facebook" className="form-input-header mt-4"><FontAwesomeIcon icon={faFacebookSquare} className="mr-3"/>{contents[11].name}</label>
                                    <input className="form-control" type="text" id="facebook" name="facebook" defaultValue={contents[11].content} />

                                    <label htmlFor="instagram" className="form-input-header mt-4"><FontAwesomeIcon icon={faInstagram} className="mr-3"/>{contents[12].name}</label>
                                    <input className="form-control" type="text" id="instagram" name="instagram" defaultValue={contents[12].content} />
                                </div>
                            </div>
                        </Col>*/}

                        <Col xs={12}>
                            <div className="d-flex flex-row">
                                <div className="flex-fill">
                                    <div className="d-flex">
                                        <div className="contact-icon-container mt-4 mr-4">
                                            <FontAwesomeIcon icon={faShare} />
                                        </div>
                                    </div>
                                    {someLinks.map(someLink => 
                                        <Row className="mt-4" key={someLink.id}>
                                            <SomeLink type={someLink.type} url={someLink.url} />
                                            <Col xs="2">
                                                <button type="button" onClick={() => deleteSomeLink(someLink.id)}>Poista</button>
                                            </Col>
                                        </Row>
                                    )}
                                    <button type="button" className="btn theme-button mt-3" onClick={() => addSomeLink("", "")}>Lisää uusi some</button>
                                </div>
                            </div>
                        </Col>

                    </Row>
                    <Row>
                        {images}
                    </Row>
                    <button type="submit" className="btn btn-lg btn-success">Tallenna</button>
                </form>
            </div>
        </>
    );
};

export default Contact;

