import React, {useState, useEffect} from "react";
import conf from "../components/config";
import { Row, Col } from "react-bootstrap";
import Wysiwyg from "../components/Wysiwyg";
import ImgCropTool from "../components/ImgCropTool";
import { v4 as uuidv4 } from "uuid";
import useCurrentPage from "../context/currentPage.context.js";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBullhorn, faAlignLeft, faFileImage } from '@fortawesome/free-solid-svg-icons';
import AdminHelmet from "../components/AdminHelmet";

const Frontpage = (props) => {
    const {saveContents} = props;
    const page = useCurrentPage("Etusivu");
    const contents = page.contents; // possible error at some point if contents undefined?

    const [images, setImages] = useState([]);
   
    useEffect(() => {
        setImages([]);
        if(!page.images) {
            return;
        }
        for (let i = 0; i < 4; i++) {
            setImages(images => [...images, 
                <Col lg="6" key={uuidv4()}>
                    <label htmlFor="image" className="form-input-header mt-4"><FontAwesomeIcon icon={faFileImage} size="1x" className="mr-3"/>Kuva {i+1} (kesä)</label>
                    {/* if image name exists in json, put it into imgcroptool */}
                    <ImgCropTool inputName="image" imgSrc={page.images[i] ? `${conf.server_base_url}/${page.images[i].summer}` : ""} />
                </Col>,
                <Col lg="6" key={uuidv4()}>
                    <label htmlFor="image" className="form-input-header mt-4"><FontAwesomeIcon icon={faFileImage} size="1x" className="mr-3"/>Kuva {i+1} (talvi)</label>
                    <ImgCropTool inputName="image" imgSrc={page.images[i] ? `${conf.server_base_url}/${page.images[i].winter}` : ""} />
                </Col>
            ]);
        }
    }, [page]);
    
    if(!contents) {
        return (
            <></>
        );
    };

    return (
        <>
            <AdminHelmet title={page.title} />
            <div className="frontPage admin-interface-content">
                <form onSubmit={saveContents} encType="multipart/form-data">
                    <input type="hidden" id="formType" name="formType" value="frontPage" />
                    <input type="hidden" id="pageId" name="id" value={page._id} />
                    <h3>{page.title}</h3>
                    
                    <label htmlFor="subtitle" className="form-input-header mt-4"><FontAwesomeIcon icon={faBullhorn} size="1x" className="mr-3"/>Alaotsikko</label>
                    <input className="form-control" type="text" id="subtitle" name="subtitle" defaultValue={contents[0].content} />

                    <Row className="triplets">
                        <Col lg="4">
                            <label className="form-input-header mt-4"><FontAwesomeIcon icon={faAlignLeft} size="1x" className="mr-3"/>{contents[1].name}</label>
                            <Wysiwyg field="palsta-1" initial={contents[1].content} />
                        </Col>
                        <Col lg="4">
                            <label className="form-input-header mt-4"><FontAwesomeIcon icon={faAlignLeft} size="1x" className="mr-3"/>{contents[2].name}</label>
                            <Wysiwyg field="palsta-2" initial={contents[2].content} />
                        </Col>
                        <Col lg="4">
                            <label className="form-input-header mt-4"><FontAwesomeIcon icon={faAlignLeft} size="1x" className="mr-3"/>{contents[3].name}</label>
                            <Wysiwyg field="palsta-3" initial={contents[3].content} />
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <label className="form-input-header mt-4"><FontAwesomeIcon icon={faAlignLeft} size="1x" className="mr-3"/>{contents[4].name}</label>
                            <Wysiwyg field="palsta-4" initial={contents[4].content} />
                        </Col>
                    </Row>
                    <Row>
                    {images}
                    </Row>

                    <button type="submit" className="btn btn-lg btn-success">Tallenna</button>
                </form>
            </div>
        </>
    );
};

export default Frontpage;

