import React from "react";
import {Link} from "react-router-dom";
import conf from "../components/config";
import { 
    Container,
} from "react-bootstrap";
import "../style.scss";
import AdminHelmet from "../components/AdminHelmet";

const NotFound = () => {
    return (
        <>
            <AdminHelmet title="404 - Sivua ei löytynyt" />
            <Container>
                <div className="notfoundpage">
                    <h1 className="display-1">404</h1>
                    <h1>Sivua ei löydy!</h1>
                    <Link to={conf.base_route}>
                    <button className="btn btn-primary">Palaa takaisin etusivulle</button> 
                    </Link>
                </div>
            </Container>
        </>
    );
};

export default NotFound;