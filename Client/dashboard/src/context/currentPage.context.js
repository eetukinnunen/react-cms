import React, { useEffect, useState } from "react";
import PagesContext from "./pages.context.js";
import { useLocation } from "react-router-dom";

const useCurrentPage = (pageTitle) => {
    const [currentPage, setCurrentPage] = useState({});
    const location = useLocation();

    const pages = React.useContext(PagesContext);

    const findPage = (pages, url) => {
        pages = pages && pages.length > 0 ? pages : [];
        let title = pageTitle;
        console.log(title, pages, url);

        let page = (pages || []).find(page => page.title === title);

        if(!page && pages.length > 0) {
            return {};
        } else if(!page) {
            return {};
        }
        return page;
    }

    useEffect(() => {
        const page = findPage(pages, location.pathname);
        setCurrentPage(page);
    }, [pages, location]);
    
    return currentPage;    
};

export default useCurrentPage;