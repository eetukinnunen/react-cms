import conf from "./components/config";
import Swal from "sweetalert2";

function apiCall(endpoint, method, formData, successMessage="Tallennettu!") {

    fetch(`${conf.server_base_url}/${endpoint}`, {
        method: method,
        credentials:"include",
        body: formData
    })
    .then(async response => {
        //IF SERVER AUTH DETECTS BAD COOKIE, KICK TO LOGIN
        if (response.status === 401){
            window.location = `${conf.base_route}/login/`
        }
        if (!response.ok) {
            const text = await response.text();
            throw new Error(text);
        } else {
            return response.json();
        }
    })
    .then(json => {
        console.log(json);
        //refreshContents();
        // success message/popup
        Swal.fire(
            successMessage,
            "",
            "success"
        ).then(() => {
            window.location.href = conf.base_route; //comment this out for easier testing
        });
    })
    .catch(error => {
        console.log(error);
        // error message/popup
        Swal.fire(
            "Hups!",
            `Toiminto epäonnistui... <br />  <span class="error-text">${error}</span>`,
            "error"
        );
    });
}

export {
    apiCall
};