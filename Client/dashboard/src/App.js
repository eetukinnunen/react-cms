import React, {useState, useEffect} from "react";
import Login from "./components/LoginComponent";
import { BrowserRouter as Router, Route, Switch, Link, Redirect } from "react-router-dom";
import conf from "./components/config";
import {apiCall} from "./utils.js";
import Header from "./components/Header";
import Sidebar from "./components/Sidebar";
import Frontpage from "./pages/Frontpage";
import NewsPage from "./pages/Newspage";
import ProductsPage from "./pages/ProductsPage";
import ServicesPage from "./pages/ServicesPage";
import Contact from "./pages/Contact";
import NotFoundPage from "./pages/NotFoundPage";
import Product from "./components/Product";
import Category from "./components/Category";
import Article from "./components/Article";
import {Container, Row, Col} from "react-bootstrap";
import LoadingSpinner from "./components/LoadingSpinner";
import Swal from 'sweetalert2';
import TrashBin from "./components/TrashBin";
import UserSettings from "./components/UserSettings";
import ForgottenPasswordEmail from "./components/ForgottenPasswordEmail";
import ForgottenPasswordReset from "./components/ForgottenPasswordReset";
import ImageSettings from "./components/ImageSettings";
import SeasonOrder from "./components/SeasonOrder";
import ScrollToTop from "./components/ScrollToTop";
import PagesContext from "./context/pages.context.js";
import ProductsContext from "./context/products.context.js";
import ArticlesContext from "./context/articles.context.js";
import ServicesContext from "./context/services.context.js";
import IsLoggedContext from "./context/isLogged.context";
import ThemeContext from "./context/theme.context.js";
import Service from "./components/Service";
import AdminHelmet from "./components/AdminHelmet";
import Instructions from "./components/Instructions";
import { faLessThanEqual } from "@fortawesome/free-solid-svg-icons";

const App = () => {
    const [pages, setPages] = useState();
    const [articles, setArticles] = useState();
    const [products, setProducts] = useState();
    const [services, setServices] = useState();
    const [isLogged, setIsLogged]= useState();

    
    // fetch(`${conf.base_url}/login/check`)
    const checkAuth = () => {
        fetch(`${conf.server_base_url}/login/check`,{credentials:"include"})
        .then(async response => {
            if (response.status === 200){
                setIsLogged(true);
                // console.log("isLogged is "+ isLogged)
            }
            else{setIsLogged(false)}
            // console.log("isLogged is "+ isLogged)
        }
    )};
    
    // fetch(`${conf.base_url}/pages/get`)
    function getPages() {
        fetch(`${conf.server_base_url}/pages/get`,{credentials:"include"})
        .then(response => response.json())
            .then((json) => {
                console.log(json);
                setPages(json);
            })
            .catch(error => {
                console.log(error);
                Swal.fire(
                    "Hups!",
                    `Sivujen hakeminen epäonnistui... <br />  <span class="error-text">${error}</span>`,
                    "error"
                );
            });
    }

    function getArticles() {
        fetch(`${conf.server_base_url}/articles/get/admin`,{credentials:"include"})
        .then(response => response.json())
            .then((json) => {
                // console.log(json);
                setArticles(json);
            })
            .catch(error => {
                console.log(error);
                Swal.fire(
                    "Hups!",
                    `Artikkelien hakeminen epäonnistui... <br />  <span class="error-text">${error}</span>`,
                    "error"
                );
            });
    }
    function getProducts() {
        fetch(`${conf.server_base_url}/products/get/admin`,{credentials:"include"})
        .then(response => response.json())
            .then((json) => {
                console.log(json);
                setProducts(json);
            })
            .catch(error => {
                console.log(error);
                Swal.fire(
                    "Hups!",
                    `Tuotteiden hakeminen epäonnistui... <br />  <span class="error-text">${error}</span>`,
                    "error"
                );
            });
    }

    function getServices() {
        fetch(`${conf.server_base_url}/services/get/admin`,{credentials:"include"})
        .then(response => response.json())
            .then((json) => {
                console.log(json);
                setServices(json);
            })
            .catch(error => {
                console.log(error);
                Swal.fire(
                    "Hups!",
                    `Palveluiden hakeminen epäonnistui... <br />  <span class="error-text">${error}</span>`,
                    "error"
                );
            });
    }

    function refreshContents() {
        checkAuth();
        getPages();
        getArticles();
        getProducts();
        getServices();
        console.log("Contents refreshed");
    }

    useEffect(() => {
        refreshContents();
    }, []);

    // example: Hyvää juhannusta kaikille -> hyvää-juhannusta-kaikille
    function stringToUrl(title) {
        //const slug = title.replace(/\s+/g, "-").toLowerCase();
        return title;
    }

    // adds the images and cropping, scaling etc to formData and then returns it to caller
    function pageImagesHandler(e, formData) {
        const imageInfos = [];

        if (e.target.image) {  // if the page in question has images
            for (let i = 0; i < e.target.image.length; i++) {
                let isNewImg;

                // if the image input in this index has a value (aka some image's fakepath)
                if (e.target.image[i].value) {
                    formData.append("image", e.target.image[i].files[0]);
                    isNewImg = true;
                }
                // otherwise there's no new image
                else {
                    isNewImg = false;
                    // the backend code checks whether an image exists at all based on crop&scaling values
                }

                const info = {
                    crop: e.target.crop[i].value,
                    scaling: e.target.scaling[i].value,
                    isNewImg: isNewImg,
                }
                imageInfos.push(info);
            }
            console.log(imageInfos);

            formData.append("imageInfos", JSON.stringify(imageInfos));
        }

        return formData;
    }

    // Gets the form data and sends it to the backend as multipart form data via one of the above functions
    function saveContents(e) {
        e.preventDefault();

        console.log(e.target);

        const formType = e.target.formType.value; //page, post, product or category

        // the FormData interface is used to make a set of key/value pairs to send multipart/form-data (text & images)
        let formData = new FormData();
        formData.append("type", formType); // and everything should have a formtype hidden input field

        let isNew = true; // by default, the thing (page, post, product or category) being saved is new

        let endpoint, method;

        // if id isn't undefined, this is an existing thing being edited, and therefore add the id to the formData
        const id = e.target.id.value;
        if (id) {
            isNew = false;
            formData.append("_id", id);
        }
        
        console.log(`New? ${isNew}`);  

        // Puts the contents of each tinyMce into an array
        const tinyEditors = e.target.querySelectorAll(".tox-edit-area__iframe");
        let tinyContents = [...tinyEditors].map(editor => editor.contentDocument.getElementById("tinymce").innerHTML);

        // Append different things to formData and send different requests
        // depending on the type of form
        switch(formType) {
            case "frontPage":
                // an array of contents where the subtitle is first, then tinymces
                const allContents = [];
                allContents.push({name: "Otsikko", content: e.target.subtitle.value});

                for (let i = 0; i < tinyContents.length; i++) {
                    allContents.push({
                        name: `Palsta ${i+1}`,
                        content: tinyContents[i]
                    });
                }

                formData.append("contents", JSON.stringify(allContents));

                formData = pageImagesHandler(e, formData);

                apiCall("pages/update", "PATCH", formData, "Sivu tallennettu!");
                break;
            //////////////////////////////////////////////////////////////////////////////////////
            case "newsPage":
                const newsContentsArray = [];
                newsContentsArray.push({name: "Otsikko", content: e.target.newsSubtitle.value});
                formData.append("articlesperpage", e.target.articlesperpage.value);
                formData.append("previewLength", e.target.previewLength.value);
                formData.append("contents", JSON.stringify(newsContentsArray));

                formData = pageImagesHandler(e, formData);
                
                apiCall("pages/update", "PATCH", formData, "Sivu tallennettu!");
                break;
            //////////////////////////////////////////////////////////////////////////////////////
            case "productsPage":
                const productContentsArray = [];
                productContentsArray.push({name: "Otsikko", content: e.target.productSubtitle.value});
                formData.append("contents", JSON.stringify(productContentsArray));

                formData = pageImagesHandler(e, formData);

                apiCall("pages/update", "PATCH", formData, "Sivu tallennettu!");
                break;
            //////////////////////////////////////////////////////////////////////////////////////
            case "servicesPage":
                const serviceContentsArray = [];
                serviceContentsArray.push({name: "Otsikko", content: e.target.serviceSubtitle.value});
                formData.append("contents", JSON.stringify(serviceContentsArray));

                formData = pageImagesHandler(e, formData);

                apiCall("pages/update", "PATCH", formData, "Sivu tallennettu!");
                break;
            //////////////////////////////////////////////////////////////////////////////////////
            case "contactPage":
                const contentsArray = [];

                contentsArray.push({name: "Otsikko", content: e.target.subtitle.value});
                contentsArray.push({name: "Yrityksen nimi", content: e.target.companyName.value});
                contentsArray.push({name: "Osoiterivi 1", content: e.target.adress1.value});
                contentsArray.push({name: "Osoiterivi 2", content: e.target.adress2.value});
                contentsArray.push({name: "Osoiterivi 3", content: e.target.adress3.value});
                contentsArray.push({name: "Yhteystiedot rivi 1", content: e.target.contact1.value});
                contentsArray.push({name: "Yhteystiedot rivi 2", content: e.target.contact2.value});
                contentsArray.push({name: "Yhteystiedot rivi 3", content: e.target.contact3.value});
                contentsArray.push({name: "Y-tunnus rivi 1", content: e.target.company1.value});
                contentsArray.push({name: "Y-tunnus rivi 2", content: e.target.company2.value});
                contentsArray.push({name: "Y-tunnus rivi 3", content: e.target.company3.value});
                //contentsArray.push({name: "Facebook", content: e.target.facebook.value});
                //contentsArray.push({name: "Instagram", content: e.target.instagram.value});
                console.log(e.target.someName);
                console.log(e.target.someLink);

                if (e.target.someName && e.target.someLink) {
                    // if there are more than one, loop through the array, otherwise just add the singular social media
                    if (e.target.someName instanceof NodeList) {
                        for (let i = 0; i < e.target.someName.length; i++) {
                            contentsArray.push({name: e.target.someName[i].value, content: e.target.someLink[i].value});
                        }
                    } else {
                        contentsArray.push({name: e.target.someName.value, content: e.target.someLink.value});
                    }
                }

                formData.append("contents", JSON.stringify(contentsArray));

                formData = pageImagesHandler(e, formData);

                apiCall("pages/update", "PATCH", formData, "Sivu tallennettu!");
                break;
            //////////////////////////////////////////////////////////////////////////////////////
            case "article":
                formData.append("title", e.target.title.value);
                formData.append("date", e.target.date.value); // format: YYYY-MM-DD
                formData.append("public", e.target.visibility.value); // "true" or "false"
                formData.append("content", tinyContents[0]); // the first (and only) tinymce field

                const cropValue = e.target.crop.value;
                console.log(JSON.parse(cropValue));

                const scalingInfo = e.target.scaling.value;
                console.log(JSON.parse(scalingInfo));

                let isNewImage;
                if (e.target.articleimage.value) {
                    formData.append("articleimage", e.target.articleimage.files[0]);
                    isNewImage = true;
                } else {
                    isNewImage = false;
                }

                const info = {
                    crop: cropValue, // x, y, width, height
                    scaling: scalingInfo, // img original size, img scaled-down canvas size, etc
                    isNewImg: isNewImage // boolean
                }
                formData.append("articleImageInfo", JSON.stringify(info));

                if (isNew) {
                    endpoint = "articles/create";
                    method = "POST";
                } else {
                    endpoint = "articles/update";
                    method = "PATCH";
                }

                apiCall(endpoint, method, formData, "Artikkeli tallennettu!");
                break;
            //////////////////////////////////////////////////////////////////////////////////////
            case "product":
                formData.append("productName", e.target.title.value);
                const addonArr = [];

                // if both types of addon fields exist
                if (e.target.addonName && e.target.addonPrice) {
                    const addonNames = e.target.addonName;
                    console.log(addonNames);
                    //const addonPrices = e.target.addonPrice[];

                    //todo maybe: check that addonName.length === addonPrice.length?
                    if (e.target.addonName.length > 1) {
                        for (let i = 0; i < e.target.addonName.length; i++) {
                            const addon = {
                                addonName: e.target.addonName[i].value,
                                addonPrice: e.target.addonPrice[i].value,
                            };
                            addonArr.push(addon);
                        }
                    } else { // this is needed because if not an array but singular, [i] syntax doesn't work
                        const addon = {
                            addonName: e.target.addonName.value,
                            addonPrice: e.target.addonPrice.value,
                        };
                        addonArr.push(addon);
                    }

                }
                console.log(addonArr);

                const productImageInfo = []; // later becomes an array of info objects, one for each image

                let isNewImg; // boolean

                console.log(e.target.productimage);
                if (e.target.productimage) {
                    // if there are more than one, loop through the array, otherwise just add the singular image
                    if (e.target.productimage.length > 1) {
                        for (let i = 0; i < e.target.productimage.length; i++) {

                            if (e.target.productimage[i].value) {
                                formData.append("productimage", e.target.productimage[i].files[0]);
                                isNewImg = true;
                            } else {
                                console.log("something");
                                isNewImg = false;
                            }

                            const info = {
                                crop: e.target.crop[i].value,
                                scaling: e.target.scaling[i].value,
                                isNewImg: isNewImg,
                                filename: e.target.filename[i].value,
                            }
                            productImageInfo.push(info);
                        }
                    } else {
                        if (e.target.productimage.value) { // if the image exists in file input field
                            formData.append("productimage", e.target.productimage.files[0]);
                            isNewImg = true;
                        } else { // if the input field is empty and the image is already on the server
                            // no image file gets added, backend code crops based on the existing image inside uploads folder
                            console.log("existing image or no image");
                            isNewImg = false;
                        }

                        const info = {
                            crop: e.target.crop.value,
                            scaling: e.target.scaling.value,
                            isNewImg: isNewImg,
                            filename: e.target.filename.value,
                        }
                        productImageInfo.push(info);
                    }

                    console.log(productImageInfo);
                }

                const categories = JSON.parse(e.target.categories.value);
                const categoryId = categories.find(cat => cat.catName === e.target.productCategory.value)._id;

                formData.append("categoryId", categoryId);
                formData.append("productBrief", e.target.brief.value);
                formData.append("productText", tinyContents[0]);
                formData.append("price", e.target.price.value);
                formData.append("tax", e.target.tax.value);
                formData.append("addons", JSON.stringify(addonArr)); // an array of addon objects
                formData.append("productImageInfo", JSON.stringify(productImageInfo)); // an array of img info objects
                formData.append("productCategory", e.target.productCategory.value);

                if (e.target.campaignPrice) {
                    formData.append("campaignPrice", e.target.campaignPrice.value);
                }

                if (e.target.campaignText) {
                    formData.append("campaignText", e.target.campaignText.value)
                }

                if (isNew) {
                    endpoint = "products/create";
                    method = "POST";
                } else {
                    endpoint = "products/update";
                    method = "PATCH";
                }
        
                apiCall(endpoint, method, formData, "Tuote tallennettu!");
                break;
            //////////////////////////////////////////////////////////////////////////////////////
            case "category":
                formData.append("catName", e.target.title.value);
                formData.append("catDescription", tinyContents[0]);

                if (isNew) {
                    endpoint = "categories/create";
                    method = "POST";
                } else {
                    endpoint = "categories/update";
                    method = "PATCH";
                }
        
                apiCall(endpoint, method, formData, "Tuotekategoria tallennettu!");
                break;
            //////////////////////////////////////////////////////////////////////////////////////
            case "service":
                formData.append("serviceName", e.target.title.value);
                formData.append("servicePrice", e.target.servicePrice.value);
                // formData.append("date", e.target.date.value); // format: YYYY-MM-DD
                formData.append("public", e.target.visibility.value); // "true" or "false"
                formData.append("content", tinyContents[0]); // the first (and only) tinymce field

                const servicecropValue = e.target.crop.value;
                console.log(JSON.parse(servicecropValue));

                const servicescalingInfo = e.target.scaling.value;
                console.log(JSON.parse(servicescalingInfo));

                let serviceisNewImage;
                if (e.target.serviceimage.value) {
                    formData.append("serviceimage", e.target.serviceimage.files[0]);
                    serviceisNewImage = true;
                } else {
                    serviceisNewImage = false;
                }

                const serviceinfo = {
                    crop: servicecropValue, // x, y, width, height
                    scaling: servicescalingInfo, // img original size, img scaled-down canvas size, etc
                    isNewImg: serviceisNewImage // boolean
                }
                formData.append("serviceImageInfo", JSON.stringify(serviceinfo));

                if (e.target.campaignPrice) {
                    formData.append("campaignPrice", e.target.campaignPrice.value);
                }

                if (e.target.campaignText) {
                    formData.append("campaignText", e.target.campaignText.value);
                }

                if (isNew) {
                    endpoint = "services/create";
                    method = "POST";
                } else {
                    endpoint = "services/update";
                    method = "PATCH";
                }

                apiCall(endpoint, method, formData, "Palvelu tallennettu!");
                break;
            //////////////////////////////////////////////////////////////////////////////////////
            default:
                console.log("unknown form type!"); // not page, post or product; or an error occurred
                Swal.fire(
                    "Hups!",
                    `Tietojen tallennus epäonnistui... <br /> <span class="error-text">Tuntematon sisältötyyppi</span>`,
                    "error"
                );
                return;
        }

        // prints all formdata things one by one for debugging purposes
        for (var pair of formData.entries()) {
            console.log(pair[0]+ ': ' + pair[1]);
        }

    }

    // This function handles both soft delete and restore, because backend code flips the boolean accordingly
    function trashAndRestore(postType, postObject) {

        let buttonText, title, text, successMessage;
        if (!(postObject._deleted === true)) {
            title = "Poisto";
            text = "Haluatko siirtää kohteen roskakoriin?";
            buttonText = "Poista";
            successMessage = "Kohde siirretty roskakoriin!";
        } else {
            title = "Palautus roskakorista";
            text = "Haluatko palauttaa kohteen roskakorista?";
            buttonText = "Palauta";
            successMessage = "Kohde palautettu roskakorista!";
        }

        if (postType && postObject && typeof postObject === "object") {
            const formData = new FormData();
            let endpoint;

            switch(postType) {
                case "article":
                    endpoint = "articles/update";
                    break;
                case "category":
                    endpoint = "categories/update";
                    break;
                case "product":
                    endpoint = "products/update";
                    break;
                case "service":
                    endpoint = "services/update";
                    break;
                default:
                    console.log("Invalid post type");
                    return;
            }

            for (const key in postObject) {
                if (key === "addons") {
                    console.log("addons");
                    formData.append(key, JSON.stringify(postObject[key]));
                } else if (key === "serviceDescription") {
                    formData.append("content", postObject[key]);
                } else {
                    formData.append(key, postObject[key]);
                }
            }

            // if formdata doesn't have a _deleted field, it means we need to add it
            if (!formData.has("_deleted")) {
                formData.append("_deleted", false);
            }

            Swal.fire({
                title: title,
                text: text,
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: buttonText,
                cancelButtonText: "Peruuta"
            }).then((result) => {
                if (result.value) {
                    apiCall(endpoint, "PATCH", formData, successMessage);
                }
            });

            for (var pair of formData.entries()) {
                console.log(pair[0]+ ': ' + pair[1]);
            }
        }
    }

    function deleteContents(endpoint, body, title="Lopullinen poisto", text="Oletko aivan varma? Poistettua kohdetta ei voi palauttaa!", successMessage="Poisto onnistui!", failMessage="Poisto epäonnistui...") {
        Swal.fire({
            title: title,
            text: text,
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Poista",
            cancelButtonText: "Peruuta"
        }).then((result) => {
            if (result.value) {
                fetch(`${conf.server_base_url}/${endpoint}`, {
                    method: "DELETE",
                    credentials:"include",
                    headers: {
                        "Content-Type": "application/json",
                    },
                    body: JSON.stringify(body),
                })
                .then(async response => {
                    //IF SERVER AUTH DETECTS BAD COOKIE, KICK TO LOGIN
                    if (response.status === 401){
                        window.location = `${conf.base_route}/login/`
                    }
                    if (!response.ok) {
                        const text = await response.text();
                        throw new Error(text);
                    } else {
                        return response.json();
                    }
                })
                .then(json => {
                    console.log(json);
                    Swal.fire(
                        successMessage,
                        "",
                        "success"
                    ).then(() => {
                        window.location.href = conf.base_route;
                    });
                })
                .catch(error => {
                    console.log(error);
                    Swal.fire(
                        "Hups!",
                        `${failMessage} <br />  <span class="error-text">${error}</span>`,
                        "error"
                    );
                });
            }
        });
    }
    return (
        <>
        <IsLoggedContext.Provider value = {isLogged}>
        <PagesContext.Provider value={pages}>
            <ProductsContext.Provider value={products}>
                <ArticlesContext.Provider value={articles}>
                    <ServicesContext.Provider value={services}>
                        <Router>
                            <ScrollToTop />
                            <Header />
                            {(pages && articles && products && services) ?
                            <Container fluid className="admin-interface-container">
                            <Row>
                                <Col sm="4" md="3" xl="2" className="pl-0 container">
                                        <Sidebar setProducts={setProducts} stringToUrl={stringToUrl} />
                                </Col>
                                <Col sm="8" md="9" xl="10" className="admin-interface-content mb-4">
                                    <Container fluid className="mt-4 pl-0" >
                                    <Switch>
                                        <Route
                                            exact path={`${conf.base_route}`}
                                            render={() => (
                                                <>
                                                    <AdminHelmet />
                                                    <div>
                                                    <h1>Tervetuloa sivustosi hallintapaneeliin!</h1>
                                                        <p>
                                                            Täällä voit muokata sivujen sisältöä.<br />
                                                            Voit lukea hallintapaneelin käyttöohjeet <Link to={`${conf.base_route}/ohjeet`}>tästä linkistä</Link>.
                                                        </p>
                                                    </div>
                                                </>
                                            )}
                                        />
                                        <Route
                                            path={`${conf.base_route}/etusivu`}
                                            render={() => (
                                                <Frontpage saveContents={saveContents} />
                                            )}
                                        />
                                        <Route
                                            path={`${conf.base_route}/uutiset`}
                                            render={() => (
                                                <div>
                                                    <NewsPage saveContents={saveContents}/>
                                                </div>
                                            )}
                                        />  
                                        <Route
                                            exact path={`${conf.base_route}/tuotteet`}
                                            render={() => (
                                                <div>
                                                    <ProductsPage saveContents={saveContents}/>
                                                </div>
                                            )}
                                        />
                                        <Route
                                            exact path={`${conf.base_route}/palvelut`}
                                            render={() => (
                                                <div>
                                                    <ServicesPage saveContents={saveContents}/>
                                                </div>
                                            )}
                                        />  
                                        <Route
                                            path={`${conf.base_route}/yhteystiedot`}
                                            render={() => (
                                                <div>
                                                    <Contact saveContents={saveContents}/>
                                                </div>
                                            )}
                                        />          
                                        <Route
                                            path={`${conf.base_route}/uusi_artikkeli`}
                                            render={() => (
                                                <Article status="new" saveContents={saveContents} />
                                            )}
                                        />
                                        <Route
                                            path={`${conf.base_route}/ajankohtaista/:id`}
                                            render={(props) => (
                                                <Article thisArticleId={props.match.params.id} saveContents={saveContents} deleteContents={deleteContents} moveToTrash={trashAndRestore} />
                                            )}
                                        />
                                        <Route
                                            path={`${conf.base_route}/uusi_kategoria`}
                                            render={() => (
                                                <Category status="new" saveContents={saveContents} />
                                            )}
                                        />
                                        <Route 
                                            path={`${conf.base_route}/kategoriat/:categoryName`}
                                            render={(props) => (
                                                <Category thisCategoryName={props.match.params.categoryName} saveContents={saveContents} deleteContents={deleteContents} moveToTrash={trashAndRestore} />
                                            )}
                                        />
                                        <Route
                                            path={`${conf.base_route}/uusi_tuote`}
                                            render={() => (
                                                <Product status="new" saveContents={saveContents} />
                                            )}
                                        />
                                        <Route 
                                            path={`${conf.base_route}/tuotteet/:id`}
                                            render={(props) => (
                                                <Product thisProductId={props.match.params.id} saveContents={saveContents} deleteContents={deleteContents} moveToTrash={trashAndRestore} />
                                            )}
                                        />
                                        <Route
                                            path={`${conf.base_route}/uusi_palvelu`}
                                            render={() => (
                                                <Service status="new" saveContents={saveContents} />
                                            )}
                                        />
                                        <Route
                                            path={`${conf.base_route}/palvelut/:id`}
                                            render={(props) => (
                                                <Service thisServiceId={props.match.params.id} saveContents={saveContents} deleteContents={deleteContents} moveToTrash={trashAndRestore} />
                                            )}
                                        />
                                        <Route
                                            path={`${conf.base_route}/jarjestys`}
                                            render={() => (
                                                <SeasonOrder />
                                            )}
                                        />
                                        <Route
                                            exact path={`${conf.base_route}/kuvat`}
                                            render={() => (
                                                <ImageSettings pageNum={1} />
                                            )}
                                        />
                                        <Route
                                            exact path={`${conf.base_route}/kuvat/sivu/:pagenum`}
                                            render={(props) => (
                                                <ImageSettings pageNum={props.match.params.pagenum} />
                                            )}
                                        />
                                        <Route
                                            path={`${conf.base_route}/roskakori`}
                                            render={() => (
                                                <TrashBin deleteContents={deleteContents} restoreFromTrash={trashAndRestore} />
                                            )}
                                        />
                                        <Route
                                            path={`${conf.base_route}/login`}
                                            render={() => (
                                                <Login />
                                            )}
                                        />
                                        <Route
                                            path={`${conf.base_route}/ohjeet`}
                                            render={() => (
                                                <Instructions />
                                            )}
                                        />
                                        <Route
                                            path="/unohditkosalasanasi"
                                            render={() => (
                                                <ForgottenPasswordEmail/>
                                            )}
                                        
                                        />
                                        <Route
										path={`${conf.base_route}/reset/:token`}
                                            render={() => (
                                                <ForgottenPasswordReset/>
                                            )}
                                        />
                                        <Route
                                            path="/asetukset"
                                            render={() => (
                                                <UserSettings/>
                                            )}
                                        />

                                        <Route component={NotFoundPage} />
                                    </Switch>
                                    </Container>
                                </Col>
                            </Row>
                            </Container>
                        : <LoadingSpinner />
                    }
                        </Router>
                    </ServicesContext.Provider>
                </ArticlesContext.Provider>
            </ProductsContext.Provider>
        </PagesContext.Provider>
        </IsLoggedContext.Provider>
        </>
    );
};


export default App;