import React, {useState} from "react";
import { Editor } from "@tinymce/tinymce-react";
import conf from "./config";
import DOMPurify from "dompurify";
import LoadingSpinner from "./LoadingSpinner";

console.log(conf["tinymce_apikey"]);

const Wysiwyg = (props) => {
    const {page, field, initial} = props;

    const [loaded, setLoaded] = useState(false);

    const handleEditorChange = (content, editor) => {
        console.log('Content was updated:', content);
    }

    return (
      <>
        {loaded ? "" : <LoadingSpinner />}
        <Editor
            initialValue={DOMPurify.sanitize(initial)}
            apiKey={conf.tinymce_apikey} // Development API Key, update this for final build
            init={{
              height: 500,
              menubar: true,
              language: "fi",
              language_url: `${conf.homepage}/langs/fi.js`,
              // the idea is to make this list match the backend's xss.whitelist as closely as possible to avoid
              // discrepancies between editing and final result, and thus confusion
              valid_elements : "@[id|class|style|title|dir<ltr?rtl|lang|xml::lang],"
              + "a[href|target|title|class],strong/b,em/i,strike,u,"
              + "#p,-ol[type|compact],-ul[type|compact],-li,br,img[longdesc|usemap|"
              + "src|border|alt=|title|hspace|vspace|width|height|align],-sub,-sup,"
              + "-blockquote,-table[border=0|cellspacing|cellpadding|width|frame|rules|"
              + "height|align|summary|bgcolor|background|bordercolor],-tr[rowspan|width|"
              + "height|align|valign|bgcolor|background|bordercolor],tbody,thead,tfoot,"
              + "#td[colspan|rowspan|width|height|align|valign|bgcolor|background|bordercolor"
              + "|scope],#th[colspan|rowspan|width|height|align|valign|scope],caption,-div,"
              + "-span,-code,-pre,address,-h1,-h2,-h3,-h4,-h5,-h6,hr[size|noshade],-font[face"
              + "|size|color],dd,dl,dt,cite,abbr,acronym,del[datetime|cite],ins[datetime|cite],"
              + "object[classid|width|height|codebase|*],param[name|value|_value],embed[type|width"
              + "|height|src|*],map[name],area[shape|coords|href|alt|target],bdo,"
              + "button,col[align|char|charoff|span|valign|width],colgroup[align|char|charoff|span|"
              + "valign|width],dfn,fieldset,"
              + "kbd,label[for],legend,noscript,optgroup[label|disabled],option[disabled|label|selected|value],"
              + "q[cite],samp,select[disabled|multiple|name|size],small,"
              + "textarea[cols|rows|disabled|name|readonly],tt,var,big",
              plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime table paste code help wordcount'
              ],
              toolbar:
                'undo redo | formatselect | bold italic backcolor | \
                alignleft aligncenter alignright alignjustify | \
                bullist numlist outdent indent | removeformat | help',

                paste_data_images: true,
                //images_upload_url: "http://localhost:4001/imageupload",
                images_upload_handler: function handleri(blobInfo, success, failure, progress) {
                const formData = new FormData();
                formData.append('imagefile', blobInfo.blob());
            
                fetch(`${conf.server_base_url}/images/imageupload`, {
                    method: "POST",
                    body: formData
                })
                .then(response => {
                    if (!response.ok) {
                        return response.text().then(text => {
                            throw new Error(text)
                        });
                    } else {
                        return response.json();
                    }
                })
                .then(json => {
                    console.log(json);
                    success(`${conf.server_base_url}/${json.location}`);
                })
                .catch(error => {
                    console.log(error);
                    failure(error);
                });
                }
            }}
            onEditorChange={handleEditorChange}
            onLoadContent={() => setLoaded(true)}
        />
      </>
    );
};

export default Wysiwyg;
