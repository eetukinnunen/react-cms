import React, {useState} from "react";
import conf from "./config";
import {Form, Col, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';

let urlIsGood = false;

const checkIfToken = async() => {
    const token = window.location.pathname.split("/").pop()
    fetch(`${conf.server_base_url}/user/checkreseturl`, {
        method: "PATCH",
        headers: {'Content-Type':'application/json'},
        body: JSON.stringify({
            "token": token,
            })
        })
        .then (async response =>
        response.status === 200 ?
            urlIsGood = true
        :   urlIsGood = false)
        .catch(error => {
            // console.log(error)
        });
}

checkIfToken();
console.log(urlIsGood);
const ForgottenPasswordReset = () => {

    const [newPassword, setNewPassword] = useState("");
    const [newPassword2, setNewPassword2] = useState("");
    const [errMsg, setErrMsg] = useState("");

    //Gets the part after last "/" in URL.
    const token = window.location.pathname.split("/").pop()
    
    const handleReset = () => {
        if (((newPassword || newPassword2) === "")){
            // console.log("new pws empty")
            setErrMsg("Salasanakentät tyhjiä");
        }
        else if (newPassword !== newPassword2){
            // console.log("new pws dont match")
            setErrMsg("Uudet salasanat eivät täsmää");
        }
        else{
            fetch(`${conf.server_base_url}/user/resetpassword`, {
            method: "PATCH",
            headers: {'Content-Type':'application/json'},
            body: JSON.stringify({
                "token": token,
                "newPassword": newPassword,
                })
            })
            .then(json => {
                // console.log(json);
                // success message/popup
                Swal.fire(
                    "Salasana päivitetty",
                    "",
                    "success"
                ).then(() => {
                    window.location.href = `${conf.base_route}/login/`
                });
            })
            .catch(error => {
                // console.log(error);
                // error message/popup
                Swal.fire(
                    "Hups!",
                    `Toiminto epäonnistui... <br />  <span class="error-text">${error}</span>`,
                    "error"
                );
            });
        }
    };

if (urlIsGood === true){
return(
    <div>        
        <Form>
            <Form.Label>Vaihda salasana</Form.Label>
            <Form.Row>
                <Col sm="12">
                    <Form.Control type="password" placeholder="Uusi salasana" value={newPassword} name="Password2"onChange={({target}) => setNewPassword(target.value)}/>
                </Col>
            </Form.Row>            
            <Form.Row>
                <Col sm="12">
                    <Form.Control type="password" placeholder="Uusi salasana uudestaan" value={newPassword2} name="Password3"onChange={({target}) => setNewPassword2(target.value)}/>
                </Col>
            </Form.Row>
            
        <Button onClick={handleReset}>Päivitä salasana</Button>
        <p style={{color: "red"}}>{errMsg}</p>
        </Form>
    </div>
);
}
else{
    return(null);
}
}


export default ForgottenPasswordReset;