import React, {useState, useEffect} from "react";
import { Redirect } from "react-router-dom";
import conf from "./config";
import { Row, Col } from "react-bootstrap";
import Wysiwyg from "./Wysiwyg";
import ImgCropTool from "./ImgCropTool";
import Swal from "sweetalert2";
import SubmitButtons from "./SubmitButtons";
import ServicesContext from "../context/services.context.js";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBullhorn, faCalendarAlt, faCommentAlt, faEye, faAlignLeft, faFileImage, faEuroSign, faTag } from '@fortawesome/free-solid-svg-icons';
import AdminHelmet from "./AdminHelmet";

const Service = (props) => {
    const {status, saveContents, deleteContents, moveToTrash, thisServiceId} = props;
    const services = React.useContext(ServicesContext);
    
    const [initialized, setInitialized] = useState(false);
    console.log(initialized);
    const [thisService, setthisService] = useState({});
    const [shortCampaignText, setShortCampaignText] = useState("");
    const [campaignWordCount, setCampaignWordCount] = useState("");

    console.log(typeof setInitialized);

    let helmetTitle;
    if (status === "new") {
        helmetTitle = "Uusi palvelu";
    } else if (thisService) {
        helmetTitle = thisService.serviceName;
    }

    function deleteHandler() {
        //deleteContents(`services/delete/${thisService._id}`, thisService, "Artikkelin poisto", "Oletko varma että haluat poistaa tämän artikkelin?", "Artikkeli poistettu!", "Artikkelin poistaminen epäonnistui...");
        moveToTrash("service", thisService);
    }

    function initialize() {
        if (status === "new") {
            // everything is created from scratch, in the end a post method is submitted
    
            // creates a default service object for the input fields' default values
            setthisService({
                serviceName: "",
                //id generation happens in backend after submit, so here id is undefined or empty
                visibility: false,
                serviceDescription: "",
            });
        } else {
            setthisService(services.find(service => service._id === props.thisServiceId && service._deleted !== true));
        }

        setInitialized(true);
    }

    // Runs when service name prop changes (including on first load)
    useEffect(() => {
        if (initialized) { // if already initialized, reload the page to reset all the data to avoid problems
            window.location.reload();
        }
        initialize();
        console.log("initialized");
    }, [props.thisServiceId]);

    console.log(thisService);

    function setCampaignTextHelper (description) { 
        setShortCampaignText(description);
        if (description.length === 0) {
            setCampaignWordCount("word-count-ok");
        } else if (description.length < 80 ) {
            setCampaignWordCount("word-count-ok");   
        } else if (description.length >= 80 && description.length <=99 ) {
            setCampaignWordCount("word-count-yellow");
        } else if (description.length >= 100) {
            setCampaignWordCount("word-count-red");
        }
    }

    const [campaign, setCampaign] = useState(false);

    // if service has campaignprice, set it
    useEffect(() => {
        if(thisService) {
            if(thisService.campaignPrice) {
                setCampaign(true);
            } else {
                setCampaign(false);
        }
    }
    },[thisService]);

    return (
        <>
            <AdminHelmet title={helmetTitle} />
            <div>
            {initialized ?
                thisService ?
                <form onSubmit={saveContents} encType="multipart/form-data">
                    <input type="hidden" id="formType" name="formType" value="service" />
                    <input type="hidden" id="postId" name="id" value={thisService._id} />
                    <Row>
                        <Col md="6">
                        
                            <label htmlFor="title" className="form-input-header"><FontAwesomeIcon icon={faBullhorn} size="1x" className="mr-3"/>Palvelun nimi</label>
                            <input className="form-control" type="text" id="title" name="title" defaultValue={thisService.serviceName} required />
                        </Col>
                        <Col md="6">
                            <label htmlFor="servicePrice" className="form-input-header"><FontAwesomeIcon icon={faEuroSign} size="1x" className="mr-3"/>Hinta</label>
                            <input className="form-control" type="number" id="servicePrice" name="servicePrice" defaultValue={thisService.servicePrice} min="0.00" step="0.01" required={campaign} />
                        </Col>
                    </Row>
                    <Row>
                        <Col xs="12">
                            <label className="form-input-header mt-4"><FontAwesomeIcon icon={faTag} size="1x" className="mr-3"/>Kampanja</label>
                        </Col>
                        <Col xs="12" sm="6">
                            <label htmlFor="campaign">Aseta kampanjatuotteeksi</label>
                            <input type="checkbox" name="campaignSelect" checked={campaign} onChange={() => setCampaign(!campaign)} />
                        </Col>
                        {campaign &&
                            <Col xs="12" sm="8">
                                <label htmlFor="campaignPrice"><FontAwesomeIcon icon={faEuroSign} size="1x" className="mr-3"/>Kampanjahinta</label>
                                <input className="form-control" type="number" name="campaignPrice" defaultValue={thisService.campaignPrice} min="0.00" step="0.01" required />
                                <label htmlFor="campaignText"><FontAwesomeIcon icon={faCommentAlt} size="1x" className="mr-3"/>Palvelulle lyhyt kampanjateksti tähän. Ohjepituus 100 merkkiä tai alle. <span className={campaignWordCount}>Merkkejä {shortCampaignText.length}/100</span></label>
                                <input className="form-control" type="text" id="campaignText" name="campaignText" defaultValue={thisService.campaignText} onChange={(e) => setCampaignTextHelper(e.target.value)} required />
                            </Col>
                        }
                    </Row>
                    <Row>
                        <Col sm={12}>
                            <h3 className="form-input-header mt-4"><FontAwesomeIcon icon={faEye} size="1x" className="mr-3"/>Palvelun julkisuus</h3>
                            <input type="radio" id="private" name="visibility" value="false" defaultChecked={!thisService.public} className="mr-2"/>
                            <label htmlFor="private" className="mr-4">Yksityinen</label>
                            <input className="ml-3" type="radio" id="public" name="visibility" value="true" defaultChecked={thisService.public} className="mr-2"/>
                            <label htmlFor="public" className="mr-4">Julkinen</label>
                        </Col>
                    </Row>
                    <h3 className="form-input-header mt-4"><FontAwesomeIcon icon={faAlignLeft} size="1x" className="mr-3"/>Palvelun teksti</h3>
                    <Wysiwyg page="jotain" initial={thisService.serviceDescription} />

                    <label htmlFor="serviceimage" className="form-input-header mt-4"><FontAwesomeIcon icon={faFileImage} size="1x" className="mr-3"/>Palvelukuva</label>
                    <ImgCropTool aspectRatio={16/9} id={thisService.id} imgSrc={`${conf.server_base_url}/${thisService.serviceimage}`} inputName="serviceimage" keepSelection="true" />

                    <SubmitButtons status={status} deleteHandler={deleteHandler} label="palvelu"/>
                </form>
                : <Redirect to={conf.base_route} />
            : ""
            }
            </div>
        </>
    );
};

export default Service;