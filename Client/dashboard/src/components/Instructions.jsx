import React from "react";
import AdminHelmet from "./AdminHelmet";

const Instructions = () => {

    return (
        <>
            <AdminHelmet title="Ohjeet" />

            <div>
                <h3>Hallintapaneelin käyttöohjeet</h3>
                <ul>
                    <li>
                        <h4>Tekstikentät</h4>
                        <p>
                            Monipuolisiin tekstieditoreihin pystyt lisäämään mm. kuvia, linkkejä, ja tekstin muotoiluja. Turvallisuussyistä tietyt editoriin lisätyt elementit saatetaan karsia tallennusvaiheessa pois.
                        </p>
                    </li>
                    <li>
                        <h4>Sivut</h4>
                        <ul>
                            <li>Uutiset-muokkaussivulla voit määrittää, montako artikkelia näkyy kerrallaan, ja montako merkkiä pitkä esikatseluteksti on.</li>
                            <li>Yhteystiedot-muokkaussivu sisältää myös alatunnisteen sisällöt.</li>
                        </ul>
                    </li>
                    <li>
                        <h4>Artikkelit</h4>
                        <ul>
                            <li>Artikkelille määritetty päivämäärä vaikuttaa vain artikkelien järjestykseen sivustolla, julkinen artikkeli näkyy saman tien vaikka päivämäärä olisikin tulevaisuudessa.</li>
                            <li>Yksityiseksi asetetut artikkelit näkyvät vain hallintapaneelissa.</li>
                            <li>Jos luot useamman samannimisen artikkelin, niiden osoitteiden perään lisätään automaattisesti järjestysnumero.</li>
                        </ul>
                    </li>
                    <li>
                        <h4>Tuotteet</h4>
                        <ul>
                            <li>Jos tuotteelle ei ole määritelty hintaa, tuotteet-sivulla lukee hinnan tilalla “Pyydä tarjous”.</li>
                            <li>Tuotteelle voi lisätä halutun määrän tuotekuvia. Jos kuvia on enemmän kuin yksi, tuotesivulla näkyy automaattisesti kuvakaruselli.</li>
                            <li>Tuotteelle voi lisätä halutun määrän lisäosia, joita käyttäjä voi valita tiedustelulomakkeeseen sivustolla.</li>
                        </ul>
                    </li>
                    <li>
                        <h4>Palvelut</h4>
                        <ul>
                            <li>Jos palvelulle ei ole määritelty hintaa, palvelut-sivulla lukee hinnan tilalla “Pyydä tarjous”.</li>
                            <li>Yksityiseksi asetetut palvelut näkyvät vain hallintapaneelissa.</li>
                        </ul>
                    </li>
                    <li>
                        <h4>Kampanjat</h4>
                        <p>
                            Tuotteelle tai palvelulle voi asettaa kampanjan. Kampanja voi olla vain yhdellä tuotteella tai palvelulla kerrallaan, ja tallentamisen jälkeen se poistuu muista kohteista. Jos kampanja on aktiivisena, hinta- ja kampanjahintakentät ovat pakollisia. Kampanjatuote tai -palvelu näkyy sivuston etusivulla.
                        </p>
                    </li>
                    <li>
                        <h4>Kuvat</h4>
                        <ul>
                            <li>Sallitut tiedostopäätteet ovat .jpg, .jpeg, .png ja .gif.</li>
                            <li>Kuvan sallittu maksimikoko on 5 megatavua. Muista, että mitä pienempi kuva, sitä nopeammin se latautuu sivuston vierailijoille.</li>
                            <li>Tietyissä sijainneissa kuvat täytyy rajata tietyssä kuvasuhteessa. Ota tämä huomioon kuvia valitessasi. Kuvasuhteet ovat:
                                <ul>
                                    <li>Artikkelikuvat: 16/9</li>
                                    <li>Palvelukuvat: 16/9</li>
                                    <li>Tuotekuvat: 3/2</li>
                                    <li>Muissa paikoissa rajauksen kuvasuhde on vapaa.</li>
                                </ul>
                            </li>
                            <li>Tietyissä paikoissa kuvan rajaaminen on pakollista kuvasuhteen säilyttämiseksi. Muualla kuvan rajaamisen voi ohittaa klikkaamalla rajatun alueen ulkopuolelle.</li>
                            <li>Tarpeettomiksi jääneet kuvat voi poistaa hallintapaneelin kautta. Tämä toiminto poistaa kuvan palvelimelta lopullisesti. Käytä tätä toimintoa varsinkin, jos palvelimen tila uhkaa loppua.</li>
                        </ul>
                    </li>
                    <li>
                        <h4>Vuodenajat</h4>
                        <p>Vuodenajan vaihtaminen sivupalkista vaikuttaa sivuston sisältöön ja ulkoasuun.</p>
                        <ul>
                            <li>Värimaailma vaihtuu vihreän ja sinisen välillä</li>
                            <li>Tietyt kuvat vaihtuvat kesäisten ja talvisten välillä, kunhan ylläpitäjä on lisännyt kunkin kuvaparin oikein</li>
                            <li>Tuotekategorioiden ja palveluiden järjestys vaihtuu</li>
                        </ul>
                    </li>
                    <li>
                        <h4>Roskakori</h4>
                        <p>
                            Artikkelin, tuotteen, tuotekategorian tai palvelun poistaminen sen muokkausnäkymästä siirtää sen roskakoriin. Sieltä sen voi palauttaa takaisin käyttöön tai poistaa lopullisesti.
                        </p>
                    </li>
                    <li>
                        <h4>Käyttäjäasetukset</h4>
                        <p>
                        Käyttäjäasetuksista voit vaihtaa tunnuksesi salasanan tai sähköpostiosoitteen. Käyttäjätunnuksesi sähköpostia käytetään kirjautumiseen, salasanan palauttamiseen ja yhteydenottolomakkeilla lähetettyjen viestien vastaanottamiseen.
                        </p>
                    </li>
                </ul>
            </div>
        </>
    );
};

export default Instructions;