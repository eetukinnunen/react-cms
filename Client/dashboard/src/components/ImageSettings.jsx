import React, {useState, useEffect} from "react";
import conf from "./config";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    NavLink
} from "react-router-dom";
import Swal from 'sweetalert2';
import { ButtonGroup } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrash, faAngleLeft, faAngleRight } from "@fortawesome/free-solid-svg-icons";
import { v4 as uuidv4 } from 'uuid';
import AdminHelmet from "./AdminHelmet";


// Function for the prev & next arrow links
const PageLink = (props) => {
    const {num, text, disabled} = props;
    const classList = `btn btn-sm btn-primary mt-4 ${disabled ? `disabled` : ``}`;

    const [anchorTarget, setAnchorTarget] = useState(null);

    useEffect(() => {
        setAnchorTarget(document.getElementById("pageLinks"));
      }, []);

    const handleClick = () => {
        anchorTarget.scrollIntoView({ behavior: "smooth" });
    }

    return (
        <Link key={uuidv4()} id="pageLinks" className={classList} to={`${conf.base_route}/kuvat/sivu/${num}`} onClick={() => handleClick()} >{text}</Link>
    );
};

const ImageSettings = (props) => {
    const {pageNum} = props;
    const pageNumInt = parseInt(pageNum, 10);
    const [imgNames, setImgNames] = useState([]);
    const [pageLinks, setPageLinks] = useState([]);

    const imgsPerPage = 6;

    // gets all filenames (hopefully just images) from backend's upload folder (in alphabetical order),
    // then we map those into img tags and paginate them to avoid having to load 1000 images at once
    function getFileNames() {
        fetch(`${conf.server_base_url}/images/get`,{credentials:"include"})
        .then(response => response.json())
            .then((json) => {
                console.log(json);
                setImgNames(json);
            })
            .catch(error => {
                console.log(error);
                Swal.fire(
                    "Hups!",
                    `Kuvien hakeminen epäonnistui... <br />  <span class="error-text">${error}</span>`,
                    "error"
                );
            });
    }

    useEffect(() => {
        getFileNames();
    }, []);

    useEffect(() => {
        // Same idea as in Client/website/src/News
        const pageAmount = Math.ceil(imgNames.length / imgsPerPage);
        setPageLinks([]);
        for (let i = 1; i <= pageAmount; i++) {
            setPageLinks(pageLinks => [...pageLinks, <NavLink activeStyle={{ background: "#0275d8" }} to={`${conf.base_route}/kuvat/sivu/${i}`} id="pageLinks" className="btn btn-sm mt-4" key={uuidv4()}>{i}</NavLink>])
        }

        // the prev & next arrow links
        setPageLinks(pageLinks => [<PageLink num={pageNumInt -1} text={<FontAwesomeIcon className="text-white" icon={faAngleLeft} />} disabled={pageNumInt > 1 ? false : true} key={uuidv4()} />, ...pageLinks])
        setPageLinks(pageLinks => [...pageLinks, <PageLink num={pageNumInt + 1} text={<FontAwesomeIcon className="text-white" icon={faAngleRight} />} disabled={pageNumInt < pageAmount ? false : true} key={uuidv4()} />])
        

        console.log(`pageNum: ${pageNum}`);
    }, [imgNames, pageNum]);


    function deleteImage(imgName) {
        console.log(imgName)
        fetch(`${conf.server_base_url}/images/location`, {
            method: "POST",
            credentials:"include",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
            },
            body: `imgName=${imgName}`
        })
        .then(response => response.json())
        .then(json => {
            const location = json.map(element => element.message);
            console.log(json);
            Swal.fire({
                title: "Kuvan poisto",
                text: `Oletko varma että haluat poistaa kuvan ${imgName}? ${json.length > 1 ? `Kuvaa käytetään sijainneissa ${location}` 
                                                                        : json.length === 1 ? `Kuvaa käytetään sijainnissa ${location}`
                                                                        : "" }`,
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Poista",
                cancelButtonText: "Peruuta"
            }).then((result) => {
                if (result.value) { // if user clicked yes
                    fetch(`${conf.server_base_url}/images/delete`, {
                        method: "DELETE",
                        credentials:"include",
                        headers: {
                            "Content-Type": "application/x-www-form-urlencoded",
                        },
                        body: `imgName=${imgName}`
                    })
                    .then(response => {
                        if (!response.ok) {
                            return response.text().then(text => {
                                throw new Error(text);
                            });
                        } else {
                            return response.json();
                        }
                    })
                    .then(json => {
                        console.log(json);
                        Swal.fire(
                            "Kuva poistettu!",
                            "",
                            "success"
                        ).then(() => {
                            getFileNames(); // refresh image array (which also updates pagination etc)
                        });
                    })
                    .catch(error => {
                        console.log(error);
                        Swal.fire(
                            "Hups!",
                            `Kuvan poistaminen epäonnistui! <br />  <span class="error-text">${error}</span>`,
                            "error"
                        );
                    });
                }
            });
            
        })
        .catch(error => {
            console.log(error)
        });
        
    }

    return (
        <>
            <AdminHelmet title="Kuvat" />
            <div>
                {imgNames.length > 0 ?
                    <div id="image-list">
                        {imgNames.slice((imgsPerPage * (pageNumInt -1)), (imgsPerPage * pageNumInt)).map(image => {
                            const style = {"backgroundImage": `url("${conf.server_base_url}/${image}")`}
                            return (<div key={uuidv4()} className="img-box mx-1 mt-4" style={style}>
                            <span>{image}</span>
                            {console.log(style)}
                            <button className="btn" onClick={() => deleteImage(image)}>
                                <FontAwesomeIcon className="text-white" icon={faTrash} size="1x" />
                            </button>
                        </div>)
                        })}
                    </div>
                : "loading"}
                <ButtonGroup>
                    {pageLinks}
                </ButtonGroup>
            </div>
        </>
    );
};

export default ImageSettings;