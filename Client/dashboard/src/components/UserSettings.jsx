import React, {useState} from "react";
import conf from "./config";
import {Form, Col, Row, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';

const UserSettings = () => {

    const [password, setPassword] = useState("");
    const [newPassword, setNewPassword] = useState("");
    const [newPassword2, setNewPassword2] = useState("");
    const [email1, setEmail1] = useState("");
    const [email2, setEmail2] = useState("");
    const [errMsg, setErrMsg] = useState("")

    const handlePassword = () => {
        //Checks if new pws are empty or not matching
        if (((newPassword || newPassword2) === "")){
            // console.log("new pws empty")
            setErrMsg("Salasanakentät tyhjiä");
        }
        else if (newPassword !== newPassword2){
            // console.log("new pws dont match")
            setErrMsg("Uudet salasanat eivät täsmää");
        }
        else{
            fetch(`${conf.server_base_url}/user/changepassword`, {
            method: "PATCH",
            credentials:"include",
            headers: {'Content-Type':'application/json'},
            body: JSON.stringify({
                "password": password,
                "newPassword": newPassword,
                })
            })
            .then ((response) =>
            response.status === 200 ?
            Swal.fire(
                "Salasana vaihdettu!",
                "",
                "success")
           : Swal.fire(
                "Hups!",
                `Salasana väärin.`,
                "error"
            ))
            .catch(error => {
                // console.log(error)
            });
        };
    };

    const handleEmail = () => {
        if (((email1 || email2) === "")){
            // console.log("emails empty")
            setErrMsg("Sähköpostikentät tyhjiä");
        }
        else if (email1 !== email2){
            // console.log("Emails don't match")
            setErrMsg("Uudet sähköpostiosoitteet eivät täsmää");
        }
        else{
            fetch(`${conf.server_base_url}/user/changeemail`, {
            method: "PATCH",
            credentials:"include",
            headers: {'Content-Type':'application/json'},
            body: JSON.stringify({
                "email": email1,
                "password": password,
                })
            })
            .then ((response) =>
            response.status === 200 ?
            Swal.fire(
                "Email vaihdettu!",
                "",
                "success")
           : Swal.fire(
                "Hups!",
                `Salasana väärin.`,
                "error"
            ))
            .catch(error => {
                // console.log(error)
            });
        };
    };

return(
<div>
    <Form>      
        <Col sm="12" md ="6">
            
                <Form.Label>Vaihda salasana</Form.Label>
                <Form.Control type="password" placeholder="Uusi salasana" value={newPassword} name="Password2"onChange={({target}) => setNewPassword(target.value)}/>
                <Form.Control type="password" placeholder="Uusi salasana uudestaan" value={newPassword2} name="Password3"onChange={({target}) => setNewPassword2(target.value)}/>
            
            
                <Form.Label>Vaihda sähköpostiosoite</Form.Label>
                <Form.Control type="text" placeholder="Uusi sähköpostiosoite" value={email1} name="email1"onChange={({target}) => setEmail1(target.value)}/>
                <Form.Control type="text" placeholder="Uusi sähköpostiosoite uudestaan" value={email2} name="email2"onChange={({target}) => setEmail2(target.value)}/>
            
            <br></br>
            <br></br>
            
                <Form.Label>Syötä nykyinen salasana jos haluat muuttaa tietoja</Form.Label>
                <Form.Control md="6" type="password" placeholder="Salasana" value={password} name="Password"onChange={({target}) => setPassword(target.value)}/>
            
            
            
                <Button variant="primary" onClick={handlePassword} >Muuta salasana</Button>
                <Button variant="primary" onClick={handleEmail} >Muuta sähköposti</Button>
            
            <br></br>
            <br></br>
            
                <Form.Label style={{color: "red"}}>{errMsg}</Form.Label>
            
        </Col>
    </Form>
</div>
);
}
export default UserSettings;