import React, {useState, useEffect} from "react";
import ArticlesContext from "../context/articles.context.js";
import ProductsContext from "../context/products.context.js";
import ServicesContext from "../context/services.context.js";
import AdminHelmet from "./AdminHelmet.jsx";

const TrashBin = (props) => {
    const {deleteContents, restoreFromTrash} = props;
    const articles = React.useContext(ArticlesContext);
    const products = React.useContext(ProductsContext);
    const services = React.useContext(ServicesContext);

    const [trashArticles, setTrashArticles] = useState();
    const [trashCats, setTrashCats] = useState();
    const [trashProducts, setTrashProducts] = useState([]);
    const [trashServices, setTrashServices] = useState();

    useEffect(() => {
        // get and set all articles, categories, products and services that have been soft-deleted
        setTrashArticles(articles.filter(article => article._deleted === true));
        setTrashCats(products.filter(category => category._deleted === true));
        setTrashServices(services.filter(service => service._deleted === true));


        products.forEach(category => {
            const removedProducts = category.catProducts.filter(product => product._deleted === true);
            removedProducts.forEach(product => {
                product.parentCategory = category.catName;
                console.log(product);
                setTrashProducts(trashProducts => [...trashProducts, product]);
            });
        });
    }, []);

    
    function deleteHandler(postType, postObject) {

        console.log(postType);
        let endpoint, title, text, success, failure;
        if (postType && postObject) {
            switch(postType) {
                case "article":
                    endpoint = `articles/delete/${postObject._id}`;
                    title = `Artikkelin "${postObject.title}" poisto`;
                    text = "Oletko varma että haluat poistaa tämän artikkelin lopullisesti? Poistettua artikkelia ei voi palauttaa!"
                    success = "Artikkeli poistettu!";
                    failure = "Artikkelin poistaminen epäonnistui.";
                    break;
                case "category":
                    endpoint = `categories/delete/${postObject._id}`;
                    title = `Tuotekategorian "${postObject.catName}" poisto`;
                    text = "Oletko varma että haluat poistaa tämän tuotekategorian lopullisesti? Poistettua kategoriaa ei voi palauttaa!"
                    success = "Tuotekategoria poistettu!";
                    failure = "Tuotekategorian poistaminen epäonnistui.";
                    break;
                case "product":
                    endpoint = `products/delete/${postObject._id}`;
                    title = `Tuotteen "${postObject.productName}" poisto`;
                    text = "Oletko varma että haluat poistaa tämän tuotteen lopullisesti? Poistettua tuotetta ei voi palauttaa!"
                    success = "Tuote poistettu!";
                    failure = "Tuotteen poistaminen epäonnistui.";
                    break;
                case "service":
                    endpoint = `services/delete/${postObject._id}`;
                    title = `Palvelun "${postObject.serviceName}" poisto`;
                    text = "Oletko varma että haluat poistaa tämän palvelun lopullisesti? Poistettua palvelua ei voi palauttaa!"
                    success = "Palvelu poistettu!";
                    failure = "Palvelun poistaminen epäonnistui.";
                    break;
                default:
                    console.log("invalid post type supplied");
                    return;
            }
            console.log("deletion now");
            deleteContents(endpoint, postObject, title, text, success, failure);
        }
    }

    return (
        <>
            <AdminHelmet title="Roskakori" />
            <div>
                <h3>Roskakori</h3>
                <hr />

                <h5>Artikkelit</h5>
                {trashArticles &&
                <ul>
                    {trashArticles.map(article => 
                    <li key={article._id} className="mb-4">
                        <p>
                            {article.title}
                            &nbsp;-&nbsp;
                            {article.date}
                        </p>
                        <button className="btn btn-primary mr-2 theme-button theme-button" onClick={() => restoreFromTrash("article", article)}>Palauta</button>
                        <button className="btn btn-danger theme-button" onClick={() => deleteHandler("article", article)}>Poista lopullisesti</button>
                    </li>)}
                </ul>
                }

                <hr />

                <h5>Tuotekategoriat</h5>
                {trashCats &&
                <ul>
                    {trashCats.map(category => 
                    <li key={category._id} className="mb-4">
                        <p>
                            {category.catName}
                        </p>
                        <button className="btn btn-primary mr-2 theme-button theme-button" onClick={() => restoreFromTrash("category", category)}>Palauta</button>
                        <button className="btn btn-danger theme-button" onClick={() => deleteHandler("category", category)}>Poista lopullisesti</button>
                    </li>)}
                </ul>
                }

                <hr />

                <h5>Tuotteet</h5>
                <ul>
                    {trashProducts.map(product => 
                    <li key={product._id} className="mb-4">
                        <p>
                            {product.productName}
                            &nbsp;(kategoria: {product.parentCategory})
                        </p>
                        <button className="btn btn-primary mr-2 theme-button theme-button" onClick={() => restoreFromTrash("product", product)}>Palauta</button>
                        <button className="btn btn-danger theme-button" onClick={() => deleteHandler("product", product)}>Poista lopullisesti</button>
                    </li>)}
                </ul>

                <hr />

                <h5>Palvelut</h5>
                {trashServices &&
                <ul>
                    {trashServices.map(service => 
                    <li key={service._id} className="mb-4">
                        <p>
                            {service.serviceName}
                        </p>
                        <button className="btn btn-primary mr-2 theme-button theme-button" onClick={() => restoreFromTrash("service", service)}>Palauta</button>
                        <button className="btn btn-danger theme-button" onClick={() => deleteHandler("service", service)}>Poista lopullisesti</button>
                    </li>)}
                </ul>
                }
                
            </div>
        </>
    );
};

export default TrashBin;