import React, {useState, useContext, useEffect} from "react";
import { BrowserRouter as Router, Route, Switch, Link, useParams, useRouteMatch } from "react-router-dom";
import ProductsContext from "../context/products.context.js";
import ServicesContext from "../context/services.context.js";
import { Container, Row, Col } from "react-bootstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleUp, faAngleDown } from '@fortawesome/free-solid-svg-icons';
import {apiCall} from "../utils.js";
import AdminHelmet from "./AdminHelmet.jsx";


const OrderSection = (props) => {
    const {season, arr, clickHandler} = props;

    let arrSorted;
    if (season === "summer") {
        arrSorted = [...arr.sort( (a, b) => a.summerNum - b.summerNum)];
    } else {
        arrSorted = [...arr.sort( (a, b) => a.winterNum - b.winterNum)];
    }

    

    return (
        <Col className={`${season} my-2`} md="6">
            <h4>{season === "summer" ? "Kesä" : "Talvi"}</h4>
            {arrSorted
            .map(element => 
                <div className="seasonorder-item p-2" key={element._id}>
                    <span>{element.catName || element.serviceName} - {season === "summer" ? element.summerNum : element.winterNum}</span>
                    <div className="seasonorder-buttons">
                        <button className="btn btn-sm" onClick={() => clickHandler("up", season, element, arrSorted)}><FontAwesomeIcon icon={faAngleUp} /></button>
                        <button className="btn btn-sm" onClick={() => clickHandler("down", season, element, arrSorted)}><FontAwesomeIcon icon={faAngleDown} /></button>
                    </div>
                </div>
            )}
        </Col>
    );
};


const SeasonOrder = () => {
    let {path, url} = useRouteMatch();
    const productsInitial = useContext(ProductsContext).filter(category => category._deleted !== true); // only non-trashbinned objects
    const servicesInitial = useContext(ServicesContext).filter(service => service._deleted !== true);
    const [products, setProducts] = useState(JSON.parse(JSON.stringify(productsInitial))); // deep copy (no, spread operator doesn't do it)
    const [services, setServices] = useState(JSON.parse(JSON.stringify(servicesInitial)));

    console.log(products);
    console.log(productsInitial);

    //todo: what if two categories have the same ordernumber
    //immediately update db? change numbers only in state before changes are saved?

    //todo: what if category has no order number or the number is 0
    
    function clickHandler(direction, season, thisObject, arrCopy) {

        // the array index of the element we clicked to move
        const catIndex = arrCopy.indexOf(thisObject);
        // the array index of the element our rising (or falling) star is gonna swap places with
        let swapIndex;

        if (direction === "up") {
            // if not at the beginning of array, set the swappable to be the one before our movable
            catIndex > 0 ? swapIndex = catIndex - 1 : swapIndex = catIndex;
        } else {
            // if not at the end of array, set the swappable to be the one before our movable
            catIndex < (arrCopy.length - 1) ? swapIndex = catIndex + 1 : swapIndex = catIndex;
        }

        // if they're the same, don't do moving. otherwise:
        if (catIndex !== swapIndex) {

            // get the objects based on indexes
            const changeling = arrCopy[catIndex];
            console.log(changeling);

            const swapBuddy = arrCopy[swapIndex];
            console.log(swapBuddy);

            // swap the order numbers around
            if (season === "summer") {
                let temp = changeling.summerNum; // temporary helper variable
                changeling.summerNum = swapBuddy.summerNum;
                swapBuddy.summerNum = temp;
            } else {
                let temp = changeling.winterNum;
                changeling.winterNum = swapBuddy.winterNum;
                swapBuddy.winterNum = temp;
            }

            console.log(arrCopy);

            // set the correct state based on what kind of object
            if (thisObject.catName) {
                setProducts([...arrCopy]);
            } else if (thisObject.serviceName) {
                setServices([...arrCopy]);
            }
        }
    }

    const saveCategoryOrder = () => {
        const order = products.map(product => {
            return {
                _id: product._id, 
                summerNum: product.summerNum, 
                winterNum: product.winterNum
            }
        })
        const data = new FormData();
        data.append("order", JSON.stringify(order));
        console.log(products);
        apiCall("categories/reorder", "PATCH", data, "Järjestys tallennettu");
    }

    const saveServiceOrder = () => {
        const order = services.map(service => {
            return {
                _id: service._id,
                summerNum: service.summerNum,
                winterNum: service.winterNum
            }
        });
        const data = new FormData();
        data.append("order", JSON.stringify(order));
        console.log(services);
        apiCall("services/reorder", "PATCH", data, "Järjestys tallennettu");
    }

    return (
        <>
            <AdminHelmet title="Vuodenaikajärjestys" />
            <Row>
                <div className="col-12">
                    <Link to={`${url}/tuotekategoriat`} className="btn btn-primary theme-button theme-button mr-2">Tuotekategoriat</Link>
                    <Link to={`${url}/palvelut`} className="btn btn-primary theme-button theme-button">Palvelut</Link>
                </div>
                <Switch>
                    <Route
                        exact path={`${url}/tuotekategoriat`}
                        render={() => (
                            <>
                                <Col xs="12">
                                    <h3>Tuotekategoriat</h3>
                                </Col>
                                <OrderSection season="summer" arr={products} clickHandler={clickHandler} />
                                <OrderSection season="winter" arr={products} clickHandler={clickHandler} />
                                <Col xs="12">
                                    <button type="submit" className="btn btn-success theme-button mr-2" onClick={() => saveCategoryOrder()}>Tallenna</button>
                                    <button type="reset" className="btn btn-warning theme-button" onClick={() => setProducts(JSON.parse(JSON.stringify(productsInitial)))}>Palauta</button>
                                </Col>
                            </>
                        )}
                    />
                    <Route
                        exact path={`${url}/palvelut`}
                        render={() => (
                            <>
                                <Col xs="12">
                                    <h3>Palvelut</h3>
                                </Col>
                                <OrderSection season="summer" arr={services} clickHandler={clickHandler} />
                                <OrderSection season="winter" arr={services} clickHandler={clickHandler} />
                                <Col xs="12">
                                    <button type="submit" className="btn btn-success theme-button mr-2" onClick={() => saveServiceOrder()}>Tallenna</button>
                                    <button type="reset" className="btn btn-warning theme-button" onClick={() => setServices(JSON.parse(JSON.stringify(servicesInitial)))}>Palauta</button>
                                </Col>
                            </>
                        )}
                    />
                </Switch>
            </Row>
        </>
    );
};

export default SeasonOrder;