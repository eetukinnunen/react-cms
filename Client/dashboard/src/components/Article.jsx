import React, {useState, useEffect} from "react";
import { Redirect } from "react-router-dom";
import conf from "./config";
import { Row, Col } from "react-bootstrap";
import Wysiwyg from "./Wysiwyg";
import ImgCropTool from "./ImgCropTool";
import SubmitButtons from "./SubmitButtons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBullhorn, faCalendarAlt, faEye, faAlignLeft, faFileImage } from "@fortawesome/free-solid-svg-icons";
import ArticlesContext from "../context/articles.context.js";
import AdminHelmet from "./AdminHelmet";

const Article = (props) => {
    const {status, saveContents, deleteContents, thisArticleId, moveToTrash} = props;
    const articles = React.useContext(ArticlesContext);
    const [initialized, setInitialized] = useState(false);
    console.log(initialized);
    const [thisArticle, setThisArticle] = useState({});

    let helmetTitle;
    if (status === "new") {
        helmetTitle = "Uusi artikkeli";
    } else if (thisArticle) {
        helmetTitle = thisArticle.title;
    }


    console.log(typeof setInitialized);

    function deleteHandler() {
        //deleteContents(`articles/delete/${thisArticle._id}`, thisArticle, "Artikkelin poisto", "Oletko varma että haluat poistaa tämän artikkelin?", "Artikkeli poistettu!", "Artikkelin poistaminen epäonnistui...");
        moveToTrash("article", thisArticle);
    }

    function initialize() {
        if (status === "new") {
            // everything is created from scratch, in the end a post method is submitted
    
            // creates a default article object for the input fields' default values
            setThisArticle({
                title: "",
                //id generation happens in backend after submit, so here id is undefined or empty
                date: new Date().toISOString().substr(0,10),
                visibility: false,
                content: "",
            });
        } else {
            setThisArticle(articles.find(article => article._id === props.thisArticleId && article._deleted !== true));
        }

        console.log(thisArticle)
        setInitialized(true);
    }

    // Runs when article name prop changes (including on first load)
    useEffect(() => {
        if (initialized) { // if already initialized, reload the page to reset all the data to avoid problems
            window.location.reload();
        }
        initialize();
        console.log("initialized");
    }, [props.thisArticleId]);

    return (
        <>
            <AdminHelmet title={helmetTitle} />
            <div>
            {initialized ?
                thisArticle ?
                <form onSubmit={saveContents} encType="multipart/form-data">
                    <input type="hidden" id="formType" name="formType" value="article" />
                    <input type="hidden" id="postId" name="id" value={thisArticle._id} />
                    {(status !== "new" && thisArticle.slug) && <a href={`${conf.client_base_url}/uutiset/${thisArticle.slug}`} target="_blank">Avaa artikkeli</a>}
                    <Row>
                        <Col md="7" lg="8" xl="9">
                            <label htmlFor="title" className="form-input-header"><FontAwesomeIcon icon={faBullhorn} size="1x" className="mr-3"/>Otsikko</label>
                            <input className="form-control" type="text" id="title" name="title" defaultValue={thisArticle.title} required />
                        </Col>
                        <Col md="5" lg="4" xl="3">
                            <label htmlFor="date" className="form-input-header"><FontAwesomeIcon icon={faCalendarAlt} size="1x" className="mr-3"/>Päivämäärä</label>
                            <input className="form-control" type="date" id="date" name="date" defaultValue={thisArticle.date} />
                        </Col>
                    </Row>
                    <Row>
                        <Col sm={12}>
                            <h3 className="form-input-header mt-4"><FontAwesomeIcon icon={faEye} size="1x" className="mr-3"/>Artikkelin julkisuus</h3>
                            <input type="radio" id="private" name="visibility" value="false" defaultChecked={!thisArticle.public} className="mr-2"/>
                            <label htmlFor="private" className="mr-4">Yksityinen</label>
                            <input className="ml-3" type="radio" id="public" name="visibility" value="true" defaultChecked={thisArticle.public} className="mr-2"/>
                            <label htmlFor="public" className="mr-4">Julkinen</label>
                        </Col>
                    </Row>
                    <h3 className="form-input-header mt-4"><FontAwesomeIcon icon={faAlignLeft} size="1x" className="mr-3"/>Artikkelin teksti</h3>
                    <Wysiwyg page="jotain" initial={thisArticle.content} />

                    <label htmlFor="articleimage" className="form-input-header mt-4"><FontAwesomeIcon icon={faFileImage} size="1x" className="mr-3"/>Artikkelikuva</label>
                    <ImgCropTool aspectRatio={16/9} id={thisArticle.id} imgSrc={`${conf.server_base_url}/${thisArticle.image}`} inputName="articleimage" keepSelection="true" />

                    <SubmitButtons status={status} deleteHandler={deleteHandler} label="artikkeli"/>
                </form>
                : <Redirect to={conf.base_route} />
            : ""
            }
            </div>
        </>
    );
};

export default Article;