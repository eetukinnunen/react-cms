import React from "react";
import Helmet from "react-helmet";


function AdminHelmet (props) {
    const {title} = props;

    let titleText =  "Hallintapaneeli";

    if (title) {
        titleText = title + " - " + titleText;
    }

    return (
        <Helmet>
            <title>{titleText}</title>
        </Helmet>
    )
};

export default AdminHelmet;
