import React from "react";
import { Row, Col, Form } from "react-bootstrap";

const SubmitButtons = (props) => {
    const {status, deleteHandler, label} = props;
    return (
        <Row className="mt-4">
            <Col xs="12" md="6" lg="4">
                <button type="submit" className="btn btn-lg btn-success article-button mb-4">Tallenna {label}</button>
            </Col>
            <Col xs="12" md="6" lg="4" className="text-right">
                {status !== "new" ?
                    <button className="btn btn-lg btn-danger article-button mb-4" type="button" onClick={() => deleteHandler()}>Poista {label}</button>
                    : ""
                }
            </Col>
        </Row>
    );
}

export default SubmitButtons;