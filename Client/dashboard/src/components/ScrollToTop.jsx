import { useEffect } from "react";
import { useLocation } from "react-router-dom";

export default function ScrollToTop() {
    const { pathname } = useLocation();
  
    useEffect(() => {
        window.scrollTo(0, 0);
        console.log("scrolled to top");

        // also close the burger menu if exists and open
        const toggleButton = document.querySelector(".navbar-toggler");
        console.log(toggleButton);
        if (toggleButton) {
            if (!toggleButton.classList.contains("collapsed")) {
                toggleButton.click();
            }
        }
    }, [pathname]);
  
    return null;
}