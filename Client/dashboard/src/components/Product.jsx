import React, {useState, useEffect} from "react";
import { Redirect } from "react-router-dom";
import conf from "./config";
import { Row, Col, Form } from "react-bootstrap";
import Wysiwyg from "./Wysiwyg";
import ImgCropTool from "./ImgCropTool";
import { v4 as uuidv4 } from "uuid";
import Swal from "sweetalert2";
import SubmitButtons from "./SubmitButtons";
import ProductsContext from "../context/products.context.js";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBullhorn, faFileAlt, faEuroSign, faMoneyBillAlt, faCommentAlt, faCogs, faCalendarAlt, faEye, faAlignLeft, faFileImage, faTag } from '@fortawesome/free-solid-svg-icons';
import AdminHelmet from "./AdminHelmet";

const Addon = (props) => {
    const {name, price} = props;
    return (
        <>
            <Col sm="5">
                <Form.Label>Lisäosan nimi</Form.Label>
                <Form.Control type="text" name="addonName" defaultValue={name} required />
            </Col>
            <Col sm="5">
                <Form.Label>Lisäosan hinta</Form.Label>
                <Form.Control type="number" name="addonPrice" defaultValue={price} min="0.00" step="0.01" required />
            </Col>
        </>
    );
}

const Product = (props) => {
    const {status, saveContents, deleteContents, thisProductId, moveToTrash} = props;
    const products = React.useContext(ProductsContext);
    
    // when editing an existing product, 
    // 1) get all images as an array
    // 2) map array into an array of ImgCropTools
    // 3) each ImgCropTool has the source url of the image in question

    const [initialized, setInitialized] = useState(false);
    console.log(initialized);

    console.log(`new? ${status}`);

    const [thisProduct, setThisProduct] = useState({});
    const [thisCategoryName, setThisCategoryName] = useState("");
    const [shortDescription, setShortDescription] = useState("");
    const [wordCountClass, setWordCountClass] = useState("");
    const [shortCampaignText, setShortCampaignText] = useState("");
    const [campaignWordCount, setCampaignWordCount] = useState("");

    const [productImages, setProductImages] = useState([]);
    const [addons, setAddons] = useState([]); // array of Addon components on edit page

    let helmetTitle;
    if (status === "new") {
        helmetTitle = "Uusi tuote";
    } else if (thisProduct) {
        helmetTitle = thisProduct.productName;
    }

    // deletes an addon based on its uuid
    function deleteAddon(id) {
        console.log("delete");
        console.log(id);

        console.log(addons);
        addons.forEach(addon => {
            console.log(addon);
        });
        setAddons([...addons.filter(addon => addon.id !== id)]);
    }

    // adds a new addon object to the state array
    function addAddon(name, price) {
        const addonId = uuidv4();
        setAddons(addons => [...addons, 
            {
                name: name,
                price: price,
                id: addonId,
            }
        ]);
    }

    function addNewImage(imgName="") {
        const id = uuidv4();
        const url= `${conf.server_base_url}/${imgName}`;

        setProductImages(productImages => [...productImages, {
            id: id,
            tool: <ImgCropTool aspectRatio={3/2} id={id} imgSrc={url} inputName="productimage" keepSelection="true" key={id} />,
            img: imgName,
        }]);
        console.log(id);
        //saveCrop();
    }

    function removeImg(id) {
        console.log("remove");
        console.log(id);

        console.log(productImages);
        productImages.forEach(img => {
            console.log(img);
        });

        setProductImages(productImages => [...productImages.filter(img => img.id !== id)]);
    }

    // Runs when the productImages array changes
    useEffect(() => {
        console.log("use the effect.");
        console.log(productImages);
        productImages.forEach(img => {
            console.log(img);
        });
    }, [productImages]);

    function deleteHandler() {
        //deleteContents(`products/delete/${thisProduct._id}`, thisProduct, "Tuotteen poisto", "Oletko varma että haluat poistaa tämän tuotteen?", "Tuote poistettu!", "Tuotteen poistaminen epäonnistui...");
        moveToTrash("product", thisProduct);
    }

    function initialize() {
        if (status === "new") {
            // everything is created from scratch, in the end a post method is submitted
    
            // creates a default product object for the input fields' default values
            setThisProduct({
                productName: "",
                //id generation happens in backend after submit, so here id is undefined or empty
                productBrief: "",
                productText: "",
                price: 0.00,
                tax: 24,
            });
            addNewImage();
        } else {
            // goes through all products to find this one by name
            products.forEach(category => {
                const tempValue = category.catProducts.find(product => product._id === props.thisProductId && product._deleted !== true);
                if (tempValue) {
                    setThisProduct(tempValue);
                    setThisCategoryName(category.catName);
                    console.log(tempValue);
                    setShortDescription(tempValue);
                    setShortDescriptionHelper(tempValue.productBrief);

                    tempValue.addons.forEach(addon => {
                        console.log(addon);
                        addAddon(addon.addonName, addon.addonPrice);
                    });
                    tempValue.productImages.forEach(img => {
                        addNewImage(img);
                    });
                }
            });         
        }

        setInitialized(true);
    }

    // Runs when product name prop changes (including on first load)
    useEffect(() => {
        if (initialized) { // if already initialized, reload the page to reset all the data to avoid problems
            window.location.reload();
        }
        initialize();
        console.log("initialized");
    }, [props.thisProductId]);

    // Function gets description as an argument and changes the helper text class accordingly
    // to change the color from green trough orange to red
    function setShortDescriptionHelper (description) { 
        setShortDescription(description);
        if (description.length === 0) {
            setWordCountClass("word-count-ok");
        } else if (description.length < 80 ) {
            setWordCountClass("word-count-ok");   
        } else if (description.length >= 80 && description.length <=99 ) {
            setWordCountClass("word-count-yellow");
        } else if (description.length >= 100) {
            setWordCountClass("word-count-red");
        }
    }
    
    function setCampaignTextHelper (description) { 
        setShortCampaignText(description);
        if (description.length === 0) {
            setCampaignWordCount("word-count-ok");
        } else if (description.length < 80 ) {
            setCampaignWordCount("word-count-ok");   
        } else if (description.length >= 80 && description.length <=99 ) {
            setCampaignWordCount("word-count-yellow");
        } else if (description.length >= 100) {
            setCampaignWordCount("word-count-red");
        }
    }

    const [campaign, setCampaign] = useState(false);

    // if product has campaignprice, set it
    useEffect(() => {
        if(thisProduct) {
            if(thisProduct.campaignPrice) {
                setCampaign(true);
            } else {
                setCampaign(false);
        }
    }
    },[thisProduct]);

    return (
        <>
            <AdminHelmet title={helmetTitle} />
            <div className="product">
            {/* if not editing an existing product or making a new one, redirect back to index */}
            {initialized ?
            (thisProduct.productName || status === "new") ?
                <form onSubmit={saveContents} encType="multipart/form-data">
                    <input type="hidden" id="formType" name="formType" value="product" />
                    <input type="hidden" id="productId" name="id" value={thisProduct._id} />
                    <Row>
                        <Col md="6">
                            <label htmlFor="title" className="form-input-header mt-4"><FontAwesomeIcon icon={faBullhorn} size="1x" className="mr-3"/>Tuotteen nimi</label>
                            <input className="form-control" type="text" id="title" name="title" defaultValue={thisProduct.productName} required />
                        </Col>
                        <Col md="6"> 
                            <label htmlFor="productCategory" className="form-input-header mt-4"><FontAwesomeIcon icon={faFileAlt} size="1x" className="mr-3"/>Tuotekategoria</label>
                            <select className="form-control" id="productCategory" name="productCategory" value={thisCategoryName} onChange={(e) => setThisCategoryName(e.target.value)} required>
                                {products ? products.filter(cat => cat._deleted !== true).map(category => <option key={category._id} value={category.catName}>{category.catName}</option>) : ""}
                            </select>
                            <input type="hidden" id="categories" name="categories" value={JSON.stringify(products.map(({_id, catName}) => ({_id, catName})))} /> {/* provides id and name of every category to the receiving function */}
                        </Col>
                    </Row>
                    <Row>
                        <Col xs="6">
                            <label htmlFor="price" className="form-input-header mt-4"><FontAwesomeIcon icon={faEuroSign} size="1x" className="mr-3"/>Hinta</label>
                            {/* additional formatting? */}
                            <input className="form-control" type="number" id="price" name="price" defaultValue={thisProduct.price} min="0.00" step="0.01" required={campaign} />
                        </Col>
                        <Col xs="6">
                            <label htmlFor="tax" className="form-input-header mt-4"><FontAwesomeIcon icon={faMoneyBillAlt} size="1x" className="mr-3"/>ALV %</label>
                            <input className="form-control" type="number" id="tax" name="tax" defaultValue={thisProduct.tax} min="0.00" step="0.01" />
                        </Col>
                    </Row>
                    <Row>
                        <Col xs="12">
                            <label className="form-input-header mt-4"><FontAwesomeIcon icon={faTag} size="1x" className="mr-3"/>Kampanja</label>
                        </Col>
                        <Col xs="12" sm="6">
                            <label htmlFor="campaign">Aseta kampanjatuotteeksi</label>
                            <input type="checkbox" name="campaignSelect" checked={campaign} onChange={() => setCampaign(!campaign)} />
                        </Col>
                        {campaign &&
                            <Col xs="12" sm="8">
                                <label htmlFor="campaignPrice"><FontAwesomeIcon icon={faEuroSign} size="1x" className="mr-3"/>Kampanjahinta</label>
                                <input className="form-control" type="number" name="campaignPrice" defaultValue={thisProduct.campaignPrice} min="0.00" step="0.01" required />
                                <label htmlFor="campaignText" ><FontAwesomeIcon icon={faCommentAlt} size="1x" className="mr-3"/>Tuotteelle lyhyt kampanjateksti tähän. Ohjepituus 100 merkkiä tai alle. <span className={campaignWordCount}>Merkkejä {shortCampaignText.length}/100</span></label>
                                <input className="form-control" type="text" id="campaignText" name="campaignText" defaultValue={thisProduct.campaignText} onChange={(e) => setCampaignTextHelper(e.target.value)} required />
                            </Col>
                        }
                    </Row>
                    <Row>
                        <Col lg="12">
                            <label htmlFor="brief" className="form-input-header mt-4"><FontAwesomeIcon icon={faCommentAlt} size="1x" className="mr-3"/>Lyhyt kuvaus (suositeltu merkkimäärä 100). <span className={wordCountClass}>Merkkejä {shortDescription.length}/100</span></label>
                            <input className="form-control" type="text" id="brief" name="brief" value={shortDescription} onChange={(e) => setShortDescriptionHelper(e.target.value)} required />
                        </Col>
                    </Row>
                    <label className="form-input-header mt-4"><FontAwesomeIcon icon={faFileAlt} size="1x" className="mr-3"/>Koko kuvaus</label>
                    <Wysiwyg field="productText" initial={thisProduct.productText} />

                    <label className="form-input-header mt-4"><FontAwesomeIcon icon={faCogs} size="1x" className="mr-3"/>Lisäosat</label>
                    {addons.map(addon => 
                        <Row className="mt-4" key={addon.id}>
                            <Addon name={addon.name} price={addon.price} />
                            <Col sm="2">
                                <button type="button" onClick={() => deleteAddon(addon.id)}>Poista</button>
                            </Col>
                        </Row>
                    )}
                    <button type="button" className="btn theme-button mt-3" onClick={() => addAddon("", 0)}>Lisää uusi lisäosa</button>

                    <Row>
                        <Col xs="12">
                            <label className="form-input-header mt-4"><FontAwesomeIcon icon={faFileImage} size="1x" className="mr-3"/>Tuotekuvat</label>
                            {productImages.map(productImg => 
                                <>
                                    {productImg.tool}
                                    <button type="button" onClick={() => removeImg(productImg.id)}>Poista</button>
                                    <input type="hidden" name="filename" value={productImg.img} />
                                </>
                            )}
                            <button type="button" onClick={() => addNewImage()}>Lisää uusi kuva</button>
                        </Col>
                    </Row>

                    <SubmitButtons status={status} deleteHandler={deleteHandler} label="tuote"/>

                </form>
                : <Redirect to={conf.base_route} />
            : "" 
            }
            </div>
        </>
    );
};

export default Product;