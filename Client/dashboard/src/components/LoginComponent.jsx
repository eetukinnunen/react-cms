import React, {useState} from 'react';
import { BrowserRouter as Router, Route, Link, Redirect, useHistory} from "react-router-dom";
import { Form, Button } from 'react-bootstrap';
import conf from "./config";
import Swal from 'sweetalert2';

//Temporary styles
const loginStyle = {
    color: "white",
    backgroundColor: "#284681",
    padding: "20px",
    width: "300px",
}

const Login = () => {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [errMsg, setErrMsg] = useState("")
    // let history = useHistory();

    const handleLogin = () => {
        fetch(`${conf.server_base_url}/login`, {
        method: "POST",
		credentials: "include",
        headers: {'Content-Type':'application/json'},
        body: JSON.stringify({
             "username": username,
             "password": password,
            })
        })
        .then ((response) =>
             response.status === 200 ?
            window.location = `${conf.base_route}`
			// console.log("test")
            : setErrMsg("Väärä käyttäjätunnus tai salasana!"))
        .catch(error => {
            // console.log(error);
        });
    };

return(
    <div style={loginStyle}>
        <Form>
            <Form.Group>
                <Form.Label>Käyttäjätunnus</Form.Label>
                <Form.Control type="text" value={username} name="Username" onChange={({target}) => setUsername(target.value)}/>
            </Form.Group>
            <Form.Group>
                <Form.Label>Salasana</Form.Label>
                <Form.Control type="password" value={password} name="Password"onChange={({target}) => setPassword(target.value)}/>
            </Form.Group>
        <Button variant="primary" onClick={handleLogin} >Kirjaudu</Button>
        <br></br>
        <br></br>
        <Link to="/unohditkosalasanasi">Unohditko salasanasi?</Link>
        <Form.Label>{errMsg}</Form.Label>
        </Form>
    </div>
);
}


export default Login;