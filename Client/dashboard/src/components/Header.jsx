import React from "react";
import conf from "./config";
import { Navbar, Nav } from "react-bootstrap";
import "../style.scss"
// import { ReactComponent as Logo } from '../img/logo.jpg';
import IsLoggedContext from "../context/isLogged.context.js";

const handleLogout = () => {
    fetch(`${conf.server_base_url}/login/logout`,{credentials:"include"})
    .then (() =>
        window.location = conf.base_route
    )};

const Header = () => {
    const isLogged = React.useContext(IsLoggedContext);
     return (
         
            <Navbar className="navbarColor fixed-top" variant="dark">
                <Nav.Link href={`${conf.base_route}/`}>
                    <Navbar.Brand>
                        {/* <Logo /> */}
                    </Navbar.Brand>
                </Nav.Link>
                <Nav className="ml-auto">
                    {isLogged === false && <Nav.Link href={`${conf.base_route}/login`}>Kirjaudu sisään</Nav.Link>}
                    {isLogged === true && <Nav.Link onClick={handleLogout} href={`/`}>Kirjaudu ulos</Nav.Link>}
                </Nav>
            </Navbar>
        
     )
}

export default Header;