import React, {useState} from "react";
import conf from "./config";
import {Form, Col, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';

const ForgottenPasswordEmail = () => {
    const [email, setEmail] = useState("");

    const handleEmail = () => {
        fetch(`${conf.server_base_url}/user/resetpasswordEmail`, {
        method: "PATCH",
        headers: {'Content-Type':'application/json'},
        body: JSON.stringify({
             "email": email,
            })
        })
        .then(response => {
            // console.log(response)
            response.status === 200
            ? Swal.fire(
                "Sähköposti lähetetty",
                "",
                "success")
            : Swal.fire(
                "Hups!",
                `Sähköpostiosoitetta ei löytynyt`,
                "error")
        })
        .catch(error => {
            // console.log(error);
            // error message/popup
            Swal.fire(
                "Hups!",
                `Toiminto epäonnistui... <br />  <span class="error-text">${error}</span>`,
                "error"
            );
        });
    };

return(
    <div>
        <Form>
            <Form.Group>
                <Form.Label>Sähköposti</Form.Label>
                <Form.Control type="text" value={email} name="Username" onChange={({target}) => setEmail(target.value)}/>
            </Form.Group>
        </Form>
        <Button variant="primary" onClick={handleEmail}>Lähetä sähköposti</Button>
    </div>
);
}


export default ForgottenPasswordEmail;