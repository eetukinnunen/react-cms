import React, {useState, useEffect} from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import {Nav, Navbar, Container, Row, Col, Collapse, ButtonGroup, Button, CardColumns} from "react-bootstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleLeft, faAngleRight, faLeaf, faSnowflake } from '@fortawesome/free-solid-svg-icons';
import { v4 as uuidv4 } from 'uuid';
import PagesContext from "../context/pages.context.js";
import ProductsContext from "../context/products.context.js";
import ArticlesContext from "../context/articles.context.js";
import ServicesContext from "../context/services.context.js";
import IsLoggedContext from "../context/isLogged.context.js";
import {apiCall} from "../utils.js";
import conf from "./config";
import "../style.scss";

const sortByDate = function (a, b) {
    if ( a.date < b.date ) {
        return 1;
    }
    if ( a.date > b.date ) {
        return -1;
    }
    return 0;
}

const ArticleLinks = (props) => {
    const {articles, stringToUrl} = props;

    // Articles per page, max number of pagination buttons and current page with their default values
    const [articlesPerPage, setArticlesPerPage] = useState(5);
    const [maxButtons, setMaxButtons] = useState(5);
    const [currentPage, setCurrentPage] = useState(1);

    // This function gets page number as a prop and returns article links for that page
    const paginatedArticleLinks = (page) => {
        const start = ((page * articlesPerPage) - articlesPerPage);
        const end = (page * articlesPerPage);

        // console.table(articles);

        const orderedArticles = [...articles];

        orderedArticles.sort(sortByDate);
        
        const articleLinks = orderedArticles.map(article =>
            <li key={uuidv4()}>
                <h5>
                    <Link to={`${conf.base_route}/ajankohtaista/${stringToUrl(article._id)}`} className="articleLinks">{article.title}</Link>
                </h5>
                <p className="sidebar-article-date">{article.date}</p>
            </li>
        );
      
        const slicedLinks = articleLinks.slice(start, end);
        return slicedLinks;

    }

    
    // This function handles the page change from pagination buttons
    const handlePageChange = (page) => {
        setCurrentPage(page)
    }

    // This function creates the pagination buttons
    const pageButtons = () => {
        let buttons = []; 
        const numberOfPages = Math.ceil(articles.length / articlesPerPage);

        // Classes for normal and acctive buttons
        const cls = "btn-sm";
        const clsActive = "btn-sm active";

        // Push all pagination buttons to the buttons-array
        for (let i = 1; i <= numberOfPages; i++) {
            buttons.push(<Button className={currentPage === i ? clsActive : cls} onClick={() => handlePageChange(i)} key={uuidv4()}>{i}</Button>);
        }

        let slicedButtons = [];

         
        // If there are more buttons than maxButtons slice the array
        // By default the slice starts at current page number
        if (numberOfPages > maxButtons) {

            let sliceStart = currentPage - 1;
            let sliceEnd = (currentPage - 1) + maxButtons; 

            // This was to make sure the slice does not end outside the button range. It seems 
            // irrelevant at the moment.
            // if ( sliceEnd > numberOfPages ) {
            //    sliceEnd = numberOfPages;

            
            // If current page is close to the max amount of pages, the slice would
            // take off too much from the beginning. For example if current page 
            // is 5 and last page is 6 we want to show pages 2-6, not just 5-6
            if ( currentPage > numberOfPages - maxButtons ) {
                sliceStart = numberOfPages - maxButtons;
            }
            
            slicedButtons = buttons.slice(sliceStart, sliceEnd);

        } else {
            slicedButtons = [...buttons];
        } 
        
        // Add next and previous buttons to the beginning and end of the array
        slicedButtons = [<Button className="btn-sm" onClick={() => handlePageChange(currentPage -1)} disabled={currentPage > 1 ? false : true} key={uuidv4()}><FontAwesomeIcon icon={faAngleLeft} /></Button>, ...slicedButtons];
        slicedButtons = [...slicedButtons, <Button className="btn-sm" onClick={() => handlePageChange(currentPage + 1)} disabled={currentPage !== numberOfPages ? false : true} key={uuidv4()}><FontAwesomeIcon icon={faAngleRight} /></Button>];
        
        return (
            <ButtonGroup className="mt-4">
                {slicedButtons}
            </ButtonGroup>
        )
    }

    return (
    <ul>
        {paginatedArticleLinks(currentPage)}
        {articles.length > articlesPerPage && pageButtons()}
    </ul>
    )
}


const CreateSidebar = (props) => {
    
    
    const isLogged = React.useContext(IsLoggedContext);

    const {stringToUrl} = props;
    const pages = React.useContext(PagesContext);
    const products = React.useContext(ProductsContext);
    const articles = React.useContext(ArticlesContext);
    const services = React.useContext(ServicesContext);
    
    const [newsOpen, setNewsOpen] = useState(false);
    const [productsOpen, setProductsOpen] = useState(false);
    const [servicesOpen, setServicesOpen] = useState(false);
    const [folderSize, setFolderSize] = useState();
    const [maxFolderSize, setMaxFolderSize] = useState();

    const isWinter = () => {
        const contactPage = pages.find(page => page.title === "Yhteystiedot");
        return contactPage && contactPage.season === "winter";
    }

    function seasonChange() {
        const contactPage = pages.find(page => page.title === "Yhteystiedot");
        const data = new FormData();

        data.append("type", "page");
        data.append("contents", JSON.stringify(contactPage.contents));
        data.append("_id", contactPage._id);
        
        if(contactPage.season === "summer") {
            data.append("season", "winter")
        } else {
            data.append("season", "summer")
        }
        console.log(contactPage)
        console.log(data);
        apiCall("pages/update", "PATCH", data, "Vuodenaika vaihdettu!");

        console.log("season change!");
        
    };

    const pageLinks = pages.sort((a, b) => a.orderNumber - b.orderNumber).map(page =>
        <li key={uuidv4()}>
            <Link to={`${conf.base_route}/${stringToUrl(page.title)}`} className="smallLinks" >{page.title}</Link>
        </li>
    );

    let productLinks = products.filter(category => category._deleted !== true)
    .map(category =>
        <div key={uuidv4()}>
            <li key={uuidv4()}>
                <h5>
                    <Link to={`${conf.base_route}/kategoriat/${stringToUrl(category.catName)}`} className="links" >{category.catName}</Link></h5>
            </li>
            <ul>
                {category.catProducts ? category.catProducts.filter(product => product._deleted !== true).map(product =>
                    <li key={uuidv4()}>
                        <Link to={`${conf.base_route}/tuotteet/${stringToUrl(product._id)}`} className="smallLinks" >{product.productName}</Link>
                    </li>
                ) : ""}
            </ul>
        </div>
    );

    let ServiceLinks = services.filter(service => service._deleted !== true).map(service =>
        <li key={uuidv4()}>
            <Link to={`${conf.base_route}/palvelut/${stringToUrl(service._id)}`} className="smallLinks" >{service.serviceName}</Link>
        </li>
    );

    const getUploadFolderSize = () => {
        fetch(`${conf.server_base_url}/images/foldersize`,{credentials:"include"})
        .then(response => response.json())
        .then(json => {
            setFolderSize(json[0]);
            setMaxFolderSize(json[1]);
        });
    };

    useEffect(() => {
        getUploadFolderSize();
    }, []);

    const convertBytes = bytes => {
        const sizes = ["Bytes", "KB", "MB", "GB", "TB"];
        if(bytes === 0) {
            return "n/a";
        }

        const i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
        if(1 === 0) {
            return bytes + " " + sizes[i];
        }
        return (bytes / Math.pow(1024, i)).toFixed(1) + " " + sizes[i];
    };

    console.log(folderSize);
    console.log(maxFolderSize);
    let spaceTakenPercentage = folderSize / maxFolderSize * 100;
    console.log(spaceTakenPercentage);
    if (spaceTakenPercentage > 100) {
        spaceTakenPercentage = 100;
    }
    const barColorValue = (0 - 120) * (spaceTakenPercentage / 100) + 120;

if(isLogged === true){
    return (
        <Row className="h-100 sidebartextwrap">
          <Col sm="4" md="3" xl="2" className="sidenav sidebar pr-0">
            <Navbar variant="dark" expand="sm" className="col-12 px-2">
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse>
                    <Nav className="flex-column pb-sm-5">
                        <h4 className="sidebarHeader">Sivut</h4>
                        <ul>
                            {pageLinks}
                        </ul>
                        <hr />
                        <h4 className="sidebarHeader dropdown-toggle" onClick={() => setNewsOpen(!newsOpen)}
                            aria-controls="news-collapse"
                            aria-expanded={newsOpen} >Ajankohtaista</h4>
                        <Collapse in={newsOpen}>
                            <div className="news-collapse">
                                <Link to={`${conf.base_route}/uusi_artikkeli`} className="btn btn-primary btn-sm sidebar-button mb-2 mt-2">Uusi artikkeli</Link>
                                <ArticleLinks articles={articles.filter(article => article._deleted !== true)} stringToUrl={stringToUrl} />
                            </div>
                        </Collapse>
                        <hr />
                        <h4 className="sidebarHeader dropdown-toggle" onClick={() => setProductsOpen(!productsOpen)}
                            aria-controls="products-collapse"
                            aria-expanded={productsOpen} >Tuotteet</h4>

                        <Collapse in={productsOpen}>
                            <div id="products-collapse">
                                <Link to={`${conf.base_route}/uusi_kategoria`} className="btn btn-primary btn-sm sidebar-button mb-2">Uusi tuotekategoria</Link>
                                <br />
                                <Link to={`${conf.base_route}/uusi_tuote`} className="btn btn-primary btn-sm sidebar-button">Uusi tuote</Link>
                                <ul>
                                    {productLinks}
                                </ul>
                            </div>
                        </Collapse>
                        <hr />
                        <h4 className="sidebarHeader dropdown-toggle" onClick={() => setServicesOpen(!servicesOpen)}
                            aria-controls="services-collapse"
                            aria-expanded={servicesOpen} >Palvelut</h4>

                        <Collapse in={servicesOpen}>
                            <div id="products-collapse">
                                <Link to={`${conf.base_route}/uusi_palvelu`} className="btn btn-primary btn-sm sidebar-button mb-2 mt-2">Uusi palvelu</Link>
                                <ul>
                                    {ServiceLinks}  
                                </ul>
                            </div>
                        </Collapse>

                        <hr />
                        <h4 className="sidebarHeader">Hallinta</h4>
                        <div id="rod-of-seasons">
                            <h6>Vuodenajan vaihto</h6>
                            <label className="switch">
                                <input type="checkbox" checked={isWinter()} name="season" onChange={() => seasonChange()} />
                                <span className="slider">
                                    <FontAwesomeIcon icon={faSnowflake} className="winter-indicator" />
                                    <FontAwesomeIcon icon={faLeaf} className="summer-indicator" />
                                </span>
                            </label>
                        </div>
                        <ul>
                            <li>
                                <Link to={`${conf.base_route}/jarjestys`} className="smallLinks">Järjestys</Link>
                            </li>
                            <li>
                                <Link to={`${conf.base_route}/kuvat/sivu/1`} className="smallLinks">Kuvat</Link>
                            </li>
                            <li>
                                <Link to={`${conf.base_route}/roskakori`} className="smallLinks">Roskakori</Link>
                            </li>
                            <li>
                                <Link to="/asetukset" className="smallLinks">Käyttäjäasetukset</Link>
                            </li>
                            <li>
                                <Link to={`${conf.base_route}/ohjeet`} className="smallLinks">Ohjeet</Link>
                            </li>
                        </ul>
                        <div id="server-space">
                            <h6>Kuvien viemä tila palvelimella</h6>
                            <div className="usage-bar">
                                <div className="filled" style={{width: `${spaceTakenPercentage}%`, backgroundColor: `hsl(${barColorValue}, 100%, 50%)`}}></div>
                            </div>
                            <p>{convertBytes(folderSize)} / {convertBytes(maxFolderSize)}</p>
                        </div>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
          </Col>
        </Row>
    )
}
else{return null}
}

export default CreateSidebar;