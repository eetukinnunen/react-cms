import React, {useState, useEffect} from "react";
import { Redirect } from "react-router-dom";
import conf from "./config";
import { Row, Col, Form } from "react-bootstrap";
import Wysiwyg from "./Wysiwyg";
import Swal from "sweetalert2";
import SubmitButtons from "./SubmitButtons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBullhorn, faAlignLeft } from "@fortawesome/free-solid-svg-icons";
import ProductsContext from "../context/products.context.js";
import AdminHelmet from "./AdminHelmet";

const Category = (props) => {
    const {status, saveContents, deleteContents, moveToTrash} = props;
    const products = React.useContext(ProductsContext);

    const [initialized, setInitialized] = useState(false);
    const [thisCategory, setThisCategory] = useState({});

    let helmetTitle;
    if (status === "new") {
        helmetTitle = "Uusi tuotekategoria";
    } else if (thisCategory) {
        helmetTitle = thisCategory.catName;
    }

    function deleteHandler() {
        //deleteContents(`categories/delete/${thisCategory._id}`, thisCategory, "Tuotekategorian poisto", "Oletko varma että haluat poistaa tämän tuotekategorian?", "Tuotekategoria poistettu!", "Tuotekategorian poistaminen epäonnistui...");
        moveToTrash("category", thisCategory);
    }

    function initialize() {
        if (status === "new") {
            // everything is created from scratch, in the end a post method is submitted
    
            // creates a default category object for the input fields' default values
            setThisCategory({
                catName: "",
                //id generation happens in backend after submit, so here id is undefined or empty
                catDescription: "",
                catProducts: []
            });
        } else {
            setThisCategory(products.find(category => category.catName === props.thisCategoryName && category._deleted !== true));
        }

        setInitialized(true);
    }

    // Runs when category name prop changes (including on first load)
    useEffect(() => {
        if (initialized) { // if already initialized, reload the page to reset all the data to avoid problems
            window.location.reload();
        }
        initialize();
        console.log("initialized");
    }, [props.thisCategoryName]);

    // Makes sure a duplicate category by the same name can't be made
    function submitHandler(e) {
        e.preventDefault();

        if (status === "new") {
            if (products.find(category => category.catName === e.target.title.value)) {
                console.log("error: category by this name already exists!");
                // add an error message for user
                Swal.fire(
                    "Hups!",
                    "Tietojen tallennus epäonnistui. Tuotekategoria on jo olemassa.",
                    "error"
                );
            } else {
                saveContents(e);
            }
        } else {
            saveContents(e);
        }
    }

    return (
        <>
            <AdminHelmet title={helmetTitle} />
            <div className="category">
            {initialized ?
                thisCategory ?
                <form onSubmit={submitHandler} encType="multipart/form-data">
                    <input type="hidden" id="formType" name="formType" value="category" />
                    <input type="hidden" id="catId" name="id" value={thisCategory._id} />
                <Row>
                    <Col md="6">
                        <label htmlFor="title" className="form-input-header"><FontAwesomeIcon icon={faBullhorn} size="1x" className="mr-3"/>Kategorian nimi</label>
                        <input className="form-control" type="text" id="title" name="title" defaultValue={thisCategory.catName} required />
                    </Col>
                </Row>
                    <label className="form-input-header mt-4"><FontAwesomeIcon icon={faAlignLeft} size="1x" className="mr-3"/>Koko kuvaus</label>
                    <Wysiwyg field="catDescription" initial={thisCategory.catDescription} />

                    <SubmitButtons status={status} deleteHandler={deleteHandler} label="kategoria"/>
                </form>
                : <Redirect to={`${conf.base_route}`} />
            : ""
            }
            </div>
        </>
    );
};

export default Category;