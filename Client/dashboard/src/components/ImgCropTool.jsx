import React, { useState, useCallback, useRef, useEffect } from "react";
import PropTypes from "prop-types";
import ReactCrop from "react-image-crop";
import "react-image-crop/dist/ReactCrop.css";
import { v4 as uuidv4 } from "uuid";
import { Row, Col } from "react-bootstrap";

// Setting a high pixel ratio avoids blurriness in the canvas crop preview.
const pixelRatio = 4;

// We resize the canvas down when saving on retina devices otherwise the image
// will be double or triple the preview size.
function getResizedCanvas(canvas, newWidth, newHeight) {
  const tmpCanvas = document.createElement("canvas");
  tmpCanvas.width = newWidth;
  tmpCanvas.height = newHeight;

  const ctx = tmpCanvas.getContext("2d");
  ctx.drawImage(
    canvas,
    0,
    0,
    canvas.width,
    canvas.height,
    0,
    0,
    newWidth,
    newHeight
  );

  return tmpCanvas;
}

function generateDownload(previewCanvas, crop) {
  if (!crop || !previewCanvas) {
    return;
  }

  const canvas = getResizedCanvas(previewCanvas, crop.width, crop.height);

  canvas.toBlob(
    blob => {
      const previewUrl = window.URL.createObjectURL(blob);

      const anchor = document.createElement("a");
      anchor.download = "cropPreview.png";
      anchor.href = URL.createObjectURL(blob);
      anchor.click();

      window.URL.revokeObjectURL(previewUrl);
    },
    "image/png",
    1
  );
}

export default function ImgCropTool(props) {
  const {aspectRatio, id, inputName, imgSrc, keepSelection} = props;
/*  ImgCropTool.propTypes = {
    aspectRatio: PropTypes.number,
    id: PropTypes.string,
    inputName: PropTypes.string.isRequired,
    imgSrc: PropTypes.string,
    setCropComplete: PropTypes.func,
};*/

  const [upImg, setUpImg] = useState(imgSrc);
  const [fileName, setFileName] = useState("Valitse kuva"); // Filename to show in the form. Default is tooltip text.
  const imgRef = useRef(null);
  const previewCanvasRef = useRef(null);
  
  function cropInitialState() {
    if (aspectRatio) {
      return {unit: "%", width: 100, aspect: aspectRatio};
    } else {
      return {unit: "%", width: 100, height: 100};
    }
  }

  const [crop, setCrop] = useState(cropInitialState());
  const [completedCrop, setCompletedCrop] = useState(null);

  const [scalingInfo, setScalingInfo] = useState(null);

  const onSelectFile = e => {
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener("load", () => setUpImg(reader.result));
      reader.readAsDataURL(e.target.files[0]);
      setFileName(e.target.files[0].name);
    }
  };

  const onLoad = useCallback(img => {
    imgRef.current = img;
  }, []);

  useEffect(() => {
    if (!completedCrop || !previewCanvasRef.current || !imgRef.current) {
      return;
    }

    const image = imgRef.current;
    const canvas = previewCanvasRef.current;
    const crop = completedCrop;

    console.log(completedCrop);

    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;

    /*console.log(`naturalwidth: ${image.naturalWidth}`);
    console.log(`width: ${image.width}`);
    console.log(`scaleX: ${scaleX}`);
    console.log(`naturalwidth: ${image.naturalWidth}`);
    console.log(`height: ${image.height}`);
    console.log(`scaleY: ${scaleY}`);*/

    setScalingInfo({
      naturalWidth: image.naturalWidth,
      scaledWidth: image.width,
      scaleX: scaleX,
      naturalHeight: image.naturalHeight,
      scaledHeight: image.height,
      scaleY: scaleY
    });

    //console.log(scalingInfo);

    const ctx = canvas.getContext("2d");

    canvas.width = crop.width * pixelRatio;
    canvas.height = crop.height * pixelRatio;

    ctx.setTransform(pixelRatio, 0, 0, pixelRatio, 0, 0);
    ctx.imageSmoothingEnabled = false;

    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      crop.width,
      crop.height
    );

    //console.log(crop);

  }, [completedCrop]);

  return (
    <div className="App">
      <Col lg={8} className="p-0">
        <div className="input-group file-input-container">
          <div className="custom-file">
            <input type="file" id={inputName+"-"+uuidv4()} name={inputName} accept="image/*" onChange={onSelectFile} className="custom-file-input" />
            <label className="custom-file-label" htmlFor="inputGroupFile01">{fileName}</label>
          </div>
        </div>
      </Col>
      <div>
        <input type="hidden" id={"crop-"+uuidv4()} name="crop" value={JSON.stringify(completedCrop)} />
        <input type="hidden" id={"scaling-"+uuidv4()} name="scaling" value={JSON.stringify(scalingInfo)} />
      </div>
      <ReactCrop
        src={upImg}
        onImageLoaded={onLoad}
        crop={crop}
        onChange={c => setCrop(c)}
        onComplete={c => setCompletedCrop(c)}
        keepSelection={keepSelection}
      />
      <div>
        <canvas
          ref={previewCanvasRef}
          className={
            completedCrop ? "cropped-image" : ""
          }
          style={{
            width: completedCrop?.width ?? 0,
            height: completedCrop?.height ?? 0
          }}
        />
      </div>
      {/* <button
        type="button"
        disabled={!completedCrop?.width || !completedCrop?.height}
        onClick={() =>
          generateDownload(previewCanvasRef.current, completedCrop)
        }
      >
        Download cropped image
      </button> */}
    </div>
  );
}
