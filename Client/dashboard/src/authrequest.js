//Path and method from starting function
const AuthenticatedRequest = async({path, method, data={}}) => { 
    try {
        const response = await fetch(path, {
            method: method,
            headers: {'content-type':'application/json',},
        });
        //This handles the response, if response is 401 unauthorized,
        //kick user back to loginscreen.
        //If response is succesful, send response back to the starting function.
        switch (response.status) {
            case 200:
                return response;
            case 401:
                `${conf.base_route}/login/`
                break;
            default:
                return response;
        }
    }
    catch(e) {
        console.log("Exception", e)
    }
};
export default AuthenticatedRequest;
//Not used, maybe not required since userfunctions are mainly fetchData() and deleteContents()