# React CMS

**Directory structure**

- The website frontend: Client/website
- CMS web interface: Client/dashboard
- Backend API: Server/

Both React applications connect to the same API.

API connects to the Mongo database.

**Startup**

1. Clone the repository.

2. (If you don't have MongoDB Server and MongoDB Tools installed, install them)

In your command line, run *mongod* in MongoDB/Server/[version_number]/bin/.

3. In another command line instance, run *mongorestore [dump_folder_path]* in the correct MongoDB/Tools/[version_number]/bin to generate a local database from the contents of the repository's dump/ directory.

4. Run *mongo* inside MongoDB/Server/[version_number]/bin/ and create a DB user.

5. Generate .env and config.js files based on the .default files.

*.env* file location: 

- Server/

*config.js* file locations:

- Client/website/src/components/
- Client/dashboard/src/components/

6. Add required database connection info into the DB variables within the .env file (username, password, host, prefix).

For example:

- DB_USER = "exampleUser"
- DB_PASS = "examplePassword"
- DB_HOST = "localhost/dev"
- DB_PREFIX = "mongodb"

7. Install node modules

- Client/website/ 	    npm install
- Client/dashboard/ 	npm install
- Server/ 		        npm install

8. Launch the applications:

- Client/website/ 	    npm start
- Client/dashboard/ 	npm start
- Server/ 		        npm start


**Ports**

Website: 		    localhost:3000

Dashboard: 	        localhost:3001

Backend: 		    localhost:4001


**Database**

MongoDB

Collections:

- articles
- categories
- pages
- products
- services
- users


**MongoDB schema**

See Server/src/models


**Using the dashboard**

By default, username: "Test", password: "qweasd".

**Images**

The maximum image size uploaded to the backend is set to 5MB.

Directory location for images saved via the dashboard:	Server/uploads/


**Credits (with main responsibilities during the project)**

Eetu Kinnunen       - scrum master, fullstack development

Jarkko Orava        - graphic design, frontend development

Mikael Komulainen   - authentication, backend development

Toni Pekkala        - database, backend development

Toni Suutari        - fullstack development